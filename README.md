# MECS

C++ Multicore Events Components and Systems

TODO:
* Change queries to take component& vs component* for all and any. This should unify the query with the function parameters event more.
* Fix: registration should have a state variable to make sure the graph is register before the delegates
* Fix: automatic registration of component types 
* Fix: group locks
* Fix: convert all the events to the new event system
* Add: Share-component
* Add: Singelton-components
* Add: value_subgroup + value_subgroup_sorted_execution
* Add: group-pages to be mapped in a contiguious memory space using virtual pages. 
* Add: Finish memory protection for multicore access to groups. (getting entities inside a system) 
* Add: Make memory protection go away on release type of builds (no overhead)
* Add: Example using compute
* Add: ImGui
* Add: Proper Debug Render
* Fix: Optimize the getComponent using the tmp entity

DONE:
* Add: Add more system query as part of function definitions func( optional* x, musthave& ) + Access 
* Fix: rename groups to architype? <<I won't renamed them, as architype has a standard meaning already, I don't want to overload it any farther.>>


References
* https://github.com/SanderMertens/flecs
* https://github.com/junkdog/entity-system-benchmarks
* https://github.com/sschmid/Entitas-CSharp
* http://lab.guchanalkan.com/2016/11/07/performance-comparison-ecs.html
* https://github.com/skypjack/entt
* https://github.com/redxdev/ECS
* 


