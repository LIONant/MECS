
namespace mecs::tools
{
#if 0
    #define CHECK_FUNCTION( FUNCTION_NAME )                                         \
    template<typename, typename T>                                                  \
    struct has_##FUNCTION_NAME                                                      \
    {                                                                               \
        static_assert( std::integral_constant<T, false>::value,                     \
            "Second template parameter needs to be of function type.");             \
    };                                                                              \
    /* specialization that does the checking */                                     \
    template<typename T_CLASS, typename Ret, typename... Args>                      \
    struct has_##FUNCTION_NAME <T_CLASS, Ret(Args...)>                              \
    {                                                                               \
        template<typename T>                                                        \
        static constexpr auto check(T*) -> typename std::is_same                    \
        <                                                                           \
            decltype(std::declval<T>().FUNCTION_NAME(std::declval<Args>()...))      \
            , Ret /* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   */    \
        >::type;  /* attempt to call it and see if the return type is correct */    \
        template<typename>                                                          \
        static constexpr std::false_type check(...);                                \
        using type = decltype(check<T_CLASS>(nullptr));                             \
        static constexpr bool value = type::value;                                  \
    };                                                                              \
    template<typename T, typename...T_ARGS>                                         \
    std::enable_if_t<true == has_##FUNCTION_NAME<T, void(T_ARGS...)>::value, void > \
    Call##FUNCTION_NAME(T& A, T_ARGS...Args) noexcept { A.FUNCTION_NAME(Args...); } \
    template<typename T, typename...T_ARGS>                                         \
    std::enable_if_t<false == has_##FUNCTION_NAME<T, void(T_ARGS...)>::value, void >\
    Call##FUNCTION_NAME(T&, T_ARGS...Args) noexcept {}                          

    CHECK_FUNCTION(Update)                          // W/R Components
    //  CHECK_FUNCTION(ExclusiveUpdate)                 // W/R Components + add/remove components
    //  CHECK_FUNCTION(BarrierUpdate)

    #undef CHECK_FUNCTION
#endif

    //---------------------------------------------------------------------------------
    // TOOLS::bits
    //---------------------------------------------------------------------------------
    struct bits
    {
        std::array<std::uint64_t,2> m_Bits;

        constexpr bits()=default;
        constexpr bits(std::nullptr_t)    noexcept : m_Bits{ 0ull, 0ull}{}
        constexpr bits(xcore::not_null_t) noexcept : m_Bits{~0ull,~0ull}{}

        xforceinline constexpr int AllocBit( void ) noexcept
        {
            for( auto& E : m_Bits ) 
            {
                const auto Index = xcore::bits::ctz64(E);
                if(Index != 64 )
                {
                    E &= ~(1ull<<Index);
                    return Index;
                }
            }

            xassert(false);
            return -1;
        }

        xforceinline void AddBit( int Index ) noexcept
        {
            m_Bits[Index/64] |= (1ull<<(Index%64));
        }

        xforceinline void Add( bits Bits ) noexcept
        {
            for( int i=0; i<m_Bits.size(); ++i ) m_Bits[i] |= Bits.m_Bits[i];
        }
        xforceinline constexpr bool getBit( int Index ) const noexcept
        {
            return !!((m_Bits[Index/64] >> (Index%64))&1);
        }

        xforceinline constexpr bool isMatchingBits( const bits& Match ) const noexcept
        {
            for( int i=0; i<m_Bits.size(); ++i )
            {
                if( m_Bits[i] & Match.m_Bits[i] ) return true;
            }
            return false;
        }

        xforceinline void clear( void ) noexcept
        {
            for( int i=0; i<m_Bits.size(); ++i )
            {
                m_Bits[i] = 0;
            }
        }

        xforceinline constexpr bool Query( bits All, bits Any, bits None ) const noexcept
        {
            bool    bAny1 = false;
            bool    bAny2 = false;
            for( int i=0; i<m_Bits.size(); ++i )
            { 
                const auto B = m_Bits[i];
                if( None.m_Bits[i] & B ) return false;
                if( const auto A = All.m_Bits[i]; (A&B) != A ) return false;
                if( const auto A = Any.m_Bits[i]; (A&B) ) bAny1 = true;
                else bAny2 = A != 0;
            }
            return bAny1 || bAny2 == bAny1;
        }
    };

    //---------------------------------------------------------------------------------
    // TOOLS::map_fixed V2
    //---------------------------------------------------------------------------------
    template< typename T_KEY, typename T_VALUE, std::size_t T_MAXSIZE_V > 
    struct map_fixed
    {
        constexpr static auto max_size_v = T_MAXSIZE_V + (T_MAXSIZE_V/10); // Add an additional 10% of entries for collisions and such

        struct lock;

        struct entry
        {
            T_KEY               m_Key;
            T_VALUE             m_Value;        // TODO: These pointers below could be converted to indices to save memory
            entry*              m_pNext;        // Next for cases where collisions happen
            std::atomic<lock*>  m_pLock;        // Pointer back to its lock
        };
                
        struct lock                             // This is the core entry in the hash table. 
        {
            xcore::lock::semaphore  m_Lock;     // Lock is used to stop from modifying any of the entries in the linklist m_pEntry
            entry*                  m_pEntry;   // link list of entries for a particular hash table entry
            entry                   m_Entry;    // Memory where an entry may be allocated
        };

        map_fixed()
        {
            // Allocate the memory
            m_Map.Alloc(max_size_v).CheckError();

            // Initialize the memory as fast as possible
            xcore::scheduler::channel Channel( xconst_universal_str("map_fixed::constructor") );
            Channel.ForeachLog( m_Map, 8, 1000, [&]( xcore::span<lock> View )
            {
                if constexpr ( std::is_trivially_constructible_v<T_VALUE> )
                {
                    memset( View.data(), 0, View.size()*sizeof(lock) );
                }
                else
                {
                    // Call constructors 
                    for( auto& E : View ) new(&E) lock{};
                }
            });
            Channel.join();
        }

        void clear()
        {
            m_Map.clear();
        }
        
        template< typename T_CALLBACK >
        entry* alloc( T_KEY Key, T_CALLBACK&& Callback = [](entry&)constexpr noexcept{} ) noexcept
        {
            assert(Key.isValid());

            auto            Index       = Key.m_Value % max_size_v;
            auto&           MapEntry    = m_Map[Index];
            entry*          pEntry      = &m_Map[Index].m_Entry;

            // Read/Write lock our hash entry
            xcore::lock::scope Lk( MapEntry.m_Lock );

            // Make sure still not constructed
            for( auto p = MapEntry.m_pEntry; p ; p = p->m_pNext )
                if( p->m_Key == Key ) 
                    return p;

            // Find/Alloc free memory
            do 
            {
                auto  L = pEntry->m_pLock.load(std::memory_order_relaxed);
                if( L )
                {
                    Index   = (Index + 1)%max_size_v;
                    pEntry  = &m_Map[Index].m_Entry;
                    continue;
                }
                if( pEntry->m_pLock.compare_exchange_weak( L, &MapEntry ) ) break;

            } while (true);

            // Do we have a collision? 
            // Make sure we have not collisions with any one
            xassert_block_basic()
            {
                for( auto p = MapEntry.m_pEntry; p ; p = p->m_pNext )
                {
                    xassert( Key.m_Value != p->m_Key.m_Value );
                }
            }

            pEntry->m_Key     = Key;
            pEntry->m_pNext   = MapEntry.m_pEntry;
            MapEntry.m_pEntry = pEntry;

            // Setup the node
            Callback(*pEntry);           // should set ( pNode->m_Value = ... )

            return nullptr;
        }

        template< typename T > xforceinline constexpr 
        bool find( T_KEY Key, T&& CallBack = [](const entry&)constexpr noexcept{} ) const noexcept
        {
            assert(Key.isValid());

            const auto& MapEntry = m_Map[Key.m_Value % max_size_v];

            // User requesting Read only lock here because the function is const
            xcore::lock::scope Lk( MapEntry.m_Lock );

            for( auto p = MapEntry.m_pEntry; p; p = p->m_pNext )
                if( p->m_Key == Key ) 
                {
                    CallBack( *p );
                    return true;
                }

            return false;
        }

        template< typename T_CREATE_CALLBACK, typename T_GET_CALLBACK > xforceinline
        bool getOrCreate( T_KEY Key, T_GET_CALLBACK&& GetCallback, T_CREATE_CALLBACK&& CreateCallBack ) noexcept
        {
            TRY_AGAIN_GET_OR_CREATE:
            if( find( Key, GetCallback ) )
                return true;

            if( alloc( Key, CreateCallBack ) )
                goto TRY_AGAIN_GET_OR_CREATE;

            // if we can not find it then create it
            return false;
        }

        template< typename T_CALLBACK > xforceinline
        bool getExlusive( T_KEY Key, T_CALLBACK&& CallBack = [](T_VALUE&)constexpr noexcept{} ) noexcept
        {
            return find(Key,[&](entry& E){ CallBack(E.m_Value); });
        }

        xforceinline
        T_VALUE get( T_KEY Key ) noexcept
        {
            T_VALUE Val;
            bool b = find(Key,[&](entry& E){ Val = E.m_Value; });
            xassert(b);
            return Val;
        }

        xforceinline
        T_VALUE get( T_KEY Key ) const noexcept
        {
            T_VALUE Val;
            bool b = find(Key,[&](entry& E){ Val = E.m_Value; });
            xassert(b);
            return Val;
        }

        template< typename T >  xforceinline
        const bool free( T_KEY Key, T&& CallBack = [](entry&)constexpr noexcept{} ) noexcept
        {
            assert(Key.isValid());

            auto&           MapEntry    = m_Map[Key.m_Value % max_size_v];
            xcore::lock::scope Lk( MapEntry.m_Lock );

            xassert( MapEntry.m_pEntry != nullptr );
            for( auto p = &MapEntry.m_pEntry; *p; p = &(*p)->m_pNext )
            {
                auto& E = **p;
                if( E.m_Key == Key ) 
                {
                    CallBack( E );
                    *p = E.m_pNext;
                    E.m_pLock.store( nullptr, std::memory_order_release );
                    return true;
                }
            }

            return false;
        }

        xcore::unique_span<lock>    m_Map{};
    };

    //---------------------------------------------------------------------------------
    // TOOLS::PAGE
    //---------------------------------------------------------------------------------
    struct alignas(128) page
    {
        std::byte    m_RawData[1024*16];
    };

    //---------------------------------------------------------------------------------
    // TOOLS::PAGE MANAGER
    //---------------------------------------------------------------------------------
    struct page_mgr
    {
        struct next
        {
            next* m_pNext;
        };

        page_mgr( void ) noexcept
        {
            XCORE_PERF_PLOT_CONFIG("Total Pages", tracy::PlotFormatType::Number )
            XCORE_PERF_PLOT_CONFIG("Free Pages",  tracy::PlotFormatType::Number )
        }

        ~page_mgr( void ) noexcept
        {
            clear();
            XCORE_PERF_PLOT("Total Pages", static_cast<int64_t>(m_TotalPages.load(std::memory_order_relaxed)) )
            XCORE_PERF_PLOT("Free Pages",  static_cast<int64_t>(m_FreePages.load(std::memory_order_relaxed)) )
        }

        void clear( void ) noexcept
        {
            std::uint32_t i=0;
            auto p =reinterpret_cast<next*>(m_pFreeHead.load(std::memory_order_relaxed));
            while( p )
            {
                auto n = p->m_pNext;
                delete p;
                p = n;
                i++;
            }
            m_pFreeHead.store(nullptr, std::memory_order_relaxed);
            m_TotalPages -= i;
            m_FreePages  -= i;
        }

        xforceinline
        page* AllocPage( void ) noexcept
        {
            next* pLocal = m_pFreeHead.load(std::memory_order_relaxed);
            do 
            {
                if(pLocal == nullptr)
                {
                    m_TotalPages++;
                    XCORE_PERF_PLOT("Total Pages", static_cast<int64_t>(m_TotalPages.load(std::memory_order_relaxed)) )
                    return new page;
                }
            } while( m_pFreeHead.compare_exchange_weak( pLocal, pLocal->m_pNext) == false );

            m_FreePages--;
            XCORE_PERF_PLOT("Free Pages",  static_cast<int64_t>(m_FreePages.load(std::memory_order_relaxed)) )
            return reinterpret_cast<page*>(pLocal);
        }

        xforceinline
        void FreePage(page* pPage) noexcept
        {
            next* p      = reinterpret_cast<next*>(pPage);
            p->m_pNext   = m_pFreeHead.load(std::memory_order_relaxed);
            while (m_pFreeHead.compare_exchange_weak(p->m_pNext, p) == false);
            m_FreePages++;
            XCORE_PERF_PLOT("Free Pages",  static_cast<std::int64_t>(m_FreePages.load(std::memory_order_relaxed)) )
        }

        void PreallocatePages( int nPages ) noexcept
        {
            for( int i=0; i<nPages; ++i ) FreePage( new page );
            XCORE_PERF_PLOT("Total Pages", static_cast<int64_t>(m_TotalPages.load(std::memory_order_relaxed)) )
            XCORE_PERF_PLOT("Free Pages",  static_cast<int64_t>(m_FreePages.load(std::memory_order_relaxed)) )
        }

        std::atomic<next*>          m_pFreeHead     { nullptr };
        std::atomic<std::uint32_t>  m_TotalPages    { 0 };
        std::atomic<std::uint32_t>  m_FreePages     { 0 };
    };

    //---------------------------------------------------------------------------------
    // TOOLS::SUB PAGE MANAGER
    //---------------------------------------------------------------------------------
    // Allocates pages and sub-dive them into subpages of size T_CUSTOM_PAGE_SIZE + sizeof(int16)
    // All the subpages of a page belong to a page_info. A page_info can have all its sub_pages allocated
    // , free, or partially free. We always want to minimize how many pages/page_infos we have.
    // To solve that problem we want to prioritize allocation of subpages from page_infos that are as full
    // as possible but not full. So we keep page_infos more or less sorted from 100% full to 100% empty. 
    // We know where the highest priority sub_page is base on m_iMin. 
    // Allocation and deallocation of subpages can be consider to be O(1)
    // TODO: Rather than a log table we may want a random distribution function. 
    //       So we prioritize both ends of the curve. 
    //---------------------------------------------------------------------------------
    template< std::size_t T_CUSTOM_PAGE_SIZE, std::size_t T_ALIGNMENT = 8 >
    struct subpage_mgr
    {
        static_assert( T_ALIGNMENT >= 2 );

        union alignas(T_ALIGNMENT) subpage
        {
            struct 
            {
                std::uint16_t   m_iNext;
            };

            struct
            {
                std::byte       m_RawData[T_CUSTOM_PAGE_SIZE];
                std::uint16_t   m_iPageInfo;
            };
        };
        static_assert( T_CUSTOM_PAGE_SIZE < sizeof(subpage) );

        constexpr static std::size_t num_subpages_per_page_v = sizeof(page) / sizeof(subpage);
        static_assert(num_subpages_per_page_v > 0);

        struct page_info
        {
            page*          m_pPage;             // pointer to the root of the page
            std::uint16_t  m_iFreeHead;         // link list of subpages
            std::uint16_t  m_nFree;             // number of sub pages free
            std::uint16_t  m_iNext;             // double link list of pages in the same log slot
            std::uint16_t  m_iPrev;
        };

        using log_table_t = std::array<std::uint16_t,xcore::bits::Log2IntRoundUp(num_subpages_per_page_v)+1>;

        struct stats
        {
            std::uint32_t   m_TotalPages;
            std::uint32_t   m_TotalSubpages;
            std::uint32_t   m_nAllocSubpages;
            std::uint32_t   m_nFreeSubpages;
            float           m_FragmentationRatePerCentage;
            float           m_WastedMemoryPerPagePerCentage;
            std::int32_t    m_WastedMemory;
            log_table_t     m_Distribution;
        };

        page_mgr&                                   m_PageMgr;
        xcore::lock::spin                           m_Lock      {};
        std::vector<page_info>                      m_lPageInfos{};
        std::uint32_t                               m_nFreeNodes{0u};
        std::uint16_t                               m_iMin      {0u};
        log_table_t                                 m_LogTable  {[]() constexpr { log_table_t A{}; for( auto& x : A ) x = std::uint16_t(~0); return A; }()};
        
        

        constexpr subpage_mgr( page_mgr& Mgr ) noexcept : m_PageMgr{Mgr} {}
        
        stats getStats( void ) const noexcept
        {
            stats Stats;
            Stats.m_TotalPages          = static_cast<std::uint32_t>(m_lPageInfos.size());
            Stats.m_TotalSubpages       = static_cast<std::uint32_t>(Stats.m_TotalPages * num_subpages_per_page_v);
            Stats.m_WastedMemory        = static_cast<std::uint32_t>(Stats.m_TotalPages * (sizeof(page) - num_subpages_per_page_v*T_CUSTOM_PAGE_SIZE));
            Stats.m_nFreeSubpages       = m_nFreeNodes;
            Stats.m_nAllocSubpages      = Stats.m_TotalSubpages - Stats.m_nFreeSubpages;
            Stats.m_FragmentationRatePerCentage     = Stats.m_TotalPages / (float(Stats.m_nAllocSubpages) / num_subpages_per_page_v);
            Stats.m_WastedMemoryPerPagePerCentage   = float(sizeof(page) - num_subpages_per_page_v*T_CUSTOM_PAGE_SIZE)/sizeof(page);

            for( std::size_t i=0; i<Stats.m_Distribution.size(); ++i )
            {
                Stats.m_Distribution[i] = 0;
                for( auto I = m_LogTable[i]; I != 0xffffu; I = m_lPageInfos[I].m_iNext )
                    Stats.m_Distribution[i]++;
            }

            return Stats;
        }


        void RemovePageInfo( const uint16_t iPageInfo ) noexcept
        {
            page_info& PageInfoEntry = m_lPageInfos[iPageInfo];

            xassert(PageInfoEntry.m_nFree == num_subpages_per_page_v);

            // Remove from the global pool
            m_PageMgr.FreePage(PageInfoEntry.m_pPage);
            m_nFreeNodes -= num_subpages_per_page_v;

            // Unlink from previous power of 2
            if (m_LogTable.back() == iPageInfo)
            {
                m_LogTable.back() = PageInfoEntry.m_iNext;
                if (PageInfoEntry.m_iNext != 0xffff) m_lPageInfos[PageInfoEntry.m_iNext].m_iPrev = 0xffff;
            } 
            else
            {
                if (PageInfoEntry.m_iNext!=0xffffu) m_lPageInfos[PageInfoEntry.m_iNext].m_iPrev = PageInfoEntry.m_iPrev;
                if (PageInfoEntry.m_iPrev!=0xffffu) m_lPageInfos[PageInfoEntry.m_iPrev].m_iNext = PageInfoEntry.m_iNext;
            }
            
           // Move the last page_info to replace the current one
           const auto iLastEntry = m_lPageInfos.size()-1; 
           if( iPageInfo != iLastEntry)
           {
                // Officially move it
                m_lPageInfos[iPageInfo] = std::move(m_lPageInfos[iLastEntry]);

                // Reset the subpages pageinfo to point to the new index
                {
                    subpage* pCustomPage = reinterpret_cast<subpage*>(PageInfoEntry.m_pPage);
                    for (std::uint16_t i=0u; i<num_subpages_per_page_v; i++)
                    {
                        xassert(pCustomPage[i].m_iPageInfo==iLastEntry);
                        pCustomPage[i].m_iPageInfo  = iPageInfo;
                    }
                }

                // Fix its dependencies in the link list
                if (PageInfoEntry.m_iNext!=0xffffu) m_lPageInfos[PageInfoEntry.m_iNext].m_iPrev = iPageInfo;
                if (PageInfoEntry.m_iPrev!=0xffffu) m_lPageInfos[PageInfoEntry.m_iPrev].m_iNext = iPageInfo;

                // fix the log table if we need to
                if (PageInfoEntry.m_iPrev==0xffffu)
                {
                    // Find the last node in the log table and change its id
                    for( auto& E : m_LogTable )
                        if( E == iLastEntry )
                        {
                            E = iPageInfo;
                            break;
                        }
                }
           }

           // Delete the page info officially
           m_lPageInfos.pop_back();
        }

        void MovePageInfoToNewLogLocation( const uint16_t iLogPrev, const uint16_t iLog, const uint16_t iPageInfo ) noexcept
        {
            page_info& PageInfoEntry = m_lPageInfos[iPageInfo];

            // Unlink from previous power of 2
            if (m_LogTable[iLogPrev] == iPageInfo)
            {
                m_LogTable[iLogPrev] = PageInfoEntry.m_iNext;
                if (PageInfoEntry.m_iNext != 0xffff) m_lPageInfos[PageInfoEntry.m_iNext].m_iPrev = 0xffff;
            } 
            else
            {
                if(PageInfoEntry.m_iNext!=0xffffu) m_lPageInfos[PageInfoEntry.m_iNext].m_iPrev = PageInfoEntry.m_iPrev;
                if(PageInfoEntry.m_iPrev!=0xffffu) m_lPageInfos[PageInfoEntry.m_iPrev].m_iNext = PageInfoEntry.m_iNext;
            }

            // Link in the new Log Entry
            PageInfoEntry.m_iPrev    = 0xffff;
            PageInfoEntry.m_iNext    = m_LogTable[iLog];
            m_LogTable[iLog]         = iPageInfo;
            if (PageInfoEntry.m_iNext != 0xffff) m_lPageInfos[PageInfoEntry.m_iNext].m_iPrev = iPageInfo;
        }

        subpage* AllocCustomPage( void ) noexcept
        {
            xcore::lock::scope Lk(m_Lock);

            if( m_nFreeNodes == 0u )
            {
                m_lPageInfos.emplace_back
                ( page_info{
                    
                        m_PageMgr.AllocPage()
                    ,   1u
                    ,   xcore::types::static_cast_safe<std::uint16_t>(num_subpages_per_page_v-1u)
                    ,   0xffffu
                    ,   0xffffu 
                });
                auto&   PageInfoEntry   =  m_lPageInfos.back();
                auto    pCustomPage     = reinterpret_cast<subpage*>(PageInfoEntry.m_pPage);
                auto    iPageInfo       = xcore::types::static_cast_safe<std::uint16_t>(m_lPageInfos.size()-1);

                // Set the total count of free subpages
                m_nFreeNodes            = xcore::types::static_cast_safe<std::uint16_t>(num_subpages_per_page_v-1);

                // Insert page Info into the correct LogTable Entry
                {
                    static constexpr auto   iLog            = xcore::bits::Log2IntRoundUp(num_subpages_per_page_v-1);
                    auto&                   LogTableEntry   = m_LogTable[iLog];

                    m_iMin = iLog;
                    if( LogTableEntry != 0xffffu ) m_lPageInfos[LogTableEntry].m_iPrev = iPageInfo;
                    PageInfoEntry.m_iNext   = LogTableEntry;
                    LogTableEntry           = iPageInfo;
                }

                // Link list all the entries
                for( std::uint16_t i=1u; i<(num_subpages_per_page_v-1); i++ )
                {
                    pCustomPage[i].m_iNext      = i+1;
                    pCustomPage[i].m_iPageInfo  = iPageInfo;
                }
                pCustomPage[num_subpages_per_page_v-1].m_iNext      = 0xffffu;
                pCustomPage[num_subpages_per_page_v-1].m_iPageInfo  = iPageInfo;
                pCustomPage[0].m_iPageInfo                          = iPageInfo;

                // Ready to give subpage
                return pCustomPage;
            }
            else
            {
                xassert(m_iMin);
                xassert(m_nFreeNodes);
                xassert(m_LogTable[m_iMin] != 0xffff);

                const auto  iPageInfo       = m_LogTable[m_iMin];
                auto&       PageInfoEntry   = m_lPageInfos[ iPageInfo ];

                m_nFreeNodes--;
                PageInfoEntry.m_nFree--;

                const auto iLog = xcore::bits::Log2IntRoundUp(PageInfoEntry.m_nFree);
                if( PageInfoEntry.m_nFree == ((1u<<iLog)-1) )
                {
                    // Unlink from previous power of 2
                    MovePageInfoToNewLogLocation( iLog + 1u, iLog, iPageInfo );
 
                    // Update the new iMin
                    if(PageInfoEntry.m_nFree)
                    {
                        if( iLog < m_iMin ) m_iMin = iLog;
                    }
                    else if( m_nFreeNodes )
                    {
                        xassert(m_iMin==1);
                        while( m_LogTable[m_iMin] == 0xffffu ) m_iMin++;
                    }
                }

                //
                // Get actual custom page
                //
                auto    pCustomPage = reinterpret_cast<subpage*>(&PageInfoEntry.m_pPage->m_RawData[ sizeof(subpage) * PageInfoEntry.m_iFreeHead ] );
                PageInfoEntry.m_iFreeHead = pCustomPage->m_iNext;

                return pCustomPage;
            }
        }

        void FreeCustomPage( subpage* pCustomPage ) noexcept
        {
            xcore::lock::scope Lk(m_Lock);

            const auto  iPageInfo       = pCustomPage->m_iPageInfo;
            auto&       PageInfoEntry   = m_lPageInfos[iPageInfo];

            pCustomPage->m_iNext        = PageInfoEntry.m_iFreeHead;
            PageInfoEntry.m_iFreeHead   = xcore::types::static_cast_safe<std::uint16_t>(static_cast<size_t>(pCustomPage->m_RawData - PageInfoEntry.m_pPage->m_RawData)/sizeof(subpage));
            PageInfoEntry.m_nFree++;
            xassert( PageInfoEntry.m_nFree <= num_subpages_per_page_v );
            m_nFreeNodes++;

            const auto iLog = xcore::bits::Log2IntRoundUp(PageInfoEntry.m_nFree);
            if (PageInfoEntry.m_nFree == (1u<<(iLog-1)))
            {
                // Unlink from previous power of 2
                const auto iLogPrev = iLog - 1u;
                MovePageInfoToNewLogLocation( iLogPrev, iLog, iPageInfo );
            
                // Update the new iMin
                if( iLog < m_iMin ) m_iMin = iLog;
                else if( m_iMin == iLogPrev && m_LogTable[iLogPrev] == 0xffffu )
                {
                    m_iMin++;
                }
            }

            // remove page when we are done with it
            if( PageInfoEntry.m_nFree == num_subpages_per_page_v )
            {
                RemovePageInfo(iPageInfo);
            }
        }
    };

    //---------------------------------------------------------------------------------
    // TOOLS::VECTOR POOL
    //---------------------------------------------------------------------------------
    template< typename T, std::size_t T_MAX_PER_SUBPAGE, std::size_t T_MAX_ENTRIES >
    struct vector
    {
        using subpage_mgr_t = subpage_mgr< sizeof(T)*T_MAX_PER_SUBPAGE, std::alignment_of_v<T> >;

        constexpr static std::size_t  entries_per_subpage_v = T_MAX_PER_SUBPAGE;
        constexpr static std::size_t  max_subpages_v        = (T_MAX_ENTRIES/entries_per_subpage_v)+1;

        constexpr vector( subpage_mgr_t& SubMgr ) noexcept : m_SubpageMgr(SubMgr) {}

        std::size_t size( void ) const noexcept
        {
            return m_Count.load(std::memory_order_relaxed).m_Count;
        }

        template< typename...T_ARGS >
        T& append( T_ARGS&&...Args ) noexcept
        {
            auto Count = m_Count.load(std::memory_order_relaxed);
            do 
            {
                if( Count.m_bLock ) 
                {
                    while( Count.m_bLock ) 
                    {
                        // spin!!
                        Count = m_Count.load(std::memory_order_relaxed);
                    }
                }

                if( m_nSubpages * entries_per_subpage_v == Count.m_Count )
                {
                    if( false == m_Count.compare_exchange_weak( Count, { Count.m_Count, 1} )) continue;
                    m_lSubpages[m_nSubpages++] = m_SubpageMgr.AllocCustomPage();
                    m_Count.store( { Count.m_Count+1, 0}, std::memory_order_relaxed );
                }
                else
                {
                    if( false == m_Count.compare_exchange_weak( Count, { Count.m_Count + 1, 0} )) continue;
                }

                break;

            } while (true);
                
            const auto iSubpage  = Count.m_Count / entries_per_subpage_v;
            const auto iIndex    = Count.m_Count % entries_per_subpage_v;
            xassert( Count.m_Count < T_MAX_ENTRIES );
            return *new( &m_lSubpages[iSubpage]->m_RawData[sizeof(T)*iIndex] ) T{ std::forward<T_ARGS>(Args)... };
        }

        void eraseSwap( std::uint32_t Index ) noexcept
        {
            auto Count = m_Count.load(std::memory_order_relaxed);
            do 
            {
                if( Count.m_bLock ) 
                {
                    while( Count.m_bLock ) 
                    {
                        // spin!!
                        Count = m_Count.load(std::memory_order_relaxed);
                    }
                }

                // always lock on destruction
                if( false == m_Count.compare_exchange_weak( Count, { Count.m_Count, 1} )) continue;

                const auto NewCount     = Count.m_Count - 1;
                const auto iLastSubpage = NewCount/entries_per_subpage_v;
                const auto LastOffset   = sizeof(T)*(NewCount%entries_per_subpage_v);
                if( Index != NewCount )
                {
                    auto& p1 = *reinterpret_cast<T*>(&m_lSubpages[iLastSubpage]->m_RawData[LastOffset]);
                    auto& p2 = *reinterpret_cast<T*>(&m_lSubpages[Index/entries_per_subpage_v]->m_RawData[sizeof(T)*(Index%entries_per_subpage_v)]);
                    p2 = std::move(p1);
                }

                if constexpr (false == std::is_trivially_destructible_v<T>)
                    std::destroy_at(reinterpret_cast<T*>(&m_lSubpages[iLastSubpage]->m_RawData[LastOffset]));

                if( LastOffset == 0 )
                {
                    --m_nSubpages;
                    m_SubpageMgr.FreeCustomPage( m_lSubpages[m_nSubpages] );
                }

                m_Count.store( { NewCount, 0}, std::memory_order_relaxed );
                break;

            } while (true);
        }

        void DeleteAllEntries( void ) noexcept
        {
            auto Count = m_Count.load(std::memory_order_relaxed);
            do 
            {
                if( Count.m_bLock ) 
                {
                    while( Count.m_bLock ) 
                    {
                        // spin!!
                        Count = m_Count.load(std::memory_order_relaxed);
                    }
                }

                // always lock on destruction
                if( false == m_Count.compare_exchange_weak( Count, { Count.m_Count, 1} )) continue;

                for (int iSubpage = 0,iEntry=entries_per_subpage_v; iSubpage<m_nSubpages; ++iSubpage,iEntry += entries_per_subpage_v)
                {
                    auto& Subpage = *m_lSubpages[iSubpage];

                    if constexpr (false == std::is_trivially_destructible_v<T>)
                    {
                        if (iEntry < Count.m_Count)
                        {
                            std::destroy(&reinterpret_cast<T*>(&Subpage.m_RawData[0])
                                         ,&reinterpret_cast<T*>(&Subpage.m_RawData[sizeof(T)*entries_per_subpage_v]));
                        } 
                        else
                        {
                            std::destroy(&reinterpret_cast<T*>(&Subpage.m_RawData[0])
                                         ,&reinterpret_cast<T*>(&Subpage.m_RawData[sizeof(T)*(Count.m_Count%entries_per_subpage_v)]));
                        }
                    }

                    m_SubpageMgr.FreeCustomPage(&Subpage);
                }

                m_Count.store( { 0, 0}, std::memory_order_relaxed );
                break;

            } while (true);
        }

        struct count
        {
            std::uint32_t           m_Count:31
            ,                       m_bLock:1;
        };

        using subpages_list_t = std::array< typename subpage_mgr_t::subpage*, max_subpages_v >;

        subpage_mgr_t&              m_SubpageMgr;
        std::atomic<count>          m_Count         { count{0, 0} };
        std::uint16_t               m_nSubpages     { 0 };
        subpages_list_t             m_lSubpages     {};
    };
    
    //---------------------------------------------------------------------------------
    // TOOLS::POOL
    //---------------------------------------------------------------------------------
    template< typename T, std::size_t MAX_PAGES >
    struct pool;

    namespace details
    {
        template< typename T, std::size_t MAX_PAGES >
        struct pool_iterator
        {
            using pool_t = pool<T,MAX_PAGES>;

            pool_t*             m_pPool;
            std::uint32_t       m_nEntries;
            std::uint16_t       m_nPages;
            std::uint16_t       m_Index;
            pool_iterator   operator    ++  (void)                   noexcept;
            bool            operator    !=  (std::nullptr_t) const   noexcept;
            T&              operator    *   (void)                   noexcept;
        };
    }
        
    template< typename T, std::size_t MAX_PAGES >
    struct pool
    {
        constexpr static auto entries_per_page_v = sizeof(page)/sizeof(T);

        struct alignas(std::uint64_t) counts
        {
            std::uint32_t   m_nEntries;
            std::uint16_t   m_nPages;
            std::uint16_t   m_bLock;
        };

        constexpr pool( page_mgr& PageMgr ) noexcept : m_PageMgr{PageMgr}{}

        std::uint32_t                           size() const noexcept{ return m_Counts.load( std::memory_order_relaxed).m_nEntries; }
        details::pool_iterator<T,MAX_PAGES>     begin() noexcept{ counts Counts = m_Counts.load( std::memory_order_relaxed); return { this, Counts.m_nEntries, 0, 0 }; }
        std::nullptr_t                          end() const noexcept { return nullptr; }

        template< typename...T_ARGS >
        xforceinline T& append( T_ARGS&&... Args ) noexcept
        {
            auto Counts    = m_Counts.load( std::memory_order_relaxed);
            do 
            {
                if( Counts.m_bLock )
                {
                    Counts = m_Counts.load( std::memory_order_relaxed );
                }
                else if( Counts.m_nEntries < Counts.m_nPages*entries_per_page_v )
                {
                    const counts NewCounts{ Counts.m_nEntries+1, Counts.m_nPages, 0 }; 
                        
                    if( m_Counts.compare_exchange_weak( Counts, NewCounts ) )
                        return *new( &m_lPages[Counts.m_nPages-1]->m_RawData[(Counts.m_nEntries%entries_per_page_v)*sizeof(T)] ) 
                                    T{std::forward<T_ARGS>(Args)...};
                }
                else if( Counts.m_nEntries == Counts.m_nPages*entries_per_page_v )
                {
                    counts NewCounts{ Counts.m_nEntries+1, static_cast<std::uint16_t>(Counts.m_nPages+1), 1u }; 
                    if( m_Counts.compare_exchange_weak( Counts, NewCounts ) )
                    {
                        auto p = m_PageMgr.AllocPage();
                        m_lPages[Counts.m_nPages] = p;
                        auto& E = *new( &p->m_RawData[(Counts.m_nEntries%entries_per_page_v)*sizeof(T)] ) 
                                    T{std::forward<T_ARGS>(Args)...};

                        // Release the lock
                        NewCounts.m_bLock = 0;
                        m_Counts.store( NewCounts );

                        return E;
                    }
                }

                // Spin Lock...
            } while (true);

            xassert(false); //-V779
        }

        void clear( void ) noexcept
        {
            counts Counts    = m_Counts.load( std::memory_order_relaxed);
            do 
            {
                if( Counts.m_bLock )
                {
                    Counts = m_Counts.load( std::memory_order_relaxed );
                }
                else if( m_Counts.compare_exchange_weak( Counts, counts{ Counts.m_nEntries, Counts.m_nPages, 1 } ) )
                {
                    if constexpr (false == std::is_trivially_destructible_v<T>)
                    {
                        for( std::uint32_t i=0; i<Counts.m_nEntries; ++i )
                        {
                            std::destroy_at
                            (
                                reinterpret_cast<T*>(&m_lPages[i/entries_per_page_v]->m_RawData[i%entries_per_page_v])
                            );
                        }                        
                    }

                    for( int i=0; i<Counts.m_nPages; ++i )
                    {
                        m_PageMgr.FreePage(m_lPages[i]);
                    }

                    m_Counts.store( counts{ 0, 0, 0 } );
                    break;
                }

                // Spin Lock...
            } while (true);
        }

        std::atomic<counts>             m_Counts    {counts{0,0,0}};
        std::array<page*, MAX_PAGES>    m_lPages    {};
        page_mgr&                       m_PageMgr;
    };

    namespace details
    {
        template< typename T, std::size_t MAX_PAGES >
        xforceinline pool_iterator<T,MAX_PAGES> pool_iterator<T,MAX_PAGES>::operator++ (void) noexcept
        {
            m_nEntries--;
            m_Index++;
            if (m_Index == pool_t::entries_per_page_v)
            {
                m_nPages++;
                m_Index = 0;
                _mm_prefetch((const char*)m_pPool->m_lPages[m_nPages], _MM_HINT_NTA);
            }
            return *this;
        }

        template< typename T, std::size_t MAX_PAGES >
        xforceinline bool pool_iterator<T,MAX_PAGES>::operator !=  (std::nullptr_t) const noexcept
        {
            return m_nEntries;
        }

        template< typename T, std::size_t MAX_PAGES >
        xforceinline T& pool_iterator<T,MAX_PAGES>::operator * (void) noexcept
        {
            return reinterpret_cast<T*>(m_pPool->m_lPages[m_nPages])[m_Index];
        }
    }

    //---------------------------------------------------------------------------------
    // TOOLS::COMMAND POOL
    //---------------------------------------------------------------------------------
    template< typename T_CMD_VARIANT >
    struct cmd_pool
    {
        struct cmd
        {
            T_CMD_VARIANT   m_Cmd;
            cmd*            m_pNext;
        };

        cmd_pool( page_mgr& PageMgr ) : m_PageMgr{PageMgr} 
        {
        }

        template< typename T, typename...T_ARGS >
        T& NewAlloc( T_ARGS...Args ) noexcept
        {
            auto    Position    = m_Position.load(std::memory_order_relaxed);
        START:
                
            // Check if we are locked... 
            while( Position < sizeof(page*) ) 
            {
                // If one means we have not initialize... so we begin the race of whom will initialize it
                if( Position == 1 )
                {
                    // Try to win the right to do it
                    if( m_Position.compare_exchange_weak( Position, 2 ) )
                    {
                        m_pPageHead = m_PageMgr.AllocPage();
                        reinterpret_cast<page*&>(*m_pPageHead) = nullptr;
                        Position    = sizeof(page*);
                        m_Position.store( Position );
                        break;
                    }
                }

                // See if someone has unlock us
                Position = m_Position.load(std::memory_order_relaxed);
            }

            auto* p = xcore::bits::Align( &m_pPageHead->m_RawData[Position], std::alignment_of_v<T> );
            if( (p + sizeof(T)) > &m_pPageHead->m_RawData[sizeof(page)] )
            {
                // Lock the command pool
                if( m_Position.compare_exchange_weak( Position, 0 ) == false ) goto START;

                // Need new page
                auto  pNewPage = m_PageMgr.AllocPage();
                auto& pNewNext = reinterpret_cast<page*&>(pNewPage->m_RawData);
                pNewNext    = m_pPageHead;
                m_pPageHead = pNewPage;

                // Allocate our entry
                p                = xcore::bits::Align( &pNewPage->m_RawData[sizeof(page*)], std::alignment_of_v<T> );
                auto NewPosition = xcore::types::static_cast_safe<std::uint32_t>( p - m_pPageHead->m_RawData ) + static_cast<std::uint32_t>(sizeof(T));

                // Release the lock and set the new position
                m_Position.store( NewPosition );
            }
            else
            {
                auto NewPosition  = xcore::types::static_cast_safe<std::uint32_t>( p - m_pPageHead->m_RawData ) + static_cast<std::uint32_t>(sizeof(T));
                if( m_Position.compare_exchange_weak( Position, NewPosition ) == false ) goto START;
            }

            return *new(p) T{ std::forward<T_ARGS>(Args)... };
        }

        template< typename T, typename...T_ARGS >
        T& NewCmd( T_ARGS...Args ) noexcept
        {
            auto& Cmd       = NewAlloc<cmd>();
            auto& Requested = Cmd.m_Cmd.emplace<T>( std::forward<T_ARGS>(Args)... );
            Cmd.m_pNext = m_pCmdHead.load( std::memory_order_relaxed );
            while( false == m_pCmdHead.compare_exchange_weak( Cmd.m_pNext, &Cmd ) );
            return Requested;
        }

        void clear( void )
        {
            // We keep one page and return the rest 
            if(m_pPageHead) for( auto p = reinterpret_cast<page**>(m_pPageHead->m_RawData); *p; p = &(*p) )
            {
                m_PageMgr.FreePage(*p);
            }

            m_pCmdHead.store(nullptr,std::memory_order_relaxed);
            m_Position.store(sizeof(page*),std::memory_order_relaxed);
        }

        page_mgr&                           m_PageMgr;
        page*                               m_pPageHead     { nullptr };
        std::atomic<cmd*>                   m_pCmdHead      { nullptr };
        std::atomic<std::uint32_t>          m_Position      {1};                // 1 means that needs to be initialize
    };
}
