
namespace mecs::delegate
{
    //---------------------------------------------------------------------------------
    // DELEGATE:: GUID
    //---------------------------------------------------------------------------------
    using guid      = xcore::guid::unit<64,struct mecs_delegate_tag>;

    //---------------------------------------------------------------------------------
    // DELEGATE:: DETAILS::
    //---------------------------------------------------------------------------------
    namespace details
    {
        //---------------------------------------------------------------------------------
        // DELEGATE:: DETAILS:: BASE
        // base class for the delegates and contains the world's events
        //---------------------------------------------------------------------------------
        struct base
        {
            virtual                        ~base            ( void ) noexcept = default;
            virtual                 void    Disable         ( void ) noexcept = 0;
            virtual                 void    Enable          ( void ) noexcept = 0;
            xforceinline            void    msgWorldStart   ( world::instance& World ) noexcept {}
            xforceinline            void    msgFrameStart   ( void ) noexcept {}
            xforceinline            void    msgFrameDone    ( void ) noexcept {}
        };

        //---------------------------------------------------------------------------------
        // DELEGATE:: DETAILS:: INSTANCE_BASE
        // This is the base class for any group based delegate
        //---------------------------------------------------------------------------------

        struct instance_base : base
        {
            virtual void AttachToEvent       ( world::instance& World )  noexcept = 0;
        };

        template< typename T_EVENT, typename T_EVENT2 = typename T_EVENT::event_t::arguments_tuple_t >
        struct instance_base_harness;

        template< typename T_EVENT, typename...T_ARGS >
        struct instance_base_harness< T_EVENT, std::tuple<T_ARGS...> > : instance_base
        {
            virtual void AttachToEvent      ( world::instance& World )  noexcept = 0;

            // For the system
            using                   base_t              = instance_base_harness;
            using                   event_t             = T_EVENT;

            // Overridable
            constexpr static auto   name_v              = xconst_universal_str("mecs::event::delegate(instance_base)");

            // Expected 
            //      constexpr static guid        guid_v { ... }

            xforceinline            void    msgHandleSystemEvents  ( T_ARGS... ) noexcept {}
        };

        //---------------------------------------------------------------------------------
        // DELEGATE:: DETAILS:: GROUP_INSTANCE
        // This is the base class for any group based delegate
        //---------------------------------------------------------------------------------

        struct group_base : base
        {
            query::instance         m_GroupQuery;
            virtual void InitializeGroupQuery( world::instance& World )                                     noexcept = 0;
            virtual void AttachToGroup       ( world::instance& World, component::group::instance& Group )  noexcept = 0;
        };

        template< typename T_EVENT, typename T_EVENT2 = typename T_EVENT::event_t::arguments_tuple_t >
        struct group_instance;

        template< typename T_EVENT, typename...T_ARGS >
        struct group_instance< T_EVENT, std::tuple<T_ARGS...> > : group_base
        {
            static_assert(event::details::template EventContract<T_EVENT>());

            // For the system
            using                   base_t              = group_instance;
            using                   event_t             = T_EVENT;

            // Overridable
            constexpr static auto   name_v              = xconst_universal_str("mecs::event::delegate(group_instance)");
            using                   query_t             = query::define<>;
            constexpr static auto   max_attachments_v   = 32;

            // Expected 
            //      constexpr static guid        guid_v { ... }

            xforceinline            void    msgHandleSystemEvents  ( T_ARGS... ) noexcept {}
        };
    }

    //---------------------------------------------------------------------------------
    // DELEGATE:: INSTANCE
    // Base on the event type we will create the correct base
    //---------------------------------------------------------------------------------
    template< typename T_EVENT >
    using instance = std::conditional_t
    <   std::is_base_of_v<event::details::group_event, T_EVENT>
    ,   details::group_instance<T_EVENT>
    ,   details::instance_base_harness<T_EVENT>
    >; 

    //---------------------------------------------------------------------------------
    // DELEGATE:: DETAILS::
    //---------------------------------------------------------------------------------
    namespace details
    {
        //---------------------------------------------------------------------------------
        // DELEGATE:: DETAILS:: DELEGATE CONTRACT
        //---------------------------------------------------------------------------------
        template< typename T_DELEGATE >
        constexpr bool DelegateContract() noexcept
        {
          //  static_assert( std::is_based_of_v< base, T_DELEGATE >
          //                 , "Make sure your delegate is derived from mecs::delegate::instance" );

            static_assert(event::details::template EventContract<T_DELEGATE::event_t>());

            static_assert(std::is_same_v<decltype(T_DELEGATE::guid_v),const delegate::guid >
                            ,"Make sure your delegate has defined a guid_v of the right type (mecs::delegate::guid)");

            static_assert(std::is_same_v<decltype(T_DELEGATE::name_v),const xcore::string::const_universal >
                            ,"Make sure your delegate has a defined a name_v of the right type (xcore::string::const_universal)");

            static_assert(T_DELEGATE::guid_v.isValid()
                            ,"Make sure your delegate has a valid guid_v. Did you know you can initialize them with a string?");

            return true;
        };

        //---------------------------------------------------------------------------------
        // DELEGATE:: DETAILS:: CALL FUNCTION
        //---------------------------------------------------------------------------------
        template< typename T_CUSTOM_INSTANCE, typename...T_FINAL_ARGS, typename...T_ADDITIONAL_ARGS >
        constexpr xforceinline static void CallFunction
        ( 
                T_CUSTOM_INSTANCE&              Instance
            ,   std::tuple<T_FINAL_ARGS...>* 
            ,   entity::instance&               Entity
            ,   system::instance&               Sys
            ,   T_ADDITIONAL_ARGS&&...          ExtraArgs
        ) noexcept
        {
            if constexpr( (sizeof...(T_FINAL_ARGS) - sizeof...(ExtraArgs)) != 0 )
            {
                auto TmpRef = Entity.getTmpRef();
                Instance
                ( 
                        Sys
                    ,   std::forward(ExtraArgs)...
                    ,   TmpRef.getComponent<xcore::types::decay_full_t<T_FINAL_ARGS>>() ... 
                );
            }
            else
            {
                Instance( Sys, std::forward(ExtraArgs)... );
            }
        }

        //---------------------------------------------------------------------------------
        template< typename T >
        struct function_helper;

        template< typename...T_ARGS >
        struct function_helper< std::tuple<T_ARGS...> >
        {
            void operator() ( T_ARGS... ) {};
        };


        //---------------------------------------------------------------------------------
        // DELEGATE:: DETAILS:: MAKE CUSTOM INSTANCE
        //---------------------------------------------------------------------------------
        template< typename T_WORLD, typename T_USER_DELEGATE, typename T_TYPE, typename...T_ARGS >
        struct make_custom_group_instance;

        template< typename T_WORLD, typename T_USER_DELEGATE, typename...T_ARGS >
        struct make_custom_group_instance< T_WORLD, T_USER_DELEGATE, event::details::group_event, std::tuple<T_ARGS...> > final : T_USER_DELEGATE
        {
            using                                               self_t              = make_custom_group_instance;
            using                                               world_instance_t    = T_WORLD;
            using                                               user_delegate_t     = T_USER_DELEGATE;
            using                                               type_t              = event::details::group_event;
            using                                               event_t             = typename user_delegate_t::event_t;
            constexpr static typename user_delegate_t::query_t  query_v{};

            static_assert(details::template DelegateContract<T_USER_DELEGATE>());

            virtual void    Disable (void) noexcept override 
            {
            }

            virtual void    Enable  (void) noexcept override
            {
            }

            virtual void InitializeGroupQuery( T_WORLD& World ) noexcept override
            {
                if constexpr (&user_delegate_t::msgHandleSystemEvents != &user_delegate_t::base_t::msgHandleSystemEvents )
                {
                    user_delegate_t::InitializeGroupQuery( World );
                }
                else
                {
                    using args = xcore::types::tuple_delete_n_t< 1u, xcore::function::traits<self_t>::args_tuple >;
                    if constexpr ( std::is_same_v<args,std::tuple<>> )
                    {
                        World.InitializeGroupQuery
                        < 
                            query::system_function<function_helper<std::tuple<const entity::instance&>>>
                        >( user_delegate_t::m_GroupQuery, query_v );                    
                    }
                    else
                    {
                        World.InitializeGroupQuery
                        < 
                            query::system_function<function_helper<args>>
                        >( user_delegate_t::m_GroupQuery, query_v );                    
                    }
                }
            }

            void HandleMessage( T_ARGS... Args ) noexcept
            {
                if constexpr (&user_delegate_t::msgHandleSystemEvents != &user_delegate_t::base_t::msgHandleSystemEvents )
                {
                    user_delegate_t::msgHandleSystemEvents( std::forward<T_ARGS>(Args)... );
                }
                else
                {
                    static_assert( std::is_same_v< system::instance&, std::tuple_element_t< 0u, xcore::function::traits<self_t>::args_tuple >>,
                                   "Your delegate function should have <<<system::instance&>>> as your first parameter" );
                    CallFunction
                    ( 
                            *this
                        ,   reinterpret_cast
                            < xcore::types::tuple_delete_n_t< 1u, xcore::function::traits<self_t>::args_tuple >* >(nullptr) 
                        ,   Args...
                    );
                }
            }

            virtual void AttachToGroup( world::instance& World, component::group::instance& Group ) noexcept
            {
                //
                // Handle the different types of events
                //
                if      constexpr ( user_delegate_t::event_t::guid_v == event::moved_in<void>::guid_v )
                {
                    Group.m_Events.m_MovedInEntity.AddDelegate<&self_t::HandleMessage>( this );
                }
                else if constexpr( user_delegate_t::event_t::guid_v == event::create_entity::guid_v )
                {
                    Group.m_Events.m_CreatedEntity.AddDelegate<&self_t::HandleMessage>( this );
                }
                else if constexpr( user_delegate_t::event_t::guid_v == event::destroy_entity::guid_v )
                {
                    Group.m_Events.m_DeletedEntity.AddDelegate<&self_t::HandleMessage>( this );
                }
                else if constexpr( user_delegate_t::event_t::guid_v == event::moved_out<void>::guid_v )
                {
                    Group.m_Events.m_MovedOutEntity.AddDelegate<&self_t::HandleMessage>( this );
                }
                else if constexpr( user_delegate_t::event_t::guid_v == event::updated_component<void>::guid_v )
                {
                    Group.m_Events.m_UpdateComponent.m_Event.AddDelegate<&self_t::HandleMessage>( this );

                    static const tools::bits Bits{[&]
                    {
                        if(event_t::components_v.size() )
                        {
                            tools::bits Bits{nullptr};
                            for( auto k : event_t::components_v )
                                Bits.AddBit( static_cast<T_WORLD&>(World).m_mapComponentDescriptor.get( k )->m_BitNumber );
                            return Bits;
                        }

                        return tools::bits{xcore::not_null};
                    }()};
                    
                    Group.m_Events.m_UpdateComponent.m_Bits.push_back(Bits);
                }
                else
                {
                    static_assert( xcore::types::always_false_v, "We are not covering all the cases!!" );
                }
            }

            typename world_instance_t::events::world_start::delegate    m_WorldStartDelegate    {       &self_t::msgWorldStart    };
            typename world_instance_t::events::frame_start::delegate    m_FrameStartDelegate    {       &self_t::msgFrameStart    };
            typename world_instance_t::events::frame_done::delegate     m_FrameDoneDelegate     {       &self_t::msgFrameDone     };
        };

        template< typename T_WORLD, typename T_USER_DELEGATE, typename...T_ARGS >
        struct make_custom_group_instance< T_WORLD, T_USER_DELEGATE, event::instance, std::tuple<T_ARGS...> > final : T_USER_DELEGATE
        {
            using                                               self_t              = make_custom_group_instance;
            using                                               world_instance_t    = T_WORLD;
            using                                               user_delegate_t     = T_USER_DELEGATE;
            using                                               type_t              = event::instance;
//            static_assert(details::template DelegateContract<T_USER_DELEGATE>());

            virtual void    Disable (void) noexcept override 
            {
            }

            virtual void    Enable  (void) noexcept override
            {
            }

            void HandleMessage( T_ARGS... Args ) noexcept
            {
                if constexpr (&user_delegate_t::msgHandleSystemEvents != &user_delegate_t::base_t::msgHandleSystemEvents )
                {
                    user_delegate_t::msgHandleSystemEvents( std::forward<T_ARGS>(Args)... );
                }
                else
                {
                    static_assert( std::is_same_v< system::instance&, std::tuple_element_t< 0u, xcore::function::traits<self_t>::args_tuple >>,
                                   "Your delegate function should have <<<system::instance&>>> as your first parameter" );

                    (*this)( std::forward<T_ARGS>(Args) ... );
                }
            }

            virtual void AttachToEvent( T_WORLD& World )  noexcept
            {
                // Connect the delegate to the event
                auto& Event = *reinterpret_cast<mecs::event::type<T_ARGS...>*>(World.m_mapEvents.get( user_delegate_t::event_t::guid_v ));
                Event.AddDelegate<&self_t::HandleMessage>( this );
            }

            typename world_instance_t::events::world_start::delegate    m_WorldStartDelegate    {       &self_t::msgWorldStart    };
            typename world_instance_t::events::frame_start::delegate    m_FrameStartDelegate    {       &self_t::msgFrameStart    };
            typename world_instance_t::events::frame_done::delegate     m_FrameDoneDelegate     {       &self_t::msgFrameDone     };
        };

        template< typename T_USER_DELEGATE, typename T_WORLD = world::instance >
        using custom_instance = make_custom_group_instance
        < 
                T_WORLD
            ,   T_USER_DELEGATE
            ,   std::conditional_t< std::is_base_of_v< event::details::group_event, typename T_USER_DELEGATE::event_t >, event::details::group_event, event::instance >
            ,   typename T_USER_DELEGATE::event_t::event_t::arguments_tuple_t 
        >;
    }
}
