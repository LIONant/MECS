namespace mecs::examples::E07_hierarchy_components
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------

    struct position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
    };

    struct local_position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"local_position component"};
    };

    struct parent : mecs::component::shared
    {
        constexpr static mecs::type_guid type_guid_v{"parent component"};
        mecs::entity::guid          m_Parent;
        mecs::component::entity     m_NextSibling;
        std::uint16_t               m_DepthValue;
        constexpr static std::uint64_t getKey( parent* p ) noexcept { return p->m_Value; }
    };

    struct children
    {
        constexpr static mecs::type_guid type_guid_v{"children component"};
        mecs::component::entity     m_FirstChild;
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_with_parent_system : mecs::system::instance
    {
        using mecs::system::instance::instance;
    
        void msgUpdate()
        {
            DoDefaultQuery();
            auto& Query = getDefaultQuery();

            // Merge Groups into a single array

            // Go throw the array of groups

        }

        void operator() ( position& Position, local_position& Local, const parent& Parent ) const noexcept
        {
            Local.m_X += 1.0f;
            Local.m_Y += 1.0f;

            getEntity( Parent, [&]( mecs::entity::tmp_ref Ref )
            {
                auto& ParentPosition = Ref.getComponent<position>();
                Position = ParentPosition + Local;
            });
        }
    };

    struct move_system : mecs::system::instance
    {
        using mecs::system::instance::instance;

        using query_t = mecs::query::define
        <
            mecs::query::none<parent>
        >;
    
        void operator() ( position& Position ) const noexcept
        {
            Position.m_X += 1.0f;
            Position.m_Y += 1.0f;
        }
    };

    //-----------------------------------------------------------------------------------------

    struct mutate_parent_delegate : mecs::event::delegate
    {
        using query_t = mecs::query::define
        <
            mecs::query::events<mecs::events::mutate_component, parent>
        >;

        // This function will be call ones per entity. Since we only will have one entity it will only be call ones per frame.
        void operator() ( mecs::system::instance& System, const parent& Parent, depth& Depth ) const noexcept
        {
            if( Parent.isValid() )
            {
                System.getEntity( Parent, [&]( mecs::entity::tmp_ref Ref )
                {
                    Depth.m_Value = 1 + Ref.getComponent<depth>().m_Value;
                }); 
            }
            else
            {
                Depth.m_Value = 0;
            }
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E07_hierarchy_components\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //
        // Register Components
        //
        upWorld->registerComponents
        <
                position
            ,   local_position
            ,   parent
            ,   depth
        >();

        //
        // Create the game graph.
        // 
        upWorld->registerGraphConnection<move_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Register event handlers
        //
        upWorld->registerEventDelegates<mutate_parent_delegate>();

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup
        <
                position
            ,   local_position
            ,   parent
            ,   depth
        >();

        //
        // Create one entity
        //
        upWorld->createEntity( Group );

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 100 frames
        //
        for( int i=0; i<100; i++) upWorld->Resume();
    }

}