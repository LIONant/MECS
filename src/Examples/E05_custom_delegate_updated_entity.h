namespace mecs::examples::E05_custom_delegate_updated_entity
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    struct position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
        constexpr position() : xcore::vector2(0,0) {}
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_system : mecs::system::instance
    {
        using mecs::system::instance::instance;

        constexpr static auto guid_v = mecs::system::guid{ "move_system" };

        // System can define their own custom events. They work similar to the standard MECS events.
        // Note that events need to have a GUID and also a name. The name is used for debugging and
        // for visual editors. Also you need to specify which system this event belongs.
        // This event could be defined outside the scope of the structure if we wanted to.
        struct my_event : mecs::event::instance
        {
            using                       event_t         = mecs::event::type< mecs::system::instance&, int >;
            constexpr static auto       guid_v          = mecs::event::guid     { "move_system::my_event" };
            constexpr static auto       name_v          = xconst_universal_str  ( "move_system::my_event" );
            using                       system_t        = move_system;
        };

        // This is how MECS knows which events this system have.
        using events_t = std::tuple
        <
            my_event
        >;

        // This function will automatically trigger the (updated_entity) event, since we have a mutable variable as a parameter.
        // if the variable will be 'const position& Position' then it won't trigger that event since it is mark as read only.
        void operator() ( position& Position ) noexcept
        {
            Position.m_X += 1.0f;
            Position.m_Y += 1.0f;

            // Let anyone listening to my_event receive 5... because why not?
            EventNotify<my_event>( 5 );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Custom Delegates
    //-----------------------------------------------------------------------------------------
    // Custom delegates are different from regular delegates. One of the main differences are
    // that they don't support 'query_t'. Stead they will received the event given by the system
    // no question asked. Also the parameters of the function is also different. The parameters
    // are given by custom event. In this example an integer. Since the custom events are issue
    // by only specific system their execution is less chaotic and that may allow certain flexibilities. 
    //-----------------------------------------------------------------------------------------
    struct my_delegate : mecs::delegate::instance< move_system::my_event >
    {
        constexpr static mecs::delegate::guid guid_v{ "my_delegate" }; 

        void operator() ( mecs::system::instance& System, int X ) const noexcept
        {
            printf("First User Base Event: %d\n", X );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Delegate 
    //-----------------------------------------------------------------------------------------
    // This delegate will be call one an entity which has position changes.
    struct mutated_position_delegate : mecs::delegate::instance< mecs::event::updated_entity >
    {
        constexpr static mecs::delegate::guid guid_v{ "mutated_position_delegate" }; 

        void operator() ( mecs::system::instance& System, const mecs::entity::instance& Entity, const position& Position ) const noexcept
        {
            printf("Entity: %I64X, New Position (%f, %f)\n", Entity.getGuid().m_Value, Position.m_X, Position.m_Y );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E05_custom_delegate_updated_entity\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<position>();

        //
        // Create the game graph.
        // 
        upWorld->registerGraphConnection<move_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Register delegates
        //
        upWorld->registerDelegates<mutated_position_delegate, my_delegate>();

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup<position>();

        //
        // Create one entity
        //
        upWorld->createEntity( Group );

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 10 frames
        //
        for( int i=0; i<10; i++) upWorld->Resume();
    }

}