
namespace mecs::examples::E15_2d_basic_physics
{
    //----------------------------------------------------------------------------------------
    // COMPONENTS::
    //----------------------------------------------------------------------------------------
    namespace component
    {
        struct position : xcore::vector2 
        {
            constexpr static mecs::type_guid type_guid_v{"mecs::examples::E15_2d_basic_physics::component::position"};
            using xcore::vector2::operator =;
        };

        struct velocity : xcore::vector2 
        {
            constexpr static mecs::type_guid type_guid_v{"mecs::examples::E15_2d_basic_physics::component::velocity"};
            using xcore::vector2::operator =;
        };

        struct collider
        {
            constexpr static mecs::type_guid type_guid_v{"mecs::examples::E15_2d_basic_physics::component::collider"};
            float m_Radius;
        };
    }

    //----------------------------------------------------------------------------------------
    // WORLD GRID::
    //----------------------------------------------------------------------------------------
    namespace world_grid
    {
        namespace system { struct advance_cell; }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: UNITS::
        //----------------------------------------------------------------------------------------
        namespace event
        {
            struct collision : mecs::event::instance
            {
                using                       event_t         = mecs::event::type< mecs::system::instance&, const mecs::entity::instance&, const mecs::entity::instance& >;
                constexpr static auto       guid_v          = mecs::event::guid     { "world_grid::event::collision" };
                constexpr static auto       name_v          = xconst_universal_str  ( "world_grid::event::collision" );
                using                       system_t        = system::advance_cell;
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: UNITS::
        //----------------------------------------------------------------------------------------
        namespace units
        {
            constexpr static int   grid_width_v  = 8;
            constexpr static int   grid_height_v = 8;

            struct vector2
            {
                int m_X, m_Y;

                vector2 operator - ( const vector2& A ) const noexcept
                {
                    return { m_X - A.m_X, m_Y - A.m_Y };
                }

                vector2 operator + ( const vector2& A ) const noexcept
                {
                    return { m_X + A.m_X, m_Y + A.m_Y };
                }
            };

            struct box
            {
                units::vector2 m_Min;
                units::vector2 m_Max;
            };

            xforceinline constexpr
            int WorldToGridX( float X ) noexcept { return static_cast<int>( X / grid_width_v ); }

            xforceinline constexpr
            int WorldToGridY( float Y ) noexcept { return static_cast<int>( Y / grid_width_v ); }

            xforceinline constexpr
            units::vector2 WorldToGrid( xcore::vector2 V ) noexcept
            {
                return{ WorldToGridX(V.m_X), WorldToGridY(V.m_Y) };
            }

            box SearchBox( units::vector2 Pos )
            {
                return box{ {Pos.m_X-1, Pos.m_Y-1}, {Pos.m_X+1, Pos.m_Y+1} };
            }

            box MakeBox( xcore::vector2 A, xcore::vector2 B, float Radious ) noexcept
            {
                box BBox;
                if( A.m_X < B.m_X )
                {
                    BBox.m_Min.m_X = WorldToGridX( A.m_X - Radious );
                    BBox.m_Max.m_X = WorldToGridX( B.m_X + Radious );
                }
                else
                {
                    BBox.m_Max.m_X = WorldToGridX( A.m_X + Radious ); 
                    BBox.m_Min.m_X = WorldToGridX( B.m_X - Radious );
                }

                if( A.m_Y < B.m_Y )
                {
                    BBox.m_Min.m_Y = WorldToGridY( A.m_Y - Radious ); 
                    BBox.m_Max.m_Y = WorldToGridY( B.m_Y + Radious );
                }
                else
                {
                    BBox.m_Max.m_Y = WorldToGridY( A.m_Y + Radious );
                    BBox.m_Min.m_Y = WorldToGridY( B.m_Y - Radious );
                }

                return BBox;
            }

            xforceinline constexpr
            std::uint64_t ComputeKeyFromPosition( int X, int Y ) noexcept
            {
                const auto x = (18446744073709551557ull ^ 9223372036854775ull) ^ (static_cast<std::uint64_t>(X)<<32) ^ static_cast<std::uint64_t>(Y);
                xassert(x);
                return x;
                //return xcore::bits::MurmurHash3( x );
            }

            xforceinline constexpr
            std::uint64_t ComputeKeyFromPosition( const vector2 A ) noexcept
            {
                return ComputeKeyFromPosition( A.m_X, A.m_Y );
            }
        }
    
        //----------------------------------------------------------------------------------------
        // WORLD GRID:: COMPONENTS::
        //----------------------------------------------------------------------------------------
        namespace component
        {
            constexpr static std::size_t max_entity_count = 64;
            struct lists : mecs::component::quantum_double_buffer
            {
                constexpr static mecs::type_guid type_guid_v{"mecs::examples::E15_2d_basic_physics::world_grid::component::lists"};

                struct entry
                {
                    xcore::vector2  m_Position;
                    xcore::vector2  m_Velocity;
                    float           m_Radius;
                };

                std::array< entry,                  max_entity_count >  m_lEntry;
                std::array< mecs::entity::instance, max_entity_count >  m_lEntity;
            };

            struct count : mecs::component::quantum_mutable
            {
                constexpr static mecs::type_guid type_guid_v{"mecs::examples::E15_2d_basic_physics::world_grid::component::count"};

                std::atomic< std::uint8_t >                             m_MutableCount  { 0 };
                std::uint8_t                                            m_ReadOnlyCount { 0 };        // Note this variable is naked/unprotected and should only be written by a single system 

                count& operator = ( count&& A )      noexcept
                {
                    m_MutableCount.store( A.m_MutableCount.load(std::memory_order_relaxed), std::memory_order_relaxed );
                    m_ReadOnlyCount = A.m_ReadOnlyCount;
                    return *this;
                }
            };
                
            struct id : units::vector2
            {
                constexpr static mecs::type_guid type_guid_v{"mecs::examples::E15_2d_basic_physics::world_grid::component::id"};
                using units::vector2::operator =;
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: SYSTEM::
        //----------------------------------------------------------------------------------------
        namespace system
        {
            //----------------------------------------------------------------------------------------
            // WORLD GRID:: SYSTEM:: ADVANCE CELL
            //----------------------------------------------------------------------------------------
            struct advance_cell : mecs::system::instance
            {
                constexpr static auto   guid_v              = mecs::system::guid{ "world_grid::advance_cell" };
                constexpr static auto   system_name_v       = xconst_universal_str("Advance 2D Physics Spatial Cell");
                constexpr static auto   entities_per_job_v  = 50;

                using mecs::system::instance::instance;

                using access_t = mecs::query::access
                <       const component::lists&
                    ,   const component::id&
                    ,         component::lists&
                    ,         component::count&
                >;

                using events_t = std::tuple
                <
                    event::collision
                >;

                xforceinline
                void operator() ( access_t& Component ) noexcept
                {
                    std::array< std::array< const component::lists*,  3 >, 3 > T0CellMap    ;
                    std::array< std::array<       component::lists*,  3 >, 3 > T1CellMap    ;
                    std::array< std::array<       component::count*,  3 >, 3 > CellMapCount ;
                    std::array< std::array<       std::uint64_t,      3 >, 3 > CellGuids    ;

                    //
                    // Cache all relevant cells
                    //
                    const auto ID               = Component.get< const component::id& >();
                    const auto SearchBoxA       = units::SearchBox( ID );
                    {
                        //XCORE_PERF_ZONE_SCOPED_N("CacheCells")
                        T0CellMap[1][1]             = &Component.get< const component::lists& >    ();
                        T1CellMap[1][1]             = &Component.get< component::lists& >          ();
                        CellMapCount[1][1]          = &Component.get< component::count& >          ();
                        CellGuids[1][1]             = world_grid::units::ComputeKeyFromPosition( Component.get< const component::id& >() );

                        for( int y = SearchBoxA.m_Min.m_Y; y<=SearchBoxA.m_Max.m_Y; ++y )
                        for( int x = SearchBoxA.m_Min.m_X; x<=SearchBoxA.m_Max.m_X; ++x )
                        {
                            if( x == ID.m_X && y == ID.m_Y ) continue;
                                
                            const auto RelativeGridPos  = units::vector2{ 1 + x, 1 + y } - ID;

                            if( false == findEntity (  mecs::entity::guid{ CellGuids[RelativeGridPos.m_X][RelativeGridPos.m_Y] = world_grid::units::ComputeKeyFromPosition(x,y) }
                                , [&]( component::count& Count, const component::lists& T0Lists, component::lists& T1Lists ) noexcept
                                {
                                    T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]             = &T0Lists;
                                    T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]             = &T1Lists;
                                    CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]          = &Count;
                                })
                            )
                            {
                                T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]    = nullptr;
                                CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y] = nullptr;
                            }
                        }
                    }

                    //
                    // Advance physics for our cell
                    //
                    //XCORE_PERF_ZONE_SCOPED_N("AdvancePhysics")
                    const float DT      = getTime().m_DeltaTime;
                    for( std::uint8_t CountT = CellMapCount[1][1]->m_ReadOnlyCount, i=0; i<CountT; ++i )
                    {
                        //
                        // Integrate
                        //
                        const auto&    T0Entry    = T0CellMap[1][1]->m_lEntry[i];
                        xcore::vector2 T1Velocity = T0Entry.m_Velocity;
                        xcore::vector2 T1Position = T0Entry.m_Position + T1Velocity;// * DT;

                        //
                        // Check for collisions
                        //
                        {
                            //XCORE_PERF_ZONE_SCOPED_N("CheckCollisions")
                            const float    VL         = T1Velocity.getLength();
                            const auto     SearchBoxB = units::MakeBox( T1Position, T1Position + (T1Velocity / VL), T0Entry.m_Radius );
                            for( int  y     = SearchBoxB.m_Min.m_Y; y<=SearchBoxB.m_Max.m_Y; y++ )
                            for( int  x     = SearchBoxB.m_Min.m_X; x<=SearchBoxB.m_Max.m_X; x++ )
                            {
                                const auto RelativeGridPos  = units::vector2{ 1 + x, 1 + y } - ID;

                                if( T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y] ) 
                                    for( std::uint8_t Count = CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_ReadOnlyCount, c = 0; c < Count; ++c )
                                    {
                                        auto& T0Other = T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntry[c];
                                        if( &T0Entry == &T0Other ) continue;
                                        
                                        // Check for collision
                                        const auto V                    = (T0Other.m_Position - T1Position);
                                        const auto MinDistanceSquare    = xcore::math::Sqr(T0Other.m_Radius + T0Entry.m_Radius);
                                        const auto DistanceSquare       = V.Dot(V);
                                        if( DistanceSquare <= MinDistanceSquare )
                                        {
                                            // Do hacky collision response
                                            const auto OneOverDistance  = xcore::math::InvSqrt(DistanceSquare);
                                            const auto Normal           = -V * OneOverDistance;
                                            const auto Distance         = 1.0f/OneOverDistance;
                                            const auto LostEnergy       = MinDistanceSquare - Distance + 0.01f;
                                            T1Velocity = xcore::math::vector2::Reflect( Normal, T1Velocity );
                                            T1Position -= Normal * LostEnergy;

                                            // Notify entities about the collision
                                            EventNotify<event::collision>( T0CellMap[1][1]->m_lEntity[i], T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntity[c] );
                                        }
                                    }
                            }
                        }

                        //
                        // Make sure we have the cells in question
                        //
                        const auto WorldPosition    = units::WorldToGrid( T1Position );
                        const auto RelativeGridPos  = units::vector2{1,1} + WorldPosition - ID;
                        int C;
                        {
                            //XCORE_PERF_ZONE_SCOPED_N("MoveToT1")
                            if( CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y] == nullptr ) getOrCreatEntity
                            <
                                    component::lists
                                ,   component::count
                                ,   component::id
                            >(  mecs::entity::guid{ CellGuids[RelativeGridPos.m_X][RelativeGridPos.m_Y] }
                                // Get entry
                                , [&]( component::count& Count, component::lists& T1Lists ) noexcept
                                {
                                    CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]          = &Count;
                                    T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]             = &T1Lists;
                                }
                                // Create entry
                                ,   [&]( component::count& Count, component::id& ID, component::lists& T1Lists ) noexcept
                                {
                                    ID = WorldPosition;

                                    CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]  = &Count;
                                    T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]     = &T1Lists;
                                }
                            );

                            //
                            // Copy the entry in to the cell
                            //
                                        C               = CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_MutableCount++;
                            auto&       Entry           = T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntry[C];
                            Entry.m_Position            = T1Position;
                            Entry.m_Velocity            = T1Velocity;
                            Entry.m_Radius              = T0Entry.m_Radius;
                            T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntity[C] = T0CellMap[1][1]->m_lEntity[i];
                        }

                        //
                        // Copy out the position to the client
                        //
                        {
                            //XCORE_PERF_ZONE_SCOPED_N("CopyToClient")
                            getEntity( T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntity[C], [&]( E15_2d_basic_physics::component::position& Position )
                            {
                                Position = T1Position;
                            });
                        }
                    }
                }
            };

            //----------------------------------------------------------------------------------------
            // WORLD GRID:: SYSTEM:: RESET COUNTS
            //----------------------------------------------------------------------------------------
            struct reset_counts : mecs::system::instance
            {
                using mecs::system::instance::instance;

                constexpr static auto guid_v = mecs::system::guid{ "reset_counts" };

                // This system is reading T0 and it does not need to worry about people changing its value midway.
                xforceinline
                void operator() ( mecs::entity::instance& Entity, const component::lists& Lists, component::count& Count, const component::id& ID ) noexcept
                {
                    Count.m_ReadOnlyCount = Count.m_MutableCount.load(std::memory_order_relaxed);

                    if( Count.m_ReadOnlyCount == 0 )
                    {
                        deleteEntity(Entity);
                    }
                    else
                    {
                        Count.m_MutableCount.store(0,std::memory_order_relaxed);

                        if(Count.m_ReadOnlyCount>10)
                            printf( "Cell[%4d,%4d] has %3d Entries\n"
                                , ID.m_X
                                , ID.m_Y
                                , Count.m_ReadOnlyCount
                                );
                            
                        if(0) for( auto i=0; i<Count.m_ReadOnlyCount; ++i )
                        {
                            printf( "   Entry[%3d] pos(%f,%f), vel(%f,%f)\n"
                            , i
                            , Lists.m_lEntry[i].m_Position.m_X
                            , Lists.m_lEntry[i].m_Position.m_Y
                            , Lists.m_lEntry[i].m_Velocity.m_X
                            , Lists.m_lEntry[i].m_Velocity.m_Y
                            );
                        }
                    }
                }
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: DELEGATE::
        //----------------------------------------------------------------------------------------
        namespace delegate
        {
            //----------------------------------------------------------------------------------------
            // WORLD GRID:: SYSTEM:: CREATE SPATIAL ENTITY
            //----------------------------------------------------------------------------------------
            struct create_spatial_entity : mecs::delegate::instance< mecs::event::create_entity >
            {
                constexpr static mecs::delegate::guid guid_v{ "world_grid::delegate::create_spatial_entity" };

                // This function will be call ones per entity. Since we only will have one entity it will only be call ones per frame.
                void operator() (       mecs::system::instance&                     System
                                , const mecs::entity::instance&                     Entity
                                , const E15_2d_basic_physics::component::position&  Position
                                , const E15_2d_basic_physics::component::velocity&  Velocity
                                , const E15_2d_basic_physics::component::collider&  Collider ) const noexcept
                {
                    const auto               GridPosition   = units::WorldToGrid(Position);
                    const mecs::entity::guid Guid           { world_grid::units::ComputeKeyFromPosition( GridPosition ) };

                    System.getOrCreatEntity
                    < 
                            component::lists
                        ,   component::count
                        ,   component::id
                    >
                    (
                        Guid
                        // Get entry
                        , [&]( component::lists& Lists, component::count& Count ) noexcept
                        {
                            const auto C = Count.m_MutableCount++;

                            auto& Entry = Lists.m_lEntry[C];
                            Entry.m_Position    = Position;
                            Entry.m_Velocity    = Velocity;
                            Entry.m_Radius      = Collider.m_Radius;
                            Lists.m_lEntity[C]  = Entity;
                        }
                        // Create Entry
                        , [&]( component::lists& Lists, component::count& Count, component::id& ID ) noexcept
                        {
                            ID = GridPosition;

                            Count.m_MutableCount.store( 1, std::memory_order_relaxed );

                            auto& Entry = Lists.m_lEntry[0];
                            Entry.m_Position    = Position;
                            Entry.m_Velocity    = Velocity;
                            Entry.m_Radius      = Collider.m_Radius;
                            Lists.m_lEntity[0]  = Entity;
                        }
                    );
                }
            };

            //----------------------------------------------------------------------------------------
            // WORLD GRID:: DELEGATE:: DESTROY SPATIAL ENTITY
            //----------------------------------------------------------------------------------------
            struct destroy_spatial_entity : mecs::delegate::instance< mecs::event::destroy_entity >
            {
                constexpr static mecs::delegate::guid guid_v{ "world_grid::delegate::destroy_spatial_entity" };

                // This function will be call ones per entity. Since we only will have one entity it will only be call ones per frame.
                void operator() (         mecs::system::instance&                     System
                                ,   const mecs::entity::instance&                     Entity
                                ,   const E15_2d_basic_physics::component::position&  Position ) const noexcept
                {
                    /*
                    const auto               WorldPosition = units::WorldToGrid(Position);
                    const mecs::entity::guid Guid{ world_grid::units::ComputeKeyFromPosition( WorldPosition ) };

                    // remove entity from the spatial grid
                    System.getEntity( Guid,
                        [&]( mecs::query::define<mecs::query::access
                        <
                                component::countentity
                        >>::access& Access )
                        {
                            auto& lList = Access.get<component::countentity>().m_lEntity;
                            for( std::uint16_t end = Access.get<component::countentity>().m_Count.load( std::memory_order_relaxed ), i=0u; i<end; ++i )
                            {
                                if( &lList[i].getPointer() == &Entity.getPointer() )
                                {
                                    lList[i].MarkAsZombie();
                                    return;
                                }
                            }
                            xassert(false);
                        });
                    */                        
                }
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: IMPORT
        //----------------------------------------------------------------------------------------
        void ImportModule( mecs::world::instance& World ) noexcept
        {
            World.registerComponents< component::lists, component::count, component::id >();
            World.registerDelegates< delegate::create_spatial_entity, delegate::destroy_spatial_entity >();
        }
    }

    //-----------------------------------------------------------------------------------------
    // Delegates
    //-----------------------------------------------------------------------------------------
    struct my_collision_delegate : mecs::delegate::instance< world_grid::event::collision >
    {
        constexpr static mecs::delegate::guid guid_v{ "my_collision_delegate" };

        void operator() ( mecs::system::instance& System, const mecs::entity::instance& E1, const mecs::entity::instance& E2 ) const noexcept
        {
            //printf( ">>>>>>>>>>Collision \n");
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E15_2d_basic_physics\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        // Import that world_grid system
        world_grid::ImportModule( *upWorld );

        //
        // Register Components
        //
        upWorld->registerComponents<component::position,component::collider,component::velocity>();

        //
        // Register Delegates
        //
        upWorld->registerDelegates<my_collision_delegate>();

        //
        // Synpoint
        //
        mecs::sync_point::instance SyncPhysics;

        //
        // Register the game graph.
        // 
        upWorld->registerGraphConnection<world_grid::system::advance_cell>( upWorld->m_StartSyncPoint,  SyncPhysics );
        upWorld->registerGraphConnection<world_grid::system::reset_counts>( SyncPhysics,                upWorld->m_EndSyncPoint );

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup<component::position,component::collider,component::velocity>();

        //
        // Create Entities
        //
        xcore::random::small_generator Rnd;
        for( int i=0; i<100000; i++ )
            upWorld->createEntity( Group, [&](  component::position&  Position
                                            ,   component::velocity&  Velocity
                                            ,   component::collider&  Collider )
            {

                Position.setup( Rnd.RandF32(-1000.0f, 1000.0f ), Rnd.RandF32(-1000.0f, 1000.0f ) );

                Velocity.setup( Rnd.RandF32(-1.0f, 1.0f ), Rnd.RandF32(-1.0f, 1.0f ) );
                Velocity.NormalizeSafe();
                //Velocity *= 10.0f;

                Collider.m_Radius = Rnd.RandF32( 0.5f, 1.0f );
            });

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 100 frames
        //
        for( int i=0; i<100; i++)
        {
            printf("-------------------------------------------------------\n");
            upWorld->Resume();
        }
            
    }
}