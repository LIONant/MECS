
#include "../mecs.h"
#include "../Examples/mecs_examples.h"
#include "../Examples/Tests/mecs_tests.h"

int main()
{
    xcore::Init("MECS");

    if (0) mecs::tests::Test();
    if (1) mecs::examples::Test();

    xcore::Kill();
    return 0;
}