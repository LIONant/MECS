namespace mecs::examples::E04_delegate_moveinout
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    struct position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
        constexpr position() : xcore::vector2(0,0) {}
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct will_add_position_system : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "will_add_position_system" };

        using mecs::system::instance::instance;

        // In ECS you can create filters/queries which identify which kind of entities will be
        // executed by the system. MECS specifies its queries via a type 'query_t' as shown below.
        // In its template arguments you can specified 3 types of filters:
        //      1. mecs::query::none< components... > == Entities with these components won't execute the our function.
        //      2. mecs::query::all < components... > == Entities must have all these components or else won't execute.
        //      3. mecs::query::any < components... > == Entities that have at least one of these component will execute.
        //
        // The system's function parameters will complement the query. All parameters with references '&' will be added to
        // the 'mecs::query::all' automatically. All parameters mark as a pointer '*' will be added to 'mecs::query::any'
        // In this example we want entities that don't have a position component. 
        using query_t = mecs::query::define
        <
            mecs::query::none<position>
        >;

        // This function will add the position component to entities
        void operator() ( mecs::entity::instance& Entity ) noexcept
        {
            getGroupBy<std::tuple<position>,std::tuple<>>( Entity, [&](mecs::component::group::instance& Group )
            {
                moveEntityToGroup( Group, Entity );
            });
        }
    };

    //-----------------------------------------------------------------------------------------

    struct will_remove_position_system : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "will_remove_position_system" };
        using mecs::system::instance::instance;

        // This system wants all entities which have a position component
        using query_t = mecs::query::define
        <
            mecs::query::all<position>
        >;

        // Here we are going to remove the position component
        void operator() ( mecs::entity::instance& Entity ) noexcept
        {
            getGroupBy<std::tuple<>,std::tuple<position>>( Entity, [&](mecs::component::group::instance& Group )
            {
                moveEntityToGroup( Group, Entity );
            });
        }
    };

    //-----------------------------------------------------------------------------------------
    // Group Delegates
    //-----------------------------------------------------------------------------------------
    // Delegates are used to extend systems. MECS's Systems generate events base on things happening to them.
    // One event could be when a system moves an 'entity into another group' (event::moved_in). When this event happen
    // we can add additional functionally. Unlike systems which are connected to a graph delegates are not.
    // They can be trigger by any system at any time. So similarly to systems they don't have state.
    // If you are any non atomic variables here the probability that something will go wrong is close to 100%.
    // Similar to systems delegates have a standard function that will be executed when the event happens.
    // The delegates standard functions is a little different from system function. The key difference is the first
    // parameter in delegates. The first parameter is the system which is calling the delegate.
    // All delegate functions must have at least this parameter.
    // Similar to systems delegates can filter out which types of entities will call the delegate using the query_t and
    // the parameters of its function.
    //-----------------------------------------------------------------------------------------
    struct add_position_delegate : mecs::delegate::instance< mecs::event::moved_in<position> >
    {
        constexpr static mecs::delegate::guid guid_v{ "add_position_delegate" }; 

        // This function will be call ones per entity. Since we only will have one entity it will only be call ones per frame.
        // Note that by putting the position component in the parameter list we are asking for groups which have this component in it.
        // Note that the first parameter is the system that created this event. Note that this parameter is require.
        void operator() ( mecs::system::instance& System, position& Position ) const noexcept
        {
            Position.m_X = 10;
            Position.m_Y = 20;
            printf("Position was Added\n");
        }
    };

    //-----------------------------------------------------------------------------------------
    
    // This delegate will be call for each entity which gets moved out of a group.
    struct remove_position_delegate : mecs::delegate::instance< mecs::event::moved_out<position> >
    {
        constexpr static mecs::delegate::guid guid_v{"remove_position_delegate"};

        // We want entities which have or had positions
        using query_t = mecs::query::define
        <
            mecs::query::all<position>
        >;

        // This function has the minimum number of parameters.
        void operator() ( mecs::system::instance& System ) const noexcept
        {
            printf("Position was Removed\n");
        }
    };
    
    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E04_delegate_moveinout\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<position>();

        //
        // Register Delegates
        //
        upWorld->registerDelegates
        <
                add_position_delegate
            ,   remove_position_delegate
        >();

        //
        // Register the game graph.
        // Note that we are adding two system which could run in parallel.
        // Note how in this example an entity only executes in one system and not in both.
        // This is because of the change of state is "visible" at the end of the executing step
        // (ones all the systems connected to the same syncpoint are done).
        // In our example that syncpoint happens to be the last one.
        upWorld->registerGraphConnection<will_add_position_system>   ( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );
        upWorld->registerGraphConnection<will_remove_position_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup<position>();

        //
        // Create one entity
        //
        upWorld->createEntity( Group );

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 10 frames
        //
        for( int i=0; i<10; i++) upWorld->Resume();
    }
}