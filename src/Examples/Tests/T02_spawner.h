namespace mecs::tests::T02_spawner
{
    namespace life
    {
        struct component
        {
            constexpr static mecs::type_guid type_guid_v{ "life component" };
            int m_Value{10};
        };

        namespace system
        {
            struct decrement : mecs::system::instance
            {
                constexpr static auto   guid_v          = mecs::system::guid{ "decrement" };
                constexpr static auto   system_name_v   = xconst_universal_str("life decrement system");
        
                using mecs::system::instance::instance;

                void operator() ( mecs::entity::instance& Entity, life::component& Life ) noexcept
                {
                    Life.m_Value--;
                    if( Life.m_Value == 0 ) 
                    {
                        deleteEntity(Entity);
                    }
                }
            };
        }
    }

    namespace spawner
    {
        struct component
        {
            constexpr static mecs::type_guid type_guid_v{ "spawner component" };

            std::uint32_t                               m_Count;
            mecs::component::group::guid                m_gGroup;
            xcore::func<void(mecs::entity::tmp_ref)>    m_fnCreated;
        };

        struct system : mecs::system::instance
        {
            constexpr static auto   guid_v          = mecs::system::guid{ "system" };
            constexpr static auto   system_name_v   = xconst_universal_str("spawner_system");
        
            using mecs::system::instance::instance;

            constexpr static std::uint32_t limit_singlecore_v = 1000u;

            void operator() ( mecs::entity::instance& Entity, spawner::component& S ) noexcept
            {
                getGroup( S.m_gGroup, [&](mecs::component::group::instance& Group ) noexcept
                {
                    if( S.m_fnCreated.isValid() ) 
                    {
                        if( S.m_Count > limit_singlecore_v )
                        {
                            xcore::scheduler::channel Channel( xconst_universal_str("Spawer"));
                            for( auto end = S.m_Count; end; )
                            {
                                auto Count = std::min( 1000u, end );

                                Channel.SubmitJob([this, Count, &Group, &S]()
                                {
                                    for( auto end = Count; end--; )
                                    {
                                        createEntity( Group, S.m_fnCreated );
                                    }
                                });

                                end -= Count;
                            }
                            Channel.join();
                        }
                        else for( auto end = S.m_Count; end--; )
                        {
                            createEntity( Group, S.m_fnCreated );
                        }
                    }
                    else if( S.m_Count > limit_singlecore_v )
                    {
                        xcore::scheduler::channel Channel( xconst_universal_str("Spawer") );
                        for( auto end = S.m_Count; end; )
                        {
                            auto Count = std::min( 1000u, end );

                            Channel.SubmitJob([this, Count, &Group]()
                            {
                                for( auto end = Count; end--; )
                                    createEntity( Group, []{} );
                            });

                            end -= Count;
                        }
                        Channel.join();
                    }
                    else for( auto end = S.m_Count; end--; )
                    {
                        createEntity( Group, []{} );
                    }
                });

                deleteEntity(Entity);
            }
        };
    }

    void Test()
    {
        XCORE_PERF_ZONE_SCOPED()
        mecs::world::instance World;
        World.registerComponents
        <
                life::component
            ,   spawner::component
        >();

        World.registerGraphConnection<spawner::system>           ( World.m_StartSyncPoint, World.m_EndSyncPoint );
        World.registerGraphConnection<life::system::decrement>   ( World.m_StartSyncPoint, World.m_EndSyncPoint );

        World.registerFinish();

        //
        // Create a bunch of entities
        //
        auto&   Group        = World.getOrCreateGroup<spawner::component>();

        World.createEntity( Group, [&](mecs::entity::tmp_ref Ref) noexcept
        {
            auto& Spawner = Ref.getComponent<spawner::component>();

            Spawner.m_Count     = 10;
            Spawner.m_gGroup    = World.getOrCreateGroup<life::component>().m_Guid;
            Spawner.m_fnCreated = [](mecs::entity::tmp_ref Ref ){ Ref.getComponent<life::component>().m_Value = 23; };
        });

        //
        // Get started
        //
        World.Start();
        for( int i=0; i<100; i++) World.Resume();
    }
}
