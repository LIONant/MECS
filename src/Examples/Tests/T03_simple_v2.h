namespace mecs::tests::T03_simple_v2
{
    struct simple
    {
        constexpr static mecs::type_guid type_guid_v{"simple"};
        int m_Value{ 22 };
    };

    struct double_buff : mecs::component::double_buffer
    {
        constexpr static mecs::type_guid type_guid_v{ "double_buff" };
        int m_Value{ 123 };
    };

    struct my_tag : mecs::component::tag 
    {
        constexpr static mecs::type_guid type_guid_v{ "my_tag" };
    };

    struct my_system : mecs::system::instance
    {
        constexpr static auto   system_name_v   = xconst_universal_str("my_system");
        constexpr static auto   guid_v          = mecs::system::guid{ "my_system" };
        using                   access_t        = mecs::query::access< mecs::entity::instance&, const double_buff&, simple& >;
        using                   query_t         = mecs::query::define< mecs::query::none<my_tag> >;

        using mecs::system::instance::instance;

        xcore::random::small_generator R;

        void operator()( const access_t& Components ) noexcept
        {
            // This is the equivalent of removing a component (double_buff) from the entity
            if( (R.RandU32() % 10)  == 0 )
                getGroupBy< std::tuple<>, std::tuple<double_buff> >( Components.get<mecs::entity::instance&>(), [&](mecs::component::group::instance& Group )
                {
                    moveEntityToGroup( Group, Components.get<mecs::entity::instance&>() );
                });
        }
    };

    void Test()
    {
        XCORE_PERF_ZONE_SCOPED()
        mecs::world::instance World;
        World.registerComponents
        <
                simple
            ,   double_buff
            ,   my_tag
        >();

        World.registerGraphConnection<my_system>( World.m_StartSyncPoint, World.m_EndSyncPoint );

        World.registerFinish();

        //
        // Allocate a bunch of pages
        //
        World.m_PageMgr.PreallocatePages( 20000 );

        //
        // Create a bunch of entities
        //
        {
            xcore::scheduler::channel MyChannel(xconst_universal_str("Create Entities"));
            auto& Group = World.getOrCreateGroup<simple,double_buff,my_tag>();

            for(int i=0; i<xcore::get().m_Scheduler.getWorkerCount(); i++)
                MyChannel.SubmitJob([&]
                {
                    while (Group.newSize() < 300000ull XCORE_CMD_ASSERT( / 10 ) )
                    {
                        World.createEntity( Group, [](mecs::entity::tmp_ref tmpEntityRef)
                        {
                            auto&   Entity = tmpEntityRef.getComponent<mecs::entity::instance>();
                            auto&   Simple = tmpEntityRef.getComponent<simple>();
                            auto&   Double = tmpEntityRef.getComponent<double_buff>();
                            assert(Simple.m_Value == 22);
                            assert(Double.m_Value == 123);
                        });
                    }
                });

            MyChannel.join();

            //
            // Check if we can add some other kind of entity
            //
            auto& NewGroup = World.getOrCreateGroupBy<std::tuple<>, std::tuple<simple>>( Group );
            World.createEntity( NewGroup, [](mecs::entity::tmp_ref tmpEntityRef)
            {
                auto&   Entity = tmpEntityRef.getComponent<mecs::entity::instance>();
                auto&   Double = tmpEntityRef.getComponent<double_buff>();
                assert(Double.m_Value == 123);
            });
        }

        World.Start();
        for( int i=0; i<100; i++) 
        {
            World.Resume();
        }
    }
}
