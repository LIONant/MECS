namespace mecs::tests::T01_simple
{
    struct simple
    {
        constexpr static mecs::type_guid type_guid_v{"simple"};
        int m_Value{ 22 };
    };

    struct double_buff : mecs::component::double_buffer
    {
        constexpr static mecs::type_guid type_guid_v{ "double_buff" };
        int m_Value{ 123 };
    };

    struct my_tag : mecs::component::tag 
    {
        constexpr static mecs::type_guid type_guid_v{ "my_tag" };
    };

    struct my_system : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "my_system" };
        constexpr static auto   system_name_v   = xconst_universal_str("my_system");
        using query_t = mecs::query::define     // This query_t overrides the default query_t. The default query_t is empty.
        <
                mecs::query::all<simple>        // Must have all these components
            ,   mecs::query::none<my_tag>       // Can not have any of these components
        >;

        using mecs::system::instance::instance;

        // This is the function that will execute. Note that the function takes components.
        // These components will be added to the query as Must Have. In other words (mecs::query::all<...>).
        // In this example there for the mecs::query::all<simple> is irrelevant since we already are including 
        // it in the parameters of the function.
        // Note the function signature is not virtual bur rather it is inline or even constexpr if possible.
        // Also note how the function is mark as const since systems dont have state.
        constexpr xforceinline
        void operator() ( const double_buff& Buff, simple& Simple ) const noexcept
        {
            Simple.m_Value += Buff.m_Value;
        }
    };

    struct my_system2 : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "my_system2" };
        constexpr static auto   system_name_v   = xconst_universal_str("my_system2");
        using                   query_t         = mecs::query::define
        <
                mecs::query::all<simple>
            ,   mecs::query::none<my_tag>
        >;
        using                   access_t        = mecs::query::access<const double_buff&, simple&>; // Note that this is the equivalent of the parameters of the function


        using mecs::system::instance::instance;

        // This function works exactly the same way than the previous system. Note that mecs::query::access is where all the parameters are. 
        // Note the you don't need to add the reference symbol, but you do need to add the const.
        // Turns out that for some reason the compiler makes faster code than actually having the parameters spell out in the function.
        constexpr xforceinline
        void operator() ( access_t& Component ) const noexcept
        {
            Component.get<simple&>().m_Value += Component.get<const double_buff&>().m_Value;
        }
    };

    void Test()
    {
        XCORE_PERF_ZONE_SCOPED()
        mecs::world::instance World;
        mecs::sync_point::instance MidPoint;
        World.registerComponents
        <
                simple
            ,   double_buff
            ,   my_tag
        >();

        World.registerGraphConnection<my_system>( World.m_StartSyncPoint, MidPoint );
        World.registerGraphConnection<my_system2>( MidPoint, World.m_EndSyncPoint );

        World.registerFinish();

        //
        // Create a bunch of entities
        //
        {
            xcore::scheduler::channel MyChannel(xconst_universal_str("Create Entities"));
            auto& Group = World.getOrCreateGroup<simple,double_buff,my_tag>();

            for(int i=0; i<xcore::get().m_Scheduler.getWorkerCount(); i++)
                MyChannel.SubmitJob([&]
                {
                    while (Group.newSize() < 10000000ull XCORE_CMD_ASSERT( / 10 ) )
                    {
                        World.createEntity( Group, []( mecs::entity::instance& Entity, simple& Simple, double_buff& Double )
                        {
                            assert(Simple.m_Value == 22);
                            assert(Double.m_Value == 123);
                        });
                    }
                });

            MyChannel.join();
        }

        World.Start();
        for( int i=0; i<100; i++) World.Resume();
    }
}