namespace mecs::tests::T05_memory
{
    void Subpages_SimpleTest( void )
    {
        mecs::tools::page_mgr Mgr;
        mecs::tools::subpage_mgr<256> SubMgr(Mgr);

        std::vector<mecs::tools::subpage_mgr<256>::subpage*> Pages;

        for (int i=0; i<100; i++)
        {
            Pages.push_back(SubMgr.AllocCustomPage());
            for (int i=0u;i<256;++i) Pages.back()->m_RawData[i] = (std::byte)0xCD;
        }

        while (Pages.size())
        {
            SubMgr.FreeCustomPage(Pages.back());
            Pages.pop_back();
        }
    }

    void Subpages_RandomAllocFrees()
    {
        mecs::tools::page_mgr Mgr;
        mecs::tools::subpage_mgr<256> SubMgr(Mgr);

        std::vector<mecs::tools::subpage_mgr<256>::subpage*> Pages;
        xcore::random::small_generator Rnd;

        for (int i=0; i<1000; i++)
        {
            auto c = Rnd.RandS32(1,(int)std::min(100ull,2+Pages.size()));
            if( Rnd.RandU32()&1 )
            {
                while (c--)
                {
                    Pages.push_back(SubMgr.AllocCustomPage());
                    for(int i=0u;i<256;++i) Pages.back()->m_RawData[i] = (std::byte)0xCD;
                }
            }
            else
            {
                while( c-- && Pages.size() )
                {
                    const auto Index = Rnd.RandS32( 0, (int)Pages.size() );
                    SubMgr.FreeCustomPage(Pages[Index]);
                    Pages.erase(Pages.begin()+Index);
                }
            }
        }

        // Get Stats
        const auto Stats = SubMgr.getStats();

        // clean up
        while (Pages.size())
        {
            SubMgr.FreeCustomPage(Pages.back());
            Pages.pop_back();
        }
    }

    void Vector_SimpleTest(void)
    {
        using vec       = mecs::tools::vector<std::array<int,128>, 25, 100>;
        using sub_mgr   = vec::subpage_mgr_t;
        
        mecs::tools::page_mgr       Mgr;
        sub_mgr                     SubMgr(Mgr);
        vec                         Vector(SubMgr);

        for (int i=0; i<100; i++)
        {
            auto& A = Vector.append();
            for( auto& E : A ) E = 0xCDCDCDCD;
        }

        while (Vector.size())
        {
            Vector.eraseSwap(0);
        }
    }

    void Vector_RandomAllocFrees()
    {
        using vec       = mecs::tools::vector<std::array<int,32>, 10, 3000>;
        using sub_mgr   = vec::subpage_mgr_t;

        mecs::tools::page_mgr           Mgr;
        sub_mgr                         SubMgr(Mgr);
        vec                             Vector(SubMgr);
        xcore::random::small_generator  Rnd;

        for (int i=0; i<1000; i++)
        {
            auto c = Rnd.RandS32(1,(int)std::min(100ull,2+Vector.size()));
            if (Rnd.RandU32()&1)
            {
                while (c--)
                {
                    auto& A = Vector.append();
                    for (auto& E : A) E = 0xCDCDCDCD;
                }
            } 
            else
            {
                while (c-- && Vector.size())
                {
                    const auto Index = Rnd.RandS32(0,(int)Vector.size());
                    Vector.eraseSwap(Index);
                }
            }
        }

        // Get Stats
        const auto Stats = SubMgr.getStats();

        // clean up
        Vector.DeleteAllEntries();
    }

    void Test()
    {
        Subpages_SimpleTest();
        Subpages_RandomAllocFrees();

        Vector_SimpleTest();
        Vector_RandomAllocFrees();
    }
}