

#include "E04_module.h"

namespace mecs::examples::E04_simple_module
{
    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E04_simple_module\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //
        // Register Components
        //
        mecs::examples::E04_module::ImportModule( *upWorld );

        //
        // Create the game graph.
        // 
        upWorld->registerGraphConnection<mecs::examples::E04_module::move_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup
        <
                mecs::examples::E04_module::position
            ,   mecs::examples::E04_module::velocity
        >();

        //
        // Create one entity
        //
        upWorld->createEntity( Group, []( mecs::entity::tmp_ref tmpRef )
        {
            tmpRef.getComponent<mecs::examples::E04_module::position>().setup( 0.0f, 0.0f );
            tmpRef.getComponent<mecs::examples::E04_module::velocity>().setup( 1.0f, 1.0f );
        });

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 100 frames
        //
        for( int i=0; i<100; i++) upWorld->Resume();
    }
}
