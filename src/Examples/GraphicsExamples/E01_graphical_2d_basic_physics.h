namespace mecs::examples::E01_graphical_2d_basic_physics
{
    namespace example = mecs::examples::E01_graphical_2d_basic_physics;

    //-----------------------------------------------------------------------------------------
    // Example Menu
    //-----------------------------------------------------------------------------------------
    struct my_menu : xcore::property::base
    {
        int     m_EntitieCount;
        bool    m_bRenderGrid;
             my_menu() { reset(); }
        void reset()
        {
            m_EntitieCount = 250000 XCORE_CMD_DEBUG( / 100 );
            m_bRenderGrid  = false;
        }

        property_vtable()
    };
    static my_menu s_MyMenu;

    //----------------------------------------------------------------------------------------
    // COMPONENTS::
    //----------------------------------------------------------------------------------------
    namespace component
    {
        struct position : xcore::vector2 
        {
            constexpr static mecs::type_guid type_guid_v    {"mecs::examples::E01_graphical_2d_basic_physics::component::position"};
            constexpr static auto            name_v         = xconst_universal_str("position");
            using xcore::vector2::operator =;
        };

        struct velocity : xcore::vector2 
        {
            constexpr static mecs::type_guid type_guid_v    {"mecs::examples::E01_graphical_2d_basic_physics::component::velocity"};
            constexpr static auto            name_v         = xconst_universal_str("velocity");
            using xcore::vector2::operator =;
        };

        struct collider
        {
            constexpr static mecs::type_guid type_guid_v    {"mecs::examples::E01_graphical_2d_basic_physics::component::collider"};
            constexpr static auto            name_v         = xconst_universal_str("collider");

            float m_Radius;
        };
    }

    //----------------------------------------------------------------------------------------
    // WORLD GRID::
    //----------------------------------------------------------------------------------------
    namespace world_grid
    {
        //----------------------------------------------------------------------------------------
        // WORLD GRID:: EVENTS::
        //----------------------------------------------------------------------------------------
        namespace system { struct advance_cell; }
        namespace event
        {
            struct collision : mecs::event::instance
            {
                using                       event_t         = mecs::event::type< mecs::system::instance&, const mecs::entity::instance&, const mecs::entity::instance& >;
                constexpr static auto       guid_v          = mecs::event::guid     { "world_grid::event::collision" };
                constexpr static auto       name_v          = xconst_universal_str  ( "world_grid::event::collision" );
                using                       system_t        = system::advance_cell;
            };

            struct render : mecs::event::instance
            {
                using                       event_t         = mecs::event::type< mecs::system::instance&        // System
                                                                                , const mecs::entity::instance& // Entity
                                                                                , const xcore::vector2&         // Position
                                                                                , const xcore::vector2&         // Velocity
                                                                                , float >;                      // Radius
                constexpr static auto       guid_v          = mecs::event::guid     { "world_grid::event::render" };
                constexpr static auto       name_v          = xconst_universal_str  ( "world_grid::event::render" );
                using                       system_t        = system::advance_cell;
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: TOOLS::
        //----------------------------------------------------------------------------------------
        namespace tools
        {
            constexpr static int    grid_width_v  = 32;
            constexpr static int    grid_height_v = 32;

            constexpr static int    grid_width_half_v  = grid_width_v/2;
            constexpr static int    grid_height_half_v = grid_height_v/2;

            constexpr static float  world_width_v  = 5500.0f;
            constexpr static float  world_height_v = 3200.0f; 
            
            struct vector2
            {
                int m_X, m_Y;

                vector2 operator - ( const vector2& A ) const noexcept
                {
                    return { m_X - A.m_X, m_Y - A.m_Y };
                }

                vector2 operator + ( const vector2& A ) const noexcept
                {
                    return { m_X + A.m_X, m_Y + A.m_Y };
                }
            };

            struct box
            {
                tools::vector2 m_Min;
                tools::vector2 m_Max;
            };

            // https://stackoverflow.com/questions/17035464/a-fast-method-to-round-a-double-to-a-32-bit-int-explained
            xforceinline
            int double2int( double d ) noexcept
            {
                d += 6755399441055744.0;
                return reinterpret_cast<int&>(d);
            }

            xforceinline 
            int WorldToGridX( const float X ) noexcept { return double2int(X / grid_width_v); }

            xforceinline
            int WorldToGridY( const float Y ) noexcept { return double2int(Y / grid_height_v); }

            xforceinline
            tools::vector2 WorldToGrid( xcore::vector2 V ) noexcept
            {
                return{ WorldToGridX(V.m_X), WorldToGridY(V.m_Y) };
            }

            xforceinline
            box SearchBox( const tools::vector2 Pos ) noexcept
            {
                return box{ {Pos.m_X-1, Pos.m_Y-1}, {Pos.m_X+1, Pos.m_Y+1} };
            }

            xforceinline
            box MakeBox( const xcore::vector2& A, const xcore::vector2& B, const float Radius ) noexcept
            {
                const float d = (B - A).getLength() + Radius;
                return
                {
                      WorldToGrid(xcore::vector2{ A.m_X - d, A.m_Y - d })
                    , WorldToGrid(xcore::vector2{ A.m_X + d, A.m_Y + d })
                };
            }

            xforceinline constexpr
            std::uint64_t ComputeKeyFromPosition( const int X, const int Y ) noexcept
            {
                const auto x = (18446744073709551557ull ^ 9223372036854775ull) ^ (static_cast<std::uint64_t>(X)<<32) ^ static_cast<std::uint64_t>(Y);
                xassert(x);
                return x;
                
            }

            xforceinline constexpr
            std::uint64_t ComputeKeyFromPosition( const vector2 A ) noexcept
            {
                return ComputeKeyFromPosition( A.m_X, A.m_Y );
            }
        }
    
        //----------------------------------------------------------------------------------------
        // WORLD GRID:: COMPONENTS::
        //----------------------------------------------------------------------------------------
        namespace component
        {
            constexpr static std::size_t max_entity_count = 64;
            struct lists : mecs::component::quantum_double_buffer
            {
                constexpr static mecs::type_guid type_guid_v{"mecs::examples::E01_graphical_2d_basic_physics::world_grid::component::lists"};
                constexpr static auto name_v = xconst_universal_str("lists");

                struct entry
                {
                    xcore::vector2  m_Position;
                    xcore::vector2  m_Velocity;
                    float           m_Radius;
                };

                std::array< entry,                  max_entity_count >  m_lEntry;
                std::array< mecs::entity::instance, max_entity_count >  m_lEntity;
            };

            struct count : mecs::component::quantum_mutable
            {
                constexpr static mecs::type_guid type_guid_v{"mecs::examples::E01_graphical_2d_basic_physics::world_grid::component::count"};
                constexpr static auto name_v = xconst_universal_str("count");
                std::atomic< std::uint8_t >                             m_MutableCount  { 0 };
                std::uint8_t                                            m_ReadOnlyCount { 0 };        // Note this variable is naked/unprotected and should only be written by a single system 
            };
                
            struct id : tools::vector2
            {
                constexpr static mecs::type_guid type_guid_v{"mecs::examples::E01_graphical_2d_basic_physics::world_grid::component::id"};
                using tools::vector2::operator =;
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: SYSTEM::
        //----------------------------------------------------------------------------------------
        namespace system
        {
            //----------------------------------------------------------------------------------------
            // WORLD GRID:: SYSTEM:: ADVANCE CELL
            //----------------------------------------------------------------------------------------
            struct advance_cell : mecs::system::instance
            {
                constexpr static auto   name_v              = xconst_universal_str("advance_cell");
                constexpr static auto   guid_v              = mecs::system::guid{ "world_grid::advance_cell" };
                constexpr static auto   entities_per_job_v  = 50;

                using mecs::system::instance::instance;

                using access_t = mecs::query::access
                <       const component::lists&
                    ,   const component::id&
                    ,         component::lists&
                    ,         component::count&
                >;

                using events_t = std::tuple
                <
                        event::collision
                    ,   event::render
                >;

                xforceinline
                void operator() ( const access_t& Component ) noexcept
                {
                    std::array< std::array< const component::lists*,  3 >, 3 > T0CellMap    ;
                    std::array< std::array<       component::lists*,  3 >, 3 > T1CellMap    ;
                    std::array< std::array<       component::count*,  3 >, 3 > CellMapCount ;
                    std::array< std::array<       std::uint64_t,      3 >, 3 > CellGuids    ;

                    //
                    // Cache all relevant cells
                    //
                    const auto ID               = Component.get< const component::id& >();
                    const auto SearchBoxA       = tools::SearchBox( ID );
                    {
                        //XCORE_PERF_ZONE_SCOPED_N("CacheCells")
                        T0CellMap[1][1]             = &Component.get< const component::lists& > ();
                        T1CellMap[1][1]             = &Component.get< component::lists& >       ();
                        CellMapCount[1][1]          = &Component.get< component::count& >       ();
                        CellGuids[1][1]             = world_grid::tools::ComputeKeyFromPosition( Component.get< const component::id& >() );

                        for( int y = SearchBoxA.m_Min.m_Y; y<=SearchBoxA.m_Max.m_Y; ++y )
                        for( int x = SearchBoxA.m_Min.m_X; x<=SearchBoxA.m_Max.m_X; ++x )
                        {
                            if( x == ID.m_X && y == ID.m_Y ) continue;
                                
                            const auto RelativeGridPos  = tools::vector2{ 1 + x, 1 + y } - ID;

                            if( false == findEntity (  mecs::entity::guid{ CellGuids[RelativeGridPos.m_X][RelativeGridPos.m_Y] = world_grid::tools::ComputeKeyFromPosition(x,y) }
                                , [&]( component::count& Count, const component::lists& T0Lists, component::lists& T1Lists ) noexcept
                                {
                                    T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]             = &T0Lists;
                                    T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]             = &T1Lists;
                                    CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]          = &Count;
                                })
                            )
                            {
                                T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]    = nullptr;
                                CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y] = nullptr;
                            }
                        }
                    }

                    //
                    // Advance physics for our cell
                    //
                    //XCORE_PERF_ZONE_SCOPED_N("AdvancePhysics")
                    const float DT      = getTime().m_DeltaTime;
                    for( std::uint8_t CountT = CellMapCount[1][1]->m_ReadOnlyCount, i=0; i<CountT; ++i )
                    {
                        //
                        // Integrate
                        //
                        auto           Entity     = T0CellMap[1][1]->m_lEntity[i];
                        const auto&    T0Entry    = T0CellMap[1][1]->m_lEntry[i];
                        xcore::vector2 T1Velocity = T0Entry.m_Velocity;
                        xcore::vector2 T1Position = T0Entry.m_Position + T1Velocity * DT;

                        //
                        // Check for collisions with other objects
                        //
                        {
                            //XCORE_PERF_ZONE_SCOPED_N("CheckCollisions")
                            const auto     SearchBoxB = tools::MakeBox( T0Entry.m_Position, T1Position, T0Entry.m_Radius );
                            for( int  y     = SearchBoxB.m_Min.m_Y; y<=SearchBoxB.m_Max.m_Y; y++ )
                            for( int  x     = SearchBoxB.m_Min.m_X; x<=SearchBoxB.m_Max.m_X; x++ )
                            {
                                const auto RelativeGridPos  = tools::vector2{ 1 + x, 1 + y } - ID;

                                if( T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y] ) 
                                    for( std::uint8_t Count = CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_ReadOnlyCount, c = 0; c < Count; ++c )
                                    {
                                        xassert( T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntity[c].getGuid().isValid() );
                                        auto& T0Other = T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntry[c];
                                        if( &T0Entry == &T0Other ) continue;
                                        
                                        // Check for collision
                                        const auto V                    = (T0Other.m_Position - T1Position);
                                        const auto MinDistanceSquare    = xcore::math::Sqr(T0Other.m_Radius + T0Entry.m_Radius);
                                        const auto DistanceSquare       = V.Dot(V);
                                        if( DistanceSquare >= 0.001f && DistanceSquare <= MinDistanceSquare )
                                        {
                                            // Do hacky collision response
                                            const auto Normal           = V * -xcore::math::InvSqrt(DistanceSquare);
                                            T1Velocity = xcore::math::vector2::Reflect( Normal, T1Velocity );
                                            T1Position = T0Other.m_Position + Normal * (T0Other.m_Radius + T0Entry.m_Radius+0.01f);

                                            // Notify entities about the collision
                                            EventNotify<event::collision>( T0CellMap[1][1]->m_lEntity[i], T0CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntity[c] );
                                        }
                                    }
                            }
                        }

                        //
                        // Collide with the world
                        //
                        {
                            if( (T1Position.m_X - T0Entry.m_Radius) <= (tools::world_width_v/-2) )
                            {
                                T1Position.m_X = (tools::world_width_v/-2) + T0Entry.m_Radius;
                                T1Velocity.m_X = -T1Velocity.m_X;
                            }

                            if( (T1Position.m_Y - T0Entry.m_Radius) <= (tools::world_height_v/-2) )
                            {
                                T1Position.m_Y = (tools::world_height_v/-2) + T0Entry.m_Radius;
                                T1Velocity.m_Y = -T1Velocity.m_Y;
                            }

                            if( (T1Position.m_X + T0Entry.m_Radius) >= (tools::world_width_v/2) )
                            {
                                T1Position.m_X = (tools::world_width_v/2) - T0Entry.m_Radius;
                                T1Velocity.m_X = -T1Velocity.m_X;
                            }

                            if( (T1Position.m_Y + T0Entry.m_Radius) >= (tools::world_height_v/2) )
                            {
                                T1Position.m_Y = (tools::world_height_v/2) - T0Entry.m_Radius;
                                T1Velocity.m_Y = -T1Velocity.m_Y;
                            }
                        }

                        //
                        // Make sure we have the cells in question
                        //
                        const auto WorldPosition    = tools::WorldToGrid( T1Position );
                        const auto RelativeGridPos  = tools::vector2{1,1} + (WorldPosition - ID);
                        int C;
                        {
                            //XCORE_PERF_ZONE_SCOPED_N("MoveToT1")
                            if( CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y] == nullptr ) getOrCreatEntity
                            <
                                    component::lists
                                ,   component::count
                                ,   component::id
                            >(  mecs::entity::guid{ CellGuids[RelativeGridPos.m_X][RelativeGridPos.m_Y] }
                                // Get entry
                                , [&]( component::count& Count, component::lists& T1Lists ) noexcept
                                {
                                    CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]          = &Count;
                                    T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]             = &T1Lists;
                                }
                                // Create entry
                                ,   [&]( component::count& Count, component::id& ID, component::lists& T1Lists ) noexcept
                                {
                                    ID = WorldPosition;
                                    xassert( world_grid::tools::ComputeKeyFromPosition(ID) == CellGuids[RelativeGridPos.m_X][RelativeGridPos.m_Y] );

                                    CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]  = &Count;
                                    T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]     = &T1Lists;
                                }
                            );

                            //
                            // Copy the entry in to the cell
                            //
                                        C               = CellMapCount[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_MutableCount++;
                            auto&       Entry           = T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntry[C];
                            Entry.m_Position            = T1Position;
                            Entry.m_Velocity            = T1Velocity;
                            Entry.m_Radius              = T0Entry.m_Radius;
                            T1CellMap[RelativeGridPos.m_X][RelativeGridPos.m_Y]->m_lEntity[C] = Entity;
                        }

                        //
                        // Notify whoever cares about the info
                        //
                        EventNotify<event::render>( Entity, T1Position, T1Velocity, T0Entry.m_Radius );
                    }

                    //
                    // Render grid
                    //
                    if(s_MyMenu.m_bRenderGrid)
                    {
                        xcore::vector2 Position;
                        Position.m_X = static_cast<float>((world_grid::tools::grid_width_v  ) * ID.m_X );
                        Position.m_Y = static_cast<float>((world_grid::tools::grid_height_v ) * ID.m_Y );

                        Draw2DQuad( 
                          Position
                        , xcore::vector2{ world_grid::tools::grid_width_half_v -1, world_grid::tools::grid_height_half_v -1 }
                        , 0xaa0000aa );
                    }
                }
            };

            //----------------------------------------------------------------------------------------
            // WORLD GRID:: SYSTEM:: RESET COUNTS
            //----------------------------------------------------------------------------------------
            struct reset_counts : mecs::system::instance
            {
                constexpr static auto   name_v = xconst_universal_str("reset_counts");
                constexpr static auto   guid_v = mecs::system::guid{ "reset_counts" };
                constexpr static auto   entities_per_job_v  = 200;

                using mecs::system::instance::instance;

                // On the start of the game we are going to update all the Counts for the first frame.
                // Since entities have been created outside the system so our ReadOnlyCounter is set to zero.
                // But After this update we should be all up to date.
                void msgWorldStart ( world::instance& ) noexcept
                {
                    query::instance Query;
                    DoQuery<reset_counts>( Query );
                    if( false == ForEach<entities_per_job_v>(Query,*this) )
                    {
                        xassert(false);
                    }
                }

                // This system is reading T0 and it does not need to worry about people changing its value midway.
                xforceinline
                void operator() ( mecs::entity::instance& Entity, component::count& Count ) noexcept
                {
                    Count.m_ReadOnlyCount = Count.m_MutableCount.load(std::memory_order_relaxed);
                    if( Count.m_ReadOnlyCount == 0 )
                    {
                        deleteEntity(Entity);
                    }
                    else
                    {
                        Count.m_MutableCount.store(0,std::memory_order_relaxed);
                    }
                }
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: DELEGATE::
        //----------------------------------------------------------------------------------------
        namespace delegate
        {
            //----------------------------------------------------------------------------------------
            // WORLD GRID:: SYSTEM:: CREATE SPATIAL ENTITY
            //----------------------------------------------------------------------------------------
            struct create_spatial_entity : mecs::delegate::instance< mecs::event::create_entity >
            {
                constexpr static mecs::delegate::guid guid_v{ "world_grid::delegate::create_spatial_entity" };

                // This function will be call ones per entity. Since we only will have one entity it will only be call ones per frame.
                void operator() (       mecs::system::instance&         System
                                , const mecs::entity::instance&         Entity
                                , const example::component::position&   Position
                                , const example::component::velocity&   Velocity
                                , const example::component::collider&   Collider ) const noexcept
                {
                    const auto               GridPosition   = tools::WorldToGrid(Position);
                    const mecs::entity::guid Guid           { world_grid::tools::ComputeKeyFromPosition( GridPosition ) };

                    System.getOrCreatEntity
                    < 
                            component::lists
                        ,   component::count
                        ,   component::id
                    >
                    (
                        Guid
                        // Get entry
                        , [&]( component::lists& Lists, component::count& Count ) noexcept
                        {
                            const auto C = Count.m_MutableCount++;

                            auto& Entry = Lists.m_lEntry[C];
                            Entry.m_Position    = Position;
                            Entry.m_Velocity    = Velocity;
                            Entry.m_Radius      = Collider.m_Radius;
                            Lists.m_lEntity[C]  = Entity;
                        }
                        // Create Entry
                        , [&]( component::lists& Lists, component::count& Count, component::id& ID ) noexcept
                        {
                            ID = GridPosition;

                            Count.m_MutableCount.store( 1, std::memory_order_relaxed );

                            auto& Entry = Lists.m_lEntry[0];
                            Entry.m_Position    = Position;
                            Entry.m_Velocity    = Velocity;
                            Entry.m_Radius      = Collider.m_Radius;
                            Lists.m_lEntity[0]  = Entity;
                        }
                    );
                }
            };

            //----------------------------------------------------------------------------------------
            // WORLD GRID:: DELEGATE:: DESTROY SPATIAL ENTITY
            //----------------------------------------------------------------------------------------
            struct destroy_spatial_entity : mecs::delegate::instance< mecs::event::destroy_entity >
            {
                constexpr static mecs::delegate::guid guid_v{ "world_grid::delegate::destroy_spatial_entity" };

                // This function will be call ones per entity. Since we only will have one entity it will only be call ones per frame.
                void operator() (         mecs::system::instance&       System
                                ,   const mecs::entity::instance&       Entity
                                ,   const example::component::position& Position ) const noexcept
                {
                    /*
                    const auto               WorldPosition = tools::WorldToGrid(Position);
                    const mecs::entity::guid Guid{ world_grid::tools::ComputeKeyFromPosition( WorldPosition ) };

                    // remove entity from the spatial grid
                    System.getEntity( Guid,
                        [&]( mecs::query::define<mecs::query::access
                        <
                                component::countentity
                        >>::access& Access )
                        {
                            auto& lList = Access.get<component::countentity>().m_lEntity;
                            for( std::uint16_t end = Access.get<component::countentity>().m_Count.load( std::memory_order_relaxed ), i=0u; i<end; ++i )
                            {
                                if( &lList[i].getPointer() == &Entity.getPointer() )
                                {
                                    lList[i].MarkAsZombie();
                                    return;
                                }
                            }
                            xassert(false);
                        });
                    */                        
                }
            };
        }

        //----------------------------------------------------------------------------------------
        // WORLD GRID:: IMPORT
        //----------------------------------------------------------------------------------------
        inline
        void ImportModule( mecs::world::instance& World ) noexcept
        {
            World.registerComponents< component::lists, component::count, component::id >();
            World.registerDelegates< delegate::create_spatial_entity, delegate::destroy_spatial_entity >();
        }
    }

    //----------------------------------------------------------------------------------------
    // SYSTEMS::
    //----------------------------------------------------------------------------------------
    namespace system
    {
        //----------------------------------------------------------------------------------------
        // SYSTEM:: RENDER_OBJECTS
        //----------------------------------------------------------------------------------------
        struct render_objects : mecs::system::instance
        {
            constexpr static auto   name_v              = xconst_universal_str("render_objects");
            constexpr static auto   guid_v              = mecs::system::guid{ "graphics::render_objects" };
            constexpr static auto   entities_per_job_v  = 1000;
            using mecs::system::instance::instance;

            xforceinline
            void operator() ( const component::position& Position, const component::collider& Collider ) noexcept
            {
                Draw2DQuad( Position, xcore::vector2{ Collider.m_Radius }, ~0 );
            }
        };

        //----------------------------------------------------------------------------------------
        // SYSTEM:: RENDER_PAGEFLIP
        //----------------------------------------------------------------------------------------
        struct render_pageflip : mecs::system::instance
        {
            constexpr static auto   name_v              = xconst_universal_str("render_pageflip");
            constexpr static auto   guid_v              = mecs::system::guid{ "graphics::render_pageflip" };

            render_pageflip(instance::construct&& Construct)
                : mecs::system::instance{ [&] { auto C = Construct; C.m_Def.m_Priority = xcore::scheduler::priority::ABOVE_NORMAL; return C; }() }
            {
                s_bContinue = true;
            }

            inline
            void msgUpdate( void ) noexcept
            {
                s_bContinue = m_Menu();
                PageFlip();
            }

            xcore::func<bool(void)> m_Menu;
            static inline bool s_bContinue;
        };
    }

    //----------------------------------------------------------------------------------------
    // DELEGATE::
    //----------------------------------------------------------------------------------------
    namespace delegate
    {
        struct my_collision : mecs::delegate::instance< world_grid::event::collision >
        {
            constexpr static mecs::delegate::guid guid_v{ "my_collision_delegate" };

            xforceinline 
            void operator() ( mecs::system::instance& System, const mecs::entity::instance& E1, const mecs::entity::instance& E2 ) const noexcept
            {
                //printf( ">>>>>>>>>>Collision \n");
            }
        };

        struct my_render : mecs::delegate::instance< world_grid::event::render >
        {
            constexpr static mecs::delegate::guid guid_v{ "my_render" };

            xforceinline 
            void operator() ( mecs::system::instance&, const mecs::entity::instance&, const xcore::vector2& Position, const xcore::vector2&, float R ) const noexcept
            {
                Draw2DQuad( Position, xcore::vector2{ R }, ~0 );
            }
        };

    }

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    inline
    void Test( bool(*Menu)(mecs::world::instance&,xcore::property::base& ) )
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E01_graphical_2d_basic_physics\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        // Import that world_grid system
        world_grid::ImportModule( *upWorld );

        //
        // Register Components
        //
        upWorld->registerComponents<component::position,component::collider,component::velocity>();

        //
        // Register Delegates
        //
        upWorld->registerDelegates<delegate::my_collision, delegate::my_render>();

        //
        // Synpoint before the physics
        //
        mecs::sync_point::instance SyncPhysics;

        //
        // Register the game graph.
        // 
        upWorld->registerGraphConnection<world_grid::system::advance_cell>  ( upWorld->m_StartSyncPoint,    SyncPhysics             );
        upWorld->registerGraphConnection<world_grid::system::reset_counts>  ( SyncPhysics,                  upWorld->m_EndSyncPoint );
        upWorld->registerGraphConnection<system::render_pageflip>           ( SyncPhysics,                  upWorld->m_EndSyncPoint ).m_Menu = [&] { return Menu(*upWorld, s_MyMenu); };

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup<component::position,component::collider,component::velocity>();

        //
        // Create Entities
        //
        xcore::random::small_generator Rnd;
        for( int i=0; i<s_MyMenu.m_EntitieCount; i++ )
            upWorld->createEntity( Group, [&](  component::position&  Position
                                            ,   component::velocity&  Velocity
                                            ,   component::collider&  Collider )
            {
                Position.setup( Rnd.RandF32(-(world_grid::tools::world_width_v/2.0f), (world_grid::tools::world_width_v/2.0f) )
                              , Rnd.RandF32(-(world_grid::tools::world_height_v/2.0f), (world_grid::tools::world_height_v/2.0f) ) );
                

                Velocity.setup( Rnd.RandF32(-1.0f, 1.0f ), Rnd.RandF32(-1.0f, 1.0f ) );
                Velocity.NormalizeSafe();
                Velocity *= 10.0f;

                Collider.m_Radius = Rnd.RandF32( 1.5f, 3.0f );
            });

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 100 frames
        //
        while (system::render_pageflip::s_bContinue)
        {
            upWorld->Resume();
        }
    }
}

property_begin_name( mecs::examples::E01_graphical_2d_basic_physics::my_menu, "Settings" )
{
    property_var( m_EntitieCount )
        .Help(  "This specifies how many entities are going to get spawn.\n"
                "You must use the upper button call 'Use Settings' to \n"
                "activate it.")
,   property_var( m_bRenderGrid )
        .Help(  "Tells the system to render the spatial grid used for the entities.\n"
                "This spatial grid is used for the broad phase of the collision.\n"
                "This is done in real time.")
,   property_var_fnbegin( "Settings", string_t )
    {
        if( isRead ) xcore::string::Copy( InOut, "Reset To Defaults" );
        else         mecs::examples::E01_graphical_2d_basic_physics::s_MyMenu.reset();
    }
    property_var_fnend()
        .EDStyle(property::edstyle<string_t>::Button())
        .Help(  "Will reset these settings menu back to the original values. \n"
                "Please note that to respawn the entities you need to use \n"
                "the 'Use Settings' button at the top.")
}
property_vend_h(mecs::examples::E01_graphical_2d_basic_physics::my_menu);

