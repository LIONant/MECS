namespace mecs::examples::E11_access_components
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    struct position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
    };

    struct velocity : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"velocity component"};
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_system : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "move_system" };
        using                   access_t        = query::access< const mecs::entity::instance&, position&, const velocity& >; 
        using mecs::system::instance::instance;

        // Note we are using this type a our own type "access_t" to access our components rather than traditional parameters.
        // This can help the compiler with optimizations as well as keeping the local variables to lower counts.
        // Note that inside the "query::access::m_Tuple" there is the tuple which contains all the data.
        // Also note how our type access mirror pretty much what we would have to use in a parameter list minus the names for the variables.
        void operator() ( const access_t& Access ) const noexcept
        {
            Access.get<position&>() += Access.get<const velocity&>() * getTime().m_DeltaTime;
            
            printf( "%I64X moved to {.x = %f, .y = %f}\n"
            , Access.get<const mecs::entity::instance&>().getGuid().m_Value
            , Access.get<position&>().m_X
            , Access.get<position&>().m_Y );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E11_access_components\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<position, velocity>();

        //
        // Create the game graph.
        // 
        upWorld->registerGraphConnection<move_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group. The entities in this group will have two components, position and velocity.
        //
        auto& Group = upWorld->getOrCreateGroup<position,velocity>();

        //
        // Create an entity and initialize its components
        //
        upWorld->createEntity( Group, []( position& Position, velocity& Velocity )
        {
            Position.setup( 0.0f, 0.0f );
            Velocity.setup( 1.0f, 1.0f );
        });

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 10 frames
        //
        for( int i=0; i<10; i++) upWorld->Resume();
    }

}