namespace mecs::examples::E06_general_custom_delegates
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    struct position 
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
        int m_X{ 0 };
    };

    //-----------------------------------------------------------------------------------------
    // General Custom Events
    //-----------------------------------------------------------------------------------------

    struct my_sound_event : mecs::event::instance
    {
        using                       event_t         = mecs::event::type< mecs::system::instance&, const char* >;
        constexpr static auto       guid_v          = mecs::event::guid     { "my_sound_event" };
        constexpr static auto       name_v          = xconst_universal_str  ( "my_sound_event" );
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_system : mecs::system::instance
    {
        using mecs::system::instance::instance;

        constexpr static auto guid_v = mecs::system::guid{ "move_system" };

        using events_t = std::tuple
        <
            my_sound_event
        >;

        void operator() ( const entity::instance& Entity, position& Position ) noexcept
        {
            Position.m_X ++;

            if( Position.m_X == 10 )
            {
                // Spawn a new entity with position
                createEntity<position>();
            }

            // Tell where is our entity
            printf("Entity[%I64X] Walking (%d) \n", Entity.getGuid().m_Value, Position.m_X );

            // Send general events
            GeneralEventNotify< move_system, my_sound_event >( "Step sound.wav" );
        }
    };

    //-----------------------------------------------------------------------------------------
    // General Custom Delegates
    //-----------------------------------------------------------------------------------------
    // These delegates are exactly the same as the custom delegates
    //-----------------------------------------------------------------------------------------
    struct my_sound_delegate : mecs::delegate::instance< my_sound_event >
    {
        constexpr static mecs::delegate::guid guid_v{ "my_sound_delegate" };

        static void PlaySound( const char* pSoundFile ) noexcept
        {
            printf("Playing a sound: %s \n", pSoundFile );
        }

        void operator() ( mecs::system::instance& System, const char* pSoundFile ) const noexcept
        {
            PlaySound(pSoundFile);
        }
    };

    //-----------------------------------------------------------------------------------------
    // Delegates
    //-----------------------------------------------------------------------------------------
    struct spawn_delegate : mecs::delegate::instance< mecs::event::create_entity >
    {
        constexpr static mecs::delegate::guid guid_v{ "my_sound_delegate" }; 

        my_sound_delegate* m_pCacheDelegate { nullptr };

        // This function is an override function that only gets call ones at the start of the game.
        // delegates and system has several overwrite functions.
        void msgWorldStart( world::instance& World ) noexcept
        {
            // lets cache the delegate that we are interested
            m_pCacheDelegate = reinterpret_cast<my_sound_delegate*>( World.m_mapDelegates.get( my_sound_delegate::guid_v ) );
        }

        // Any entity will trigger this sound right now... we could filter which kinds of entities we want.
        void operator() ( mecs::system::instance& System ) const noexcept
        {
            // Route all sound playing to the sound_delegate
            if(m_pCacheDelegate)(*m_pCacheDelegate)(System, "Spawn Sound.wav");
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E06_general_custom_delegates\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<position>();

        //
        // Create the game graph.
        // 
        upWorld->registerGraphConnection<move_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Register delegates
        //
        upWorld->registerDelegates<my_sound_delegate, spawn_delegate>();

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup<position>();

        //
        // Create one entity
        //
        upWorld->createEntity( Group );

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 100 frames
        //
        for( int i=0; i<10; i++) upWorld->Resume();
    }
}

