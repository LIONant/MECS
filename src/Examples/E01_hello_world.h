namespace mecs::examples::E01_hello_world
{
    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E01_hello_world\n");
        printf( "--------------------------------------------------------------------------------\n");

        // Create an instance of the world. Much better to create it in the heap than in the stack!
        auto upWorld = std::make_unique<mecs::world::instance>();

        // Nothing to register to lets end it
        upWorld->registerFinish();

        // Start executing the world
        upWorld->Start();

        // 10 frames of nothing...
        for( int i=0; i<10; i++) upWorld->Resume();
    }
}