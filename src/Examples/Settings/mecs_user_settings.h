namespace mecs::settings
{
    constexpr static std::size_t    max_systems_v                   = 256;
    constexpr static std::size_t    max_components_in_entity_v      = 64;
    constexpr static std::size_t    max_tags_in_entity_v            = 64;
    constexpr static std::size_t    max_delegates_count_v           = 512;
    constexpr static std::size_t    max_groups_count_v              = 1024;

    constexpr static std::uint32_t  fixed_deltatime_microseconds_v  = 16 * 1000;
}