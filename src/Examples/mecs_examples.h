
#include "E01_hello_world.h"
#include "E02_system_introduction.h"
#include "E03_system_moving_entities.h"
#include "E04_delegate_moveinout.h"
#include "E05_custom_delegate_updated_entity.h"
#include "E06_geneal_custom_delegates.h"
#include "E07_double_buffer_components.h"
#include "E08_quantum_mutable_components.h"
#include "E09_quantum_double_buffer_components.h"
#include "E10_shared_components.h"
#include "E11_access_components.h"
#include "E12_tentative_components.h"

#include "E15_2d_basic_physics.h"

namespace mecs::examples
{
    void Test(void)
    {
        mecs::examples::E07_double_buffer_components::Test();

        if(0)
        {
            mecs::examples::E01_hello_world::Test();
            mecs::examples::E02_system_introduction::Test();
            mecs::examples::E03_system_moving_entities::Test();
            mecs::examples::E04_delegate_moveinout::Test();
            mecs::examples::E05_custom_delegate_updated_entity::Test();
            mecs::examples::E06_general_custom_delegates::Test();
            mecs::examples::E07_double_buffer_components::Test();
            mecs::examples::E08_quantum_mutable_components::Test();
            mecs::examples::E09_quantum_double_buffer_components::Test();
            mecs::examples::E10_shared_components::Test();
            mecs::examples::E11_access_components::Test();
            mecs::examples::E12_tentative_components::Test();
            mecs::examples::E15_2d_basic_physics::Test();
        }
    }
}
