namespace mecs::examples::E03_system_moving_entities
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    // In this example we show how you can derived components from other structures.
    // Also note how we created the type_guid_v. We initialize with a unique string.
    struct position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
    };

    struct velocity : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"velocity component"};
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_system : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "move_system" };

        using mecs::system::instance::instance;

        // in this example we show how you can access the entity component itself. Just like
        // any other type of component we should use const property. The entity component
        // is not used very often and its main job is to hold a Global Unique Identifier.
        // This GUID is how you can differentiate amount entities. 
        void operator() ( const mecs::entity::instance& Entity, position& Position, const velocity& Velocity ) const noexcept
        {
            Position += Velocity * getTime().m_DeltaTime;
            
            printf( "%I64X moved to {.x = %f, .y = %f}\n"
            , Entity.getGuid().m_Value
            , Position.m_X
            , Position.m_Y );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E03_system_moving_entities\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<position, velocity>();

        //
        // Create the game graph.
        // 
        upWorld->registerGraphConnection<move_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group. The entities in this group will have two components, position and velocity.
        //
        auto& Group = upWorld->getOrCreateGroup<position,velocity>();

        //
        // Create an entity and initialize its components
        //
        upWorld->createEntity( Group, []( position& Position, velocity& Velocity )
        {
            Position.setup( 0.0f, 0.0f );
            Velocity.setup( 1.0f, 1.0f );
        });

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 10 frames
        //
        for( int i=0; i<10; i++) upWorld->Resume();
    }

}