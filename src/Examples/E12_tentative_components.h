namespace mecs::examples::E12_tentative_components
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    struct position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
    };

    struct velocity : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"velocity component"};
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_system : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "move_system" };
        using                   access_t        = query::access< const mecs::entity::instance&, position&, const velocity* >; 
        using mecs::system::instance::instance;

        // This example show cases how one system can handle entities that may or may nor have velocities.
        // Note that the way we indicate that a component is optional is by asking for its pointer.
        // This mirrors good practices of C++. This helps reduce the number of systems that you need.
        // This type of references/pointers are call tentative references because you may get nothing.
        // If we use query::access like in there we can use the "isValid" to check if we have it,
        // and "get" to get its reference (note that the "get" returns a reference and not the pointer)
        void operator() ( const access_t& Access ) const noexcept
        {
            if( Access.isValid<const velocity*>() )
            {
                Access.get<position&>() += Access.get<const velocity*>() * getTime().m_DeltaTime;
            }
            
            printf( "%I64X moved to {.x = %f, .y = %f}\n"
            , Access.get<const mecs::entity::instance&>().getGuid().m_Value
            , Access.get<position&>().m_X
            , Access.get<position&>().m_Y );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct print_velocity2 : mecs::system::instance
    {
        constexpr static auto   guid_v          = mecs::system::guid{ "move_system2" };
        using mecs::system::instance::instance;

        // Similar to the system above this one also takes a tentative reference to velocity.
        // However because we are not using the "query::access" we are dealing with a naket pointer.
        // So we must check to make sure is not null before using it.
        // As you can see the function signature can handle allot of the details to query the groups.
        void operator() ( const mecs::entity::instance& Entity, const velocity* pV ) const noexcept
        {
            if( pV )
            {
                printf( "%I64X velocity {.x = %f, .y = %f}\n"
                , Entity.getGuid().m_Value
                , pV->m_X
                , pV->m_Y );
            }
            else
            {
                printf( "%I64X velocity { none }\n"
                , Entity.getGuid().m_Value );
            }
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E12_tentative_components\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<position, velocity>();

        //
        // Create the game graph.
        // 
        upWorld->registerGraphConnection<move_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );
        upWorld->registerGraphConnection<print_velocity2>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group. The entities in this group will have two components, position and velocity.
        //
        auto& GroupPV = upWorld->getOrCreateGroup<position,velocity>();
        auto& GroupP  = upWorld->getOrCreateGroup<position>();

        //
        // Create an entity and initialize its components
        //
        upWorld->createEntity( GroupPV, []( position& Position, velocity& Velocity )
        {
            Position.setup( 0.0f, 0.0f );
            Velocity.setup( 1.0f, 1.0f );
        });

        upWorld->createEntity( GroupP, []( position& Position )
        {
            Position.setup( 0.0f, 0.0f );
        });

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 10 frames
        //
        for( int i=0; i<10; i++) 
            upWorld->Resume();
    }

}