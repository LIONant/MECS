namespace mecs::examples::E10_shared_components
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------

    struct material
    {
        constexpr static mecs::type_guid type_guid_v{"material"};
        int m_DiffuseTexture;
    };

    // Thanks to this value group all entries that have this component will be subgroup by the value.
    // This allows entities to execute more cache friendly. In this example for instance all submeshes
    // with the same material will be grouped together. So only one look up is necessary for the material
    // and from a memory accesses point of view is also allot better.
    struct material_ref : mecs::component::value_subgroup
    {
        constexpr static mecs::type_guid type_guid_v{"material_ref"};

        std::uint64_t getValue( void ) const noexcept { return m_gMaterial.m_Value; }
        mecs::entity::guid  m_gMaterial;
    };

    struct submesh
    {
        constexpr static mecs::type_guid type_guid_v{"submesh"};
        int m_nIndices;
        int m_nVertices;
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct render_system : mecs::system::instance
    {
        using mecs::system::instance::instance;

        constexpr static auto guid_v = mecs::system::guid{ "render_system" };

        // We will be incrementing the value of value. Note that while the syntax looks like a simple
        // increment in reality is an atomic increment. So there is magic behind that add.
        void operator() ( const submesh& SubMesh, const material_ref& MaterialRef  ) noexcept
        {
            getEntity( MaterialRef.m_gMaterial, [&]( const material& Material )
            {
                printf( "Rendering( Material: %I64X, DiffuseTexture: %d, Indices:%d Vertices: %d\n",
                                    MaterialRef.m_gMaterial.m_Value
                                ,   Material.m_DiffuseTexture
                                ,   SubMesh.m_nIndices
                                ,   SubMesh.m_nVertices );
            });
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E10_shared_components\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<material, material_ref, submesh >();

        //
        // Create the game graph.
        // These two systems can run in parallel thanks to the double buffer component, other wise it will be a problem.

        upWorld->registerGraphConnection<render_system>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );


        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create the Material group
        //
        auto& MaterialGroup = upWorld->getOrCreateGroup<material>();

        //
        // Crete a few materials
        //
        auto gMaterialOne = mecs::entity::guid{ "MaterialOne" };
        auto gMaterialTwo = mecs::entity::guid{ "MaterialTwo" };

        upWorld->createEntity( MaterialGroup,[](material& Material)
        {
            Material.m_DiffuseTexture = 100;
        }, gMaterialOne );

        upWorld->createEntity( MaterialGroup,[](material& Material)
        {
            Material.m_DiffuseTexture = 22;
        }, gMaterialTwo );

        //
        // Crete a few submeshes
        //
        auto& SubmeshGroup = upWorld->getOrCreateGroup< submesh, material_ref >();

        upWorld->createEntity( SubmeshGroup,[&](submesh& Submesh, material_ref& MaterialRef )
        {
            Submesh.m_nIndices      = 101;
            Submesh.m_nVertices     = 5;
            MaterialRef.m_gMaterial = gMaterialOne;
        });

        upWorld->createEntity( SubmeshGroup,[&](submesh& Submesh, material_ref& MaterialRef)
        {
            Submesh.m_nIndices      = 1003;
            Submesh.m_nVertices     = 1;
            MaterialRef.m_gMaterial = gMaterialTwo;
        });

        upWorld->createEntity( SubmeshGroup,[&](submesh& Submesh, material_ref& MaterialRef)
        {
            Submesh.m_nIndices      = 2;
            Submesh.m_nVertices     = 10;
            MaterialRef.m_gMaterial = gMaterialOne;
        });

        upWorld->createEntity( SubmeshGroup,[&](submesh& Submesh, material_ref& MaterialRef)
        {
            Submesh.m_nIndices      = 1;
            Submesh.m_nVertices     = 0;
            MaterialRef.m_gMaterial = gMaterialTwo;
        });

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 100 frames
        //
        for( int i=0; i<100; i++) upWorld->Resume();
    }
}

