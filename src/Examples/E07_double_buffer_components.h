namespace mecs::examples::E07_double_buffer_components
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    // Double buffer components are design to made modifications while another system reads its
    // previous state in a safe way. Which means that there are two copies of the component.
    // One used for writing and the other used for reading. This can be thought of having to
    // different times [T0 - what is the current state] [T1 - What will be the future state]
    // T0 is the read only state and it must have a const. T1 is the writing only state and
    // it must not have a const. Only one system can have access to T1, but many systems can
    // have access to T0 as it does not change. T0 will become T1 and vice versa at the syncpoint. 
    struct position : mecs::component::double_buffer
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
        int m_X{ 0 };
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_system : mecs::system::instance
    {
        using mecs::system::instance::instance;

        constexpr static auto guid_v = mecs::system::guid{ "move_system" };

        // This system can modify T1 position safely without worrying if there are other systems
        // reading T0.
        void operator() ( position& T1Position, const position& T0Position ) noexcept
        {
            T1Position.m_X = T0Position.m_X + 1;
        }
    };

    //-----------------------------------------------------------------------------------------

    struct print_position : mecs::system::instance
    {
        using mecs::system::instance::instance;

        constexpr static auto guid_v = mecs::system::guid{ "print_position" };

        // This system is reading T0 and it does not need to worry about people changing its value midway.
        void operator() ( const entity::instance& Entity, const position& T0Position ) noexcept
        {
            printf( "%I64X moved to {.x = %d}\n"
                , Entity.getGuid().m_Value
                , T0Position.m_X );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E07_double_buffer_components\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<position>();

        //
        // Create the game graph.
        // These two systems can run in parallel thanks to the double buffer component, other wise it will be a problem.
        upWorld->registerGraphConnection<move_system>   ( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );
        upWorld->registerGraphConnection<print_position>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup<position>();

        //
        // Create one entity
        //
        upWorld->createEntity( Group );

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 10 frames
        //
        for( int i=0; i<10; i++) 
            upWorld->Resume();
    }
}

