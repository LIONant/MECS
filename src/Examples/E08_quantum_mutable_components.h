namespace mecs::examples::E08_quantum_mutable_components
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    // Quantum mutable components are components that are meant to be read and write at the same time.
    // As you can imagine only that is possible if you have atomic variables or a way to linearize such as locks.
    // For such components there is no protection at all. So be very careful when you use them.
    struct value : mecs::component::quantum_mutable
    {
        constexpr static mecs::type_guid type_guid_v{"value component"};
        std::atomic<int> m_Value{ 0 };
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct counter_up: mecs::system::instance
    {
        using mecs::system::instance::instance;

        constexpr static auto guid_v = mecs::system::guid{ "counter_up" };

        // We will be incrementing the value of value. Note that while the syntax looks like a simple
        // increment in reality is an atomic increment. So there is magic behind that add.
        void operator() ( value& Value ) noexcept
        {
            Value.m_Value++;
        }
    };

    //-----------------------------------------------------------------------------------------

    struct counter_down: mecs::system::instance
    {
        using mecs::system::instance::instance;

        constexpr static auto guid_v = mecs::system::guid{ "counter_down" };

        // Here mecs doesn't care that there is another system trying to write to this variable
        // at the same time because it is a quantum component. As a user you better
        // know what you are doing
        void operator() ( value& Value ) noexcept
        {
            Value.m_Value--;
        }
    };

    //-----------------------------------------------------------------------------------------
    // Delegate 
    //-----------------------------------------------------------------------------------------
    
    struct mutated_value_delegate : mecs::delegate::instance< mecs::event::updated_entity >
    {
        constexpr static mecs::delegate::guid guid_v{ "mutated_value_delegate" }; 

        void operator() ( mecs::system::instance& System, const mecs::entity::instance& Entity, const value& Value ) const noexcept
        {
            // The value of value will be undetermined since it lives in the quantum world.
            // However we know that the range will be from -1 to 1 because for a given frame it is been incremented and decremented 
            printf("System: %s, Entity: %I64X, New Value %d\n"
                , System.getGuid() == counter_down::guid_v ? "CountDown" 
                                                           : "CountUp  "
                , Entity.getGuid().m_Value
                , Value.m_Value.load( std::memory_order_relaxed ) );
        }
    };

    //-----------------------------------------------------------------------------------------
    // Test
    //-----------------------------------------------------------------------------------------
    void Test()
    {
        printf( "--------------------------------------------------------------------------------\n");
        printf( "E08_quantum_mutable_components\n");
        printf( "--------------------------------------------------------------------------------\n");

        auto upWorld = std::make_unique<mecs::world::instance>();

        //------------------------------------------------------------------------------------------
        // Registration
        //------------------------------------------------------------------------------------------

        //
        // Register Components
        //
        upWorld->registerComponents<value>();

        //
        // Create the game graph.
        // These two systems can run in parallel thanks to the double buffer component, other wise it will be a problem.
        upWorld->registerGraphConnection<counter_up>  ( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );
        upWorld->registerGraphConnection<counter_down>( upWorld->m_StartSyncPoint, upWorld->m_EndSyncPoint );

        //
        // Register delates
        //
        upWorld->registerDelegates<mutated_value_delegate>();

        //
        // Done with all the registration
        //
        upWorld->registerFinish();

        //------------------------------------------------------------------------------------------
        // Initialization
        //------------------------------------------------------------------------------------------

        //
        // Create an entity group
        //
        auto& Group = upWorld->getOrCreateGroup<value>();

        //
        // Create one entity
        //
        upWorld->createEntity( Group );

        //------------------------------------------------------------------------------------------
        // Running
        //------------------------------------------------------------------------------------------

        //
        // Start executing the world
        //
        upWorld->Start();

        //
        // run 10 frames
        //
        for( int i=0; i<10; i++) upWorld->Resume();
    }
}

