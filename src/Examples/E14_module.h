
namespace mecs::examples::E04_module
{
    //-----------------------------------------------------------------------------------------
    // Components
    //-----------------------------------------------------------------------------------------
    struct position : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"position component"};
    };

    struct velocity : xcore::vector2
    {
        constexpr static mecs::type_guid type_guid_v{"velocity component"};
    };

    //-----------------------------------------------------------------------------------------
    // Systems
    //-----------------------------------------------------------------------------------------
    struct move_system : mecs::system::instance
    {
        using mecs::system::instance::instance;

        // This function will be call ones per entity. Since we only will have one entity it will only be call ones per frame.
        void operator() ( const mecs::entity::instance& Entity, position& Position, const velocity& Velocity ) const noexcept
        {
            Position += Velocity * getTime().m_DeltaTime;
            
            printf( "%I64X moved to {.x = %f, .y = %f}\n"
            , Entity.getGuid().m_Value
            , Position.m_X
            , Position.m_Y );
        }
    };

    //-----------------------------------------------------------------------------------------
    // ImportModule
    //-----------------------------------------------------------------------------------------
    inline
    void ImportModule( mecs::world::instance& Instance ) noexcept
    {
        Instance.registerComponents
        < 
                position
            ,   velocity
        >();
    }
}

