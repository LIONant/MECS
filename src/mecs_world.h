namespace mecs::world
{
    namespace details
    {
        struct component_desc
        {
            using fn_construct = void(void*);
            using fn_destruct  = void(void*);

            type_guid       m_Guid;
            fn_construct&   m_Construct;
            fn_destruct&    m_Destruct;
            std::uint16_t   m_Size;
            bool            m_isDoubleBuffer;
        };

        /*
        struct subgroup
        {
            xcore::vector<component::group::instance*>  m_lSubgroups;
            std::uint8_t                                m_nKeys;
        };
        */

        struct dbase
        {
            struct tag_entry
            {
                struct group_entry
                {
                    query::filter_bits                          m_GroupBits;
                    component::group::instance*                 m_pGroup{nullptr};
                };

                type_guid                                                   m_Guid;         // Tag UID (Sum of all individual tags)
                query::filter_bits                                          m_TagBits;      // Tag bitsets (Unique bit set for each tag)
                std::uint32_t                                               m_nGroups;      
                xcore::array<group_entry,32>                                m_lGroups;
                std::uint32_t                                               m_nTags;      
                xcore::array<type_guid,settings::max_tags_in_entity_v>      m_lTagGuids;    // Individual tag guids that make this entry
            };

            std::uint32_t                                       m_nTagEntries;
            xcore::array<tag_entry,128>                         m_lTagEntries;
        };

        struct tag_lookup
        {
            dbase::tag_entry*   m_pEntry;
            std::uint8_t        m_BitIndex;
        };

        struct default_system : mecs::system::instance
        {
            constexpr static auto                guid_v         = system::guid{ "mecs::world::default_system" };
            constexpr static auto                system_name_v  = xconst_universal_str("mecs::world::default_system");
            using mecs::system::instance::instance;
            xforceinline void msgUpdate ( void ) noexcept {}
        };

        template< typename T, typename T_WORLD = world::instance >
        struct register_events;

        template< typename...T_ARGS, typename T_WORLD >
        struct register_events<std::tuple<T_ARGS...>, T_WORLD >
        {
            template< typename T_SYSTEM > xforceinline
            static void Register( T_WORLD& This, T_SYSTEM& System ) noexcept
            {
                if constexpr ( sizeof...(T_ARGS) != 0 )
                {
                    ((  [&]() constexpr noexcept
                        {
                            void* p;
                            This.m_mapEvents.getOrCreate( T_ARGS::guid_v
                                // Get
                                , [&]( auto& Node ) constexpr noexcept
                                {
                                    p = Node.m_Value;
                                }
                                // Create
                                , [&]( auto& Node ) constexpr noexcept
                                {
                                    p = Node.m_Value = &std::get< xcore::types::make_unique< typename T_ARGS::event_t, T_ARGS > >( System.m_Events );
                                });
                            return p;
                        }()
                    ), ... );
                }
            }
        };
    }

    //---------------------------------------------------------------------------------
    // WORLD::events
    //---------------------------------------------------------------------------------
    struct events
    {
        using world_start   = xcore::types::make_unique< xcore::event::type<world::instance&>, struct world_start_tag >;
        using frame_start   = xcore::types::make_unique< xcore::event::type<>, struct frame_start_tag >;
        using frame_done    = xcore::types::make_unique< xcore::event::type<>, struct frame_done_tag >;

        world_start m_WorldStart;
        frame_start m_FrameStart;
        frame_done  m_FrameDone;
    };

    //---------------------------------------------------------------------------------
    // WORLD::instance
    //---------------------------------------------------------------------------------
    struct instance : xcore::scheduler::job<1>
    {
        sync_point::instance    m_StartSyncPoint;
        sync_point::instance    m_EndSyncPoint;

        using events = mecs::world::events;

        void msgSyncPointDone( sync_point::instance& SyncPoint )
        {
            auto& Scheduler = xcore::get().m_Scheduler;
            EndFrameUpdate();
            Scheduler.MainThreadStopWorking();
        }

        void EndFrameUpdate( void )
        {
            XCORE_PERF_ZONE_SCOPED()

            m_bFrameStarted = false;

            //
            // Clean up all deleted entities
            //
            {
                /*
                XCORE_PERF_ZONE_SCOPED_N("Clean up all deleted entities")
                for( auto pEntry : m_DeleteList )
                {
                    m_mapEntity.free( pEntry->m_Key,[](auto&){} );
                }
                m_DeleteList.clear();
                */

                /*
                for( auto pNext = m_EntityFreeList.load(std::memory_order_relaxed); pNext; pNext = reinterpret_cast<entity::map_t::entry*>(pNext->m_Value.m_pGroupInstance))
                {
                    m_mapEntity.free( pNext->m_Key, [](auto&){} );
                }
                m_EntityFreeList.store( nullptr, std::memory_order_relaxed );
                */
            }

            //
            // Let everyone know that we are done
            //
            m_Events.m_FrameDone.NotifyAll();

            //
            // Deal with profiler
            //
            XCORE_PERF_FRAME_MARK_END("mecs::Frame")
            XCORE_PERF_PLOT("Total Pages", static_cast<int64_t>(m_PageMgr.m_TotalPages.load(std::memory_order_relaxed)) )
            XCORE_PERF_PLOT("Free Pages",  static_cast<int64_t>(m_PageMgr.m_FreePages.load(std::memory_order_relaxed)) )
        }

        const sync_point::events::done::delegate m_SyncPointDoneDelegate{ this, &instance::msgSyncPointDone, m_EndSyncPoint.m_Events.m_Done };

        instance()
        {
            m_Definition.m_Triggers = xcore::scheduler::triggers::DONT_CLEAN_COUNT;
            registerComponents<entity::instance>();
            m_EndSyncPoint.AddJobToBeTrigger(*this);
            m_EndSyncPoint.FreezCount();
            m_EndSyncPoint.m_bDisable = true;

            m_StartSyncPoint.JobWillNotifyMe(*this);
            m_StartSyncPoint.FreezCount();

            m_pDefaultSystem = &registerGraphConnection<details::default_system>( m_StartSyncPoint, m_EndSyncPoint );
        }

        virtual void qt_onRun( void ) noexcept override
        {
            XCORE_PERF_FRAME_MARK_START("mecs::Frame")
            m_bFrameStarted = true;
            m_FrameNumber++;
            m_Events.m_FrameStart.NotifyAll();
        }

        void Start( void )
        {
            //
            // Make sure that the scene is ready to go
            //
            for( const auto& TagE : m_TagDBase.m_lTagEntries.ViewTo(m_TagDBase.m_nTagEntries) )
                for( auto& GroupE : TagE.m_lGroups.ViewTo(TagE.m_nGroups) )
                {
                    GroupE.m_pGroup->Start();
                    //GroupE.m_pGroup->m_Count = GroupE.m_pGroup->m_NewCount.load(std::memory_order_relaxed);
                    //GroupE.m_pGroup->m_NewCount.store( 0, std::memory_order_relaxed );
                }

            //
            // Make sure all groups have their caches ready
            //
            for( auto& E : m_lSystems )
                E->clearGroupCache();

            //
            // Notify All
            //
            m_Events.m_WorldStart.NotifyAll(*this);

            //
            // Begin Time
            //
            m_Time.Start();

            //
            // Get started
            //
            XCORE_PERF_FRAME_MARK()
            auto& Scheduler = xcore::get().m_Scheduler;
            Scheduler.AddJobToQuantumWorld(*this);
            Scheduler.MainThreadStartsWorking();
        }

        void Resume( void )
        {
            //
            // Update Time
            //
            m_Time.UpdateDeltaTimeRoundTrip();

            //
            // Get started
            //
            XCORE_PERF_FRAME_MARK()
            auto& Scheduler = xcore::get().m_Scheduler;
            Scheduler.AddJobToQuantumWorld(*this);
            Scheduler.MainThreadStartsWorking();
        }

        void Stop( void )
        {
        }

        template< typename T_INIT_CALLBACK > 
        xforceinline void createEntity( system::instance& System, component::group::instance& Group, T_INIT_CALLBACK&& InitFunction, entity::guid EntityGuid = mecs::entity::guid{xcore::not_null} ) noexcept
        {
            entity::tmp_ref Ref{ nullptr, component::group::local_index{0u}, component::group::page_index{0u} };;
            m_mapEntity.alloc( EntityGuid, [&]( entity::map_t::entry& E )
            {
                Ref = E.m_Value = Group.AllocEntity(&E);

                // Let the user do whatever he needs to do
                system::details::CallFunctionWithComponents( Ref, InitFunction );
            });

            // Check to make sure there is not a guid collision
            xassert( Ref.m_pGroupInstance );
            if( !Ref.m_pGroupInstance ) XCORE_BREAK;

            // Notify anyone it needs to know that there is a new entity
            // We put this ourside the alloc because if a user is creating another entity and it happens to hit the same entry in the hash table
            // since it is already lock it could end up in a deadlock. The way to solve this is by having a reentry lock there.
            // But by putting it out here here may create a race condition.
            if( Group.m_Events.m_CreatedEntity.hasSubscribers() )
                Group.m_Events.m_CreatedEntity.NotifyAll( Ref.getComponent<entity::instance>(), System );
        }

        template< typename T_GET_CALLBACK, typename T_INIT_CALLBACK > 
        xforceinline void getOrCreateEntity( system::instance& System, component::group::instance& Group, T_GET_CALLBACK& GetCallback, T_INIT_CALLBACK&& InitFunction, entity::guid EntityGuid = mecs::entity::guid{xcore::not_null} ) noexcept
        {
            entity::tmp_ref Ref{ nullptr, component::group::local_index{0u}, component::group::page_index{0u} };
            if( false == m_mapEntity.getOrCreate( EntityGuid
                // Get
                , [&]( entity::map_t::entry& E ) constexpr noexcept
                {
                    system::details::CallFunctionWithComponents( E.m_Value, GetCallback );
                }
                // Create
                , [&]( entity::map_t::entry& E ) noexcept
                {
                    Ref = E.m_Value = Group.AllocEntity(&E);

                    // Let the user do whatever he needs to do
                    system::details::CallFunctionWithComponents( E.m_Value, InitFunction );
                }))
            {
                // Notify anyone it needs to know that there is a new entity
                // We put this out side the alloc because if a user is creating another entity and it happens to hit the same entry in the hash table
                // since it is already lock it could end up in a deadlock. The way to solve this is by having a reentry lock there.
                // But by putting it out here here may create a race condition.
                if( Group.m_Events.m_CreatedEntity.hasSubscribers() )
                    Group.m_Events.m_CreatedEntity.NotifyAll( Ref.getComponent<entity::instance>(), System );
            }
        }

        template< typename T_GET_CALLBACK, typename T_INIT_CALLBACK > 
        xforceinline void getOrCreateEntityRelax( system::instance& System, component::group::instance& Group, T_GET_CALLBACK& GetCallback, T_INIT_CALLBACK&& InitFunction, entity::guid EntityGuid = mecs::entity::guid{xcore::not_null} ) noexcept
        {
            entity::tmp_ref Ref{ nullptr, component::group::local_index{0u}, component::group::page_index{0u} };
            if( m_mapEntity.getOrCreate( EntityGuid
                // Get
                , [&]( entity::map_t::entry& E ) constexpr noexcept
                {
                    Ref = E.m_Value;
                }
                // Create
                , [&]( entity::map_t::entry& E ) constexpr noexcept
                {
                    Ref = E.m_Value = Group.AllocEntity(&E);

                    // Let the user do whatever he needs to do
                    system::details::CallFunctionWithComponents( Ref, InitFunction );
                }))
            {
                // Unprotected callback function
                system::details::CallFunctionWithComponents( Ref, GetCallback );
            }
            else
            {
                // Notify anyone it needs to know that there is a new entity
                // We put this out side the alloc because if a user is creating another entity and it happens to hit the same entry in the hash table
                // since it is already lock it could end up in a deadlock. The way to solve this is by having a reentry lock there.
                // But by putting it out here here may create a race condition.
                if( Group.m_Events.m_CreatedEntity.hasSubscribers() )
                    Group.m_Events.m_CreatedEntity.NotifyAll( Ref.getComponent<entity::instance>(), System );
            }
        }

        template< typename T_INIT_CALLBACK> 
        xforceinline void createEntity( component::group::instance& Group, T_INIT_CALLBACK&& InitFunction, entity::guid EntityGuid = mecs::entity::guid{xcore::not_null} ) noexcept
        {
            createEntity( *m_pDefaultSystem, Group, std::forward<T_INIT_CALLBACK>(InitFunction), EntityGuid );
        }

        xforceinline void createEntity( component::group::instance& Group ) noexcept
        {
            createEntity( *m_pDefaultSystem, Group, []{} );
        }

        component::group::instance& getOrCreateGroup( const std::span<const type_guid>    lComponentTypesGuids
                                                    , const std::span<const type_guid>    lTagTypesGuids ) noexcept
        {
            bool                bHasEntity          = false;
            const auto          ComponentGroupGuid  = component::group::guid{ [&] 
            {
                std::uint64_t Key = 0; 
                for (auto E : lComponentTypesGuids) 
                {
                    xassert( E != entity::instance::type_guid_v );
                    Key += E.m_Value;
                }
                return Key + entity::instance::type_guid_v.m_Value; 
                }() };
            const auto          TagGuid             = type_guid{ [&] {std::uint64_t Key = 0; for (auto E : lTagTypesGuids) Key += E.m_Value; return Key + entity::instance::type_guid_v.m_Value; }() };
            const auto          TagGroupGuid        = component::group::guid{ ComponentGroupGuid.m_Value + TagGuid.m_Value };

            component::group::instance* pEntry;
            m_mapGroupPerTag.getOrCreate( TagGroupGuid
                , [&]( auto& E ) constexpr noexcept
                {
                    pEntry = E.m_Value.get();
                }
                , [&]( auto& E ) constexpr noexcept
                {
                    auto& TagEntry = [&]() noexcept ->auto&
                    {
                        details::dbase::tag_entry* pTagEntry;
                        m_mapTagLookup.getOrCreate( TagGuid
                        ,[&]( auto& Node ) constexpr noexcept
                        {
                            pTagEntry = Node.m_Value;
                        }
                        ,[&]( auto& Node ) constexpr noexcept
                        {
                            auto& Entry = m_TagDBase.m_lTagEntries[ m_TagDBase.m_nTagEntries++ ];
                            Entry.m_Guid      = TagGuid;
                            Entry.m_nGroups   = 0;
                            Entry.m_nTags     = 0;
                                                         for( auto E : lTagTypesGuids )
                            {
                               Entry.m_TagBits.AddBit( m_mapTagDescriptor.get(E) );
                                Entry.m_lTagGuids[ Entry.m_nTags++ ] = E;
                            }

                            pTagEntry = Node.m_Value = &Entry;
                        });
                        return *pTagEntry;
                    }();

                    //
                    // Create a new group if we need to
                    //
                    //                             XCORE_PERF_ZONE_SCOPED_N("getOrCreateGroup::Wasting Time")
                    auto&               GroupEntry          = *new( &TagEntry.m_lGroups[TagEntry.m_nGroups++] ) details::dbase::tag_entry::group_entry {};
                    auto                nComponents         = lComponentTypesGuids.size();
                        
                    xcore::array<const component::descriptor*, settings::max_components_in_entity_v> lComponetDesc;
                    for( int i=0; i<nComponents; ++i )
                    {
                        auto& C = lComponentTypesGuids[i];
                        xassert(C.m_Value);

                        // Collect all the component descriptors
                        lComponetDesc[i] = m_mapComponentDescriptor.get(C);

                        // Make sure we set all the bits for the query
                        GroupEntry.m_GroupBits.AddBit(lComponetDesc[i]->m_BitNumber);
                    }

                    // the entity if we have to
                    if(bHasEntity == false)
                    {
                        lComponetDesc[nComponents] = m_mapComponentDescriptor.get(entity::instance::type_guid_v);
                        GroupEntry.m_GroupBits.AddBit(lComponetDesc[nComponents]->m_BitNumber);
                        nComponents++;
                    }

                    // Make sure that we store data in the ascending order
                    std::sort(  lComponetDesc.begin()
                            ,   lComponetDesc.begin() + nComponents
                            ,   [](const component::descriptor* pA, const component::descriptor* pB) { return pA->m_Guid.m_Value < pB->m_Guid.m_Value;} 
                    );

                    auto pGroupDesc = [&]() noexcept
                    {
                        const component::group::descriptor* pEntry;
                        m_mapGroupDescriptor.getOrCreate
                        ( 
                            ComponentGroupGuid
                            // Get
                            , [&]( auto& Node ) constexpr noexcept
                            {
                                pEntry = Node.m_Value;
                            }
                            // Create
                            , [&]( auto& Node ) constexpr noexcept
                            {
                                pEntry = Node.m_Value = new component::group::descriptor
                                {
                                    lComponetDesc.ViewTo(nComponents)
                                };
                            }
                        );
                        return pEntry;
                    }();

                    GroupEntry.m_pGroup = new component::group::instance
                    { 
                            TagGroupGuid
                        , m_PageMgr
                        , *pGroupDesc
                    };

                    GroupEntry.m_pGroup->m_pDataBaseRef = &TagEntry;

                    //
                    // Attach Delegates
                    //
                    for( auto& pDelegate : m_lDelegates )
                    {
                        auto& Instance = pDelegate->m_GroupQuery;

                        if( false == TagEntry.m_TagBits.Query  ( Instance.m_TagQuery.m_All, Instance.m_TagQuery.m_Any, Instance.m_TagQuery.m_None )                   ) continue;
                        if( false == GroupEntry.m_GroupBits.Query( Instance.m_ComponentQuery.m_All, Instance.m_ComponentQuery.m_Any, Instance.m_ComponentQuery.m_None ) ) continue;
                        
                        pDelegate->AttachToGroup( *this, *GroupEntry.m_pGroup );
                    }

                    E.m_Value = std::unique_ptr<component::group::instance>{ GroupEntry.m_pGroup };
                    pEntry = E.m_Value.get();
                });

            return *pEntry;
        }

        template< typename...T_COMPONENTS_AND_TAGS >
        xforceinline component::group::instance& getOrCreateGroup( void ) noexcept
        {
            constexpr static component::group::details::guid_arrays_from_types_with_duplication<T_COMPONENTS_AND_TAGS...> arrays_v;
            return getOrCreateGroup( arrays_v.component_guid_list_v, arrays_v.tag_guid_list_v );
        }

        /*
        template< typename T_COMPONENTS_AND_TAGS, typename X >
        xcore::string::const_universal
        getComponentName(){ return xconst_universal_str("xx"); }

        template< typename T_COMPONENTS_AND_TAGS >
        std::enable_if_t< xcore::types::is_defined_v< decltype( T_COMPONENTS_AND_TAGS::name_v) >, xcore::string::const_universal>
        getComponentName<T_COMPONENTS_AND_TAGS,int>(){ return T_COMPONENTS_AND_TAGS::name_v; }
        */
        template< typename T_COMPONENTS_AND_TAGS, typename X >
        struct get_name
        {
            static constexpr auto value = xconst_universal_str("DefaultName");
        };

        template< typename T_COMPONENTS_AND_TAGS >
        struct get_name<T_COMPONENTS_AND_TAGS,decltype(T_COMPONENTS_AND_TAGS::name_v)>
        {
            static constexpr auto value = T_COMPONENTS_AND_TAGS::name_v;
        };

        template< typename T_COMPONENTS_AND_TAGS >
        inline void registerComponent(void) noexcept
        {
            xassert( m_isRegistrationFinish == false );

            if constexpr ( std::is_base_of_v<mecs::component::tag, T_COMPONENTS_AND_TAGS> )
            {
                static_assert( sizeof(T_COMPONENTS_AND_TAGS)==1, "Tags should not contain any data" );
                m_mapTagDescriptor.alloc( T_COMPONENTS_AND_TAGS::type_guid_v, [&]( auto& E )
                {
                    E.m_Value = m_TagRegistrationFreeBits.AllocBit();
                });
            }
            else
            {
                constexpr static component::descriptor component_descriptor_v
                {
                        T_COMPONENTS_AND_TAGS::type_guid_v
                    ,   [](void* p) noexcept { new(p) T_COMPONENTS_AND_TAGS{}; }
                    ,   [](void* p) noexcept { std::destroy_at( reinterpret_cast<T_COMPONENTS_AND_TAGS*>(p) ); }
                        // //property::table&  m_PropertyTable;
                    ,   static_cast<std::uint16_t>(sizeof(T_COMPONENTS_AND_TAGS))
                    ,   static_cast<std::uint16_t>(std::alignment_of_v<T_COMPONENTS_AND_TAGS>)
                    ,      std::is_base_of_v<mecs::component::double_buffer, T_COMPONENTS_AND_TAGS>
                        || std::is_base_of_v<mecs::component::quantum_double_buffer, T_COMPONENTS_AND_TAGS>
                    ,   -1
                    ,   get_name<T_COMPONENTS_AND_TAGS,const xcore::string::const_universal>::value
                    ,   component::type_v<T_COMPONENTS_AND_TAGS>
                };

                component_descriptor_v.m_BitNumber = m_ComponentRegistrationFreeBits.AllocBit();
                m_mapComponentDescriptor.alloc(T_COMPONENTS_AND_TAGS::type_guid_v, [&]( auto& E )
                {
                    E.m_Value = &component_descriptor_v;
                });
            }
        }
 
        template< typename...T_COMPONENTS_AND_TAGS >
        inline void registerComponents(void) noexcept
        {
            (registerComponent<T_COMPONENTS_AND_TAGS>(), ... );
        }

        template< typename T_SYSTEM, typename...T_ARGS >
        T_SYSTEM& registerGraphConnection(sync_point::instance& StartSynpoint, T_ARGS&&... EndSyncPoints) noexcept
        {
            xassert( m_isRegistrationFinish == false );

            static_assert(sizeof...(EndSyncPoints) > 0, "You must pass at least one end Sync_point");
            static_assert(std::is_base_of_v<sync_point::instance, std::common_type_t<T_ARGS...>>, "All the end sync_points must be non const sync_points");
            
            using custom_system_t  = system::details::custom_system<T_SYSTEM>;
            auto pSystem = new custom_system_t{*this};

            // Add all end triggers
            ( EndSyncPoints.JobWillNotifyMe(*pSystem), ... );
            ( EndSyncPoints.FreezCount(), ... );

            // Set the start trigger
            StartSynpoint.AddJobToBeTrigger(*pSystem);
            StartSynpoint.FreezCount();

            m_lSystems.push_back( std::unique_ptr<system::instance>{pSystem});

            //
            // Register to SyncPoint messages as needed
            //

            // This message is a must have since we need to deal with the memory barrier
            pSystem->m_SyncPointDoneDelegate.Connect( xcore::types::PickFirstArgument( std::forward<T_ARGS>(EndSyncPoints) ... ).m_Events.m_Done );

            if constexpr ( &custom_system_t::msgSyncPointStart != &system::instance::msgSyncPointStart )
            {
                pSystem->m_WorldStartDelegate.setupThis( *pSystem );
                pSystem->m_SyncPointStartDelegate.Connect( StartSynpoint.m_Events.m_Start );
            }

            //
            // Register to World Instances messages
            //
            if constexpr ( &custom_system_t::msgWorldStart != &system::instance::msgWorldStart )
            {
                pSystem->m_WorldStartDelegate.setupThis( *pSystem );
                pSystem->m_WorldStartDelegate.Connect( m_Events.m_WorldStart );
            }

            if constexpr ( &custom_system_t::msgFrameStart != &system::instance::msgFrameStart )
            {
                pSystem->m_FrameStartDelegate.setupThis( *pSystem );
                pSystem->m_FrameStartDelegate.Connect( m_Events.m_FrameStart );
            }

            if constexpr ( &custom_system_t::msgFrameDone != &system::instance::msgFrameDone )
            {
                pSystem->m_FrameDoneDelegate.setupThis( *pSystem );
                pSystem->m_FrameDoneDelegate.Connect( m_Events.m_FrameDone );
            }

            //
            // Register all the events
            //

            // Register all the events    
            details::register_events<custom_system_t::events_t>::Register(*this, *pSystem);


            return *pSystem;
        }

        template< typename T_CALLBACK, typename K = world::instance >
        xforceinline bool getEntity( entity::guid gEntity, T_CALLBACK&& Callback ) const noexcept
        {
            return m_mapEntity.find( gEntity, [&](entity::map_t::entry& Entry ) constexpr noexcept
            {
                system::details::CallFunctionWithComponents( Entry.m_Value, std::forward<T_CALLBACK>(Callback) );
            });
        }

        template< typename T_DELEGATE >
        T_DELEGATE& registerDelegate( void ) noexcept
        {
            xassert( m_isRegistrationFinish == false );

            using custom_delegate_t = typename delegate::details::template custom_instance<T_DELEGATE>;
            auto pDelegate = new custom_delegate_t;
        
            if constexpr ( std::is_same_v< typename custom_delegate_t::type_t, event::details::group_event > )
            {
                m_mapDelegates.alloc(custom_delegate_t::guid_v, [&]( map_delegates_t::entry& E )
                {
                    E.m_Value = pDelegate;
                    m_lDelegates.push_back( std::unique_ptr<delegate::details::group_base>{pDelegate} );
                });
            }
            else
            {
                m_lInstanceBaseDelegates.push_back( std::unique_ptr<delegate::details::instance_base>{pDelegate} );
            }

            //
            // Register to World Instances messages
            //
            if constexpr (&custom_delegate_t::msgWorldStart != &custom_delegate_t::base_t::msgWorldStart)
            {
                pDelegate->m_WorldStartDelegate.setupThis(*pDelegate);
                pDelegate->m_WorldStartDelegate.Connect(m_Events.m_WorldStart);
            }

            if constexpr (&custom_delegate_t::msgFrameStart != &custom_delegate_t::base_t::msgFrameStart)
            {
                pDelegate->m_FrameStartDelegate.setupThis(*pDelegate);
                pDelegate->m_FrameStartDelegate.Connect(m_Events.m_FrameStart);
            }

            if constexpr (&custom_delegate_t::msgFrameDone != &custom_delegate_t::base_t::msgFrameDone)
            {
                pDelegate->m_FrameEndDelegate.setupThis(*pDelegate);
                pDelegate->m_FrameEndDelegate.Connect(m_Events.m_FrameDone);
            }

            return *pDelegate;
        }

        template< typename...T_DELEGATES >
        void registerDelegates( void ) noexcept
        {
            ( registerDelegate<T_DELEGATES>(), ... );
        }

        void registerFinish( void ) noexcept
        {
            xassert( m_isRegistrationFinish == false );

            //
            // Initialize all Group Delegates
            //
            for( auto& Delegate : m_lDelegates )
            {
                Delegate->InitializeGroupQuery( *this );
            }

            for( auto& Delegate : m_lInstanceBaseDelegates )
            {
                Delegate->AttachToEvent( *this );
            }

            //
            // Done with registration
            //
            m_isRegistrationFinish = true;
        }

        template< typename T_SYSTEM_FUNCTION >
        void InitializeGroupQuery( query::instance& Instance, const query::define_data& Defined ) const noexcept
        {
            if( Instance.m_isInitialized ) return;

            using function  = T_SYSTEM_FUNCTION;

            Instance.m_isInitialized = true;

            //
            // Handle the Component Side
            //
            for( auto E : Defined.m_ComponentQuery.m_All )
                Instance.m_ComponentQuery.m_All.AddBit( m_mapComponentDescriptor.get(E)->m_BitNumber );

            for( auto E : Defined.m_ComponentQuery.m_Any )
                Instance.m_ComponentQuery.m_Any.AddBit( m_mapComponentDescriptor.get(E)->m_BitNumber );

            for( auto E : Defined.m_ComponentQuery.m_None )
                Instance.m_ComponentQuery.m_None.AddBit( m_mapComponentDescriptor.get(E)->m_BitNumber );

            //
            // Add the contribution from the function parameters
            //
            for( int i=0; i<function::all_v.size(); ++i )
            {
                // DEBUG WARNING: Make sure all components are register!!!
                const auto BitNum = m_mapComponentDescriptor.get(function::all_v[i])->m_BitNumber;
                Instance.m_ComponentQuery.m_All.AddBit( BitNum );
                if( function::access_mode_list_v[i] != component::access_mode::READ_ONLY ) Instance.m_Write.AddBit(BitNum);
            }

            for( int i=0; i<function::any_v.size(); ++i )
            {
                // DEBUG WARNING: Make sure all components are register!!!
                const auto BitNum = m_mapComponentDescriptor.get(function::any_v[i])->m_BitNumber;
                Instance.m_ComponentQuery.m_Any.AddBit( BitNum );
                if( function::access_mode_list_v[i] != component::access_mode::READ_ONLY ) Instance.m_Write.AddBit(BitNum);
            }

/*
            for( int i=0; i<function::list_v.size(); ++i )
            {
                // DEBUG WARNING: Make sure all components are register!!!
                const auto BitNum = m_mapComponentDescriptor.get(function::list_v[i])->m_BitNumber;
                Instance.m_Function.AddBit( BitNum );
                if( function::access_mode_list_v[i] != component::access_mode::READ_ONLY ) Instance.m_Write.AddBit(BitNum);
            }

            // Function argument are a must have
            Instance.m_ComponentQuery.m_All.Add(Instance.m_Function);
*/

            //
            // Handle the tag side
            //
            for( auto E : Defined.m_TagQuery.m_All )
                Instance.m_TagQuery.m_All.AddBit( m_mapTagDescriptor.get(E) );

            for( auto E : Defined.m_TagQuery.m_Any )
                Instance.m_TagQuery.m_Any.AddBit( m_mapTagDescriptor.get(E) );

            for( auto E : Defined.m_TagQuery.m_None )
                Instance.m_TagQuery.m_None.AddBit( m_mapTagDescriptor.get(E) );
        }

        template<typename T >
        void DoQuery( query::instance& Instance, const query::define_data& Defined ) const noexcept
        {
            XCORE_PERF_ZONE_SCOPED()
            using function  = query::system_function<T>;

            if( Instance.m_isInitialized == false ) InitializeGroupQuery<function>(Instance, Defined);

            //
            // Do query
            //
            Instance.m_lResults.clear();
            for( const auto& TagE : m_TagDBase.m_lTagEntries.ViewTo(m_TagDBase.m_nTagEntries) )
            {
                if( false == TagE.m_TagBits.Query( Instance.m_TagQuery.m_All, Instance.m_TagQuery.m_Any, Instance.m_TagQuery.m_None ) ) continue;

                for( auto& GroupE : TagE.m_lGroups.ViewTo(TagE.m_nGroups) )
                {
                    if( false == GroupE.m_GroupBits.Query( Instance.m_ComponentQuery.m_All, Instance.m_ComponentQuery.m_Any, Instance.m_ComponentQuery.m_None ) ) continue;
                    if( GroupE.m_pGroup->m_Count == 0 ) continue;

                    //
                    // Create new entry for this query
                    //
                    auto&           Entry                   = Instance.m_lResults.append();
                    Entry.m_pGroup                          = const_cast<component::group::instance*>( GroupE.m_pGroup );

                    const auto&     Group                   = *GroupE.m_pGroup;
                    const auto&     lOffsets                = Group.m_GroupDescriptor.m_lComponentOffsets;
                    const auto&     lCTypeGuid              = Group.m_GroupDescriptor.m_lComponentTypeGuids;
                    const int       Size                    = Group.m_GroupDescriptor.m_nComponents;
                    int             F                       = 0;
                    std::uint64_t   DoubleBufferDirtyBits   = 0;

                    for( int i=0; i<Size; ++i)
                    {
                        const auto SortedGuid = function::sorted_list_v[F];
                        if( lCTypeGuid[i] == SortedGuid )
                        {
                            // Are we a double buffer component?
                            if( lOffsets[ i ].m_HasT1 )
                            {
                                // First find the base for this double buffer component
                                int iBase = (i > 0)? (lCTypeGuid[i-1] == SortedGuid? (i-1) : i) : 0;

                                // Are we looking for T0?
                                if( function::access_mode_sorted_list_v[F] == component::access_mode::READ_ONLY )
                                {
                                    iBase += static_cast<int>((Group.m_DoubleBufferInfo.m_StateBits>>iBase)&1);
                                }
                                else
                                {
                                    // Mark the Double Buffer State as Dirty since we are going to update it
                                    // Note that we always change the base component index for the double buffer.
                                    DoubleBufferDirtyBits |= (1ull<<iBase);

                                    // Now ge the new iBase
                                    iBase += static_cast<int>( 1 - ((Group.m_DoubleBufferInfo.m_StateBits>>iBase)&1) );

                                }

                                // Okay ready to set the offsets now
                                Entry.m_lRemaps    [F + function::remap_v[F]] = lOffsets[ iBase ].m_Offset;
                                Entry.m_lLockRemaps[F + function::remap_v[F]] = iBase;
                            }
                            else
                            {
                                Entry.m_lRemaps    [F + function::remap_v[F]] = lOffsets[ i ].m_Offset;
                                Entry.m_lLockRemaps[F + function::remap_v[F]] = i;
                            }

                            // Move to next component
                            F++;
                            if(F==function::list_v.size()) break;
                        }
                        else if( lCTypeGuid[i] > SortedGuid )
                        {
                            // We did not found the previous guid... it must have been an "any" type.
                            // Lets mark it as so...
                            Entry.m_lRemaps    [F + function::remap_v[F]] = -1;
                            Entry.m_lLockRemaps[F + function::remap_v[F]] = -1;

                            F++;
                            if(F==function::list_v.size()) break;

                            // Lets try this again
                            i--;
                        }
                    }

                    if( (F+1) == function::list_v.size() )
                    {
                        // We did not found the previous guid... it must have been an "any" type.
                        // Lets mark it as so...
                        Entry.m_lRemaps    [F + function::remap_v[F]] = -1;
                        Entry.m_lLockRemaps[F + function::remap_v[F]] = -1;

                        F++;
                    }

                    xassert(auto n = function::list_v.size(); F == n );
                    Entry.m_nRemaps    = static_cast<std::uint8_t>(function::list_v.size()); 
                    Entry.m_AccessMode = function::access_mode_list_v;
                    if( DoubleBufferDirtyBits )
                    {
                        for( auto L = Entry.m_pGroup->m_DoubleBufferInfo.m_DirtyBits.load(std::memory_order_relaxed);
                             false == Entry.m_pGroup->m_DoubleBufferInfo.m_DirtyBits.compare_exchange_weak( L, L | DoubleBufferDirtyBits );
                            );
                    }
                }
            }
        }

        void deleteEntity( system::instance& System, entity::instance& Entity ) noexcept
        {
            XCORE_PERF_ZONE_SCOPED()
            if( Entity.isZombie() ) return;

            auto& Node   = *Entity.m_pInstance;
            auto  TmpRef =  Node.m_Value;

            // Mark for deletion in the group
            TmpRef.m_pGroupInstance->DeleteEntity( Entity );

            //
            // Notify that we are deleting an entity
            //
            auto& Group = *TmpRef.m_pGroupInstance;
            if( Group.m_Events.m_DeletedEntity.hasSubscribers() )
                Group.m_Events.m_DeletedEntity.NotifyAll( TmpRef.getComponent<entity::instance>(), System );

            //
            // Delete Entry now!
            //
            {
                m_mapEntity.free( Node.m_Key,[](auto&){} );

                /*
                m_DeleteList.append(&Node);
                // Mark it as an invalid entry
                Node.m_Value.clear();
                */

                
                /*
                auto  pHead = m_EntityFreeList.load(std::memory_order_relaxed);
                auto& pNext = reinterpret_cast<entity::map_t::entry*&>( Node.m_Value.m_pGroupInstance );

                Node.m_Value.

                pNext = pHead;
                while( false == m_EntityFreeList.compare_exchange_weak( pNext, &Node) );
                */

                //
                // Mark entity as officially deleted
                //

                // Make sure we mark the tmp_ref as invalid
                // Node.m_Value.clear();

            }
        }

        template< typename...T_ADD_COMPONENTS_OR_TAGS, typename...T_REMOVE_COMPONENTS_OR_TAGS >
        component::group::instance& getOrCreateGroupBy( 
                component::group::instance&                 Group
            ,   std::tuple<T_ADD_COMPONENTS_OR_TAGS...>*
            ,   std::tuple<T_REMOVE_COMPONENTS_OR_TAGS...>*
            ) noexcept
        {
            // XCORE_PERF_ZONE_SCOPED()
            constexpr static auto ModifyComponentList = []( 
                    std::span<type_guid>        OutputList
                ,   std::span<const type_guid>  CurrentList
                ,   std::span<const type_guid>  AddList
                ,   std::span<const type_guid>  RemoveList ) constexpr noexcept -> int
            {
                int nComponents = 0;
                for( int i=0, a=0 ;;)
                {
                    if( a == AddList.size() )
                    {
                        while( i < CurrentList.size() )
                            OutputList[nComponents++] = CurrentList[i++];
                        break;
                    }
                    else if( i == CurrentList.size() )
                    {
                        while( a < AddList.size() )
                            OutputList[nComponents++] = AddList[a++];
                        break;
                    }

                    if( CurrentList[i] > AddList[a] )
                    {
                        OutputList[nComponents++] = AddList[a++];
                    }
                    else
                    {
                        if( CurrentList[i] == AddList[a] )
                        {
                            //TODO: WARNING User is trying to add a duplicate component. We should issue a warning
                            i++;
                        }
                        else
                        {
                            OutputList[nComponents++] = CurrentList[i++];
                        }
                    }
                }

                // Delete components
                if( RemoveList.size() )
                {
                    int n=0;
                    for( int i=0, r=0;;  )
                    {
                        if( r == RemoveList.size() )
                        {
                            while( i<nComponents )
                                OutputList[n++] = OutputList[i++];
                            break;
                        }
                        else if( i == (nComponents - r) ) break;

                        if( CurrentList[i] > RemoveList[r] ) 
                        { 
                            ++r;
                        }
                        else 
                        {
                            if( CurrentList[i] < RemoveList[r] ) OutputList[n++] = OutputList[i];
                            ++i;
                        }
                    }
                    nComponents = n;
                }

                return nComponents;
            };

            constexpr static component::group::details::guid_arrays_from_types_with_duplication<T_ADD_COMPONENTS_OR_TAGS...>    add_v;
            constexpr static component::group::details::guid_arrays_from_types_with_duplication<T_REMOVE_COMPONENTS_OR_TAGS...> remove_v;

            auto&   GroupDescriptor          = Group.m_GroupDescriptor;
            auto&   CurrentTagEntry          = *reinterpret_cast<details::dbase::tag_entry*>(Group.m_pDataBaseRef);

            std::array<type_guid, settings::max_components_in_entity_v >    Components;
            std::array<type_guid, settings::max_tags_in_entity_v >          Tags;

            const int nComponents = ModifyComponentList(    Components
                                                        ,   std::span{ std::as_const(GroupDescriptor.m_lComponentTypeGuids).data(), GroupDescriptor.m_nComponents }
                                                        ,   add_v.component_guid_list_v
                                                        ,   remove_v.component_guid_list_v );

            const int nTags       = ModifyComponentList(    Tags
                                                        ,   std::span{ std::as_const(CurrentTagEntry.m_lTagGuids).data(), CurrentTagEntry.m_nTags }
                                                        ,   add_v.tag_guid_list_v
                                                        ,   remove_v.tag_guid_list_v );

            // Add components
            auto& NewGroup = getOrCreateGroup( std::span<const type_guid>{Components.data()+1, static_cast<std::size_t>(nComponents-1)}
                                                , std::span<const type_guid>{Tags.data(), static_cast<std::size_t>(nTags) } );

            if( &NewGroup == &Group )
            {
                // TODO: WARNING, This move resulted in no operation!
            }

            return NewGroup;
        }

        template< typename T_TUPLE_ADD_COMPONENTS_OR_TAGS, typename T_TUPLE_REMOVE_COMPONENTS_OR_TAGS >
        xforceinline component::group::instance& getOrCreateGroupBy( component::group::instance& Group ) noexcept
        {
            return getOrCreateGroupBy(Group, (T_TUPLE_ADD_COMPONENTS_OR_TAGS*)nullptr, (T_TUPLE_REMOVE_COMPONENTS_OR_TAGS*)nullptr );
        }

        void moveEntityToGroup( system::instance& System, component::group::instance& NewGroup, entity::instance& Entity, xcore::function::view<void(entity::tmp_ref)> Callback = [](entity::tmp_ref){} )
        {
            //XCORE_PERF_ZONE_SCOPED()
            // This entity has been mark for deletion.... there is nothing we can accomplish here 
            xassert( Entity.isZombie() == false );

            auto  OldTmpRef = Entity.m_pInstance->m_Value;
            auto& OldGroup  = *OldTmpRef.m_pGroupInstance;

            // If we are going to move it to the same group there is nothing to do
            if( &NewGroup == &OldGroup ) return;

            // Let anyone that is interested know
            if( OldGroup.m_Events.m_MovedOutEntity.hasSubscribers() )
                OldGroup.m_Events.m_MovedOutEntity.NotifyAll( OldTmpRef.getComponent<entity::instance>(), System );

            auto& NewGDescriptor = NewGroup.m_GroupDescriptor;
            auto& OldGDescriptor = OldGroup.m_GroupDescriptor;

            // Allocate our new entity in the new group
            auto  NewTmpRef      = NewGroup.AllocEntity( Entity.m_pInstance );
                    
            //
            // Move/construct the components over 
            //
            NewGDescriptor.ConstructOnlyEntityComponent( NewGroup.m_lPages[NewTmpRef.m_PageIndex.m_Value], NewTmpRef.m_LocalIndex, Entity.m_pInstance );
            for( int inew=1, iold=1; inew < NewGDescriptor.m_nComponents; )
            {
                if( iold == OldGDescriptor.m_nComponents ) 
                {
                    do 
                    {
                        NewGDescriptor.ConstructOnlyComponent( NewGroup.m_lPages[NewTmpRef.m_PageIndex.m_Value], NewTmpRef.m_LocalIndex, inew );
                        inew++;
                    } while (inew < NewGDescriptor.m_nComponents);
                    break;
                }

                if( NewGDescriptor.m_lComponentTypeGuids[inew] < OldGDescriptor.m_lComponentTypeGuids[iold] )
                {
                    // New group has a component that the old group did not so we need to construct this entry
                    NewGDescriptor.ConstructOnlyComponent( NewGroup.m_lPages[NewTmpRef.m_PageIndex.m_Value], NewTmpRef.m_LocalIndex, inew );
                    inew++;
                }
                else if( NewGDescriptor.m_lComponentTypeGuids[inew] > OldGDescriptor.m_lComponentTypeGuids[iold] )
                {
                    // We are missing this component so skip it
                    iold++;
                }
                else
                {
                    const auto& NewOffsets = NewGDescriptor.m_lComponentOffsets[inew];
                    const auto& OldOffsets = OldGDescriptor.m_lComponentOffsets[iold];
                    void*       pSrc       = &OldGroup.m_lPages[OldTmpRef.m_PageIndex.m_Value]->m_RawData[OldOffsets.m_Offset + iold * OldOffsets.m_Size ];
                    void*       pDst       = &NewGroup.m_lPages[NewTmpRef.m_PageIndex.m_Value]->m_RawData[NewOffsets.m_Offset + inew * NewOffsets.m_Size ];

                    // Move this component to new location
                    std::memcpy( pDst, pSrc, NewGDescriptor.m_lComponentDescriptors[inew]->m_Size );

                    // Get ready for next component
                    inew++;
                    iold++;
                }
            }

            //
            // Lets tell the system to delete the old component
            //
            OldGroup.DeleteEntity( Entity );

            //
            // Release the new entity. We must lock it before making any changes
            //
            {
                auto& Instance = Entity.getPointer();
                xcore::lock::scope Lock( Instance.m_pLock.load(std::memory_order_relaxed)->m_Lock );
                Instance.m_Value = NewTmpRef;
            }

            //
            // Let the user know that there is has been officially moved
            //
            Callback(NewTmpRef);

            if( NewGroup.m_Events.m_MovedInEntity.hasSubscribers() )
                NewGroup.m_Events.m_MovedInEntity.NotifyAll( NewTmpRef.getComponent<entity::instance>(), System );
        }

        ~instance()
        {
            XCORE_PERF_ZONE_SCOPED()
                
            xcore::scheduler::channel MyChannel(xconst_universal_str("World Destruction"));
            MyChannel.SubmitJob([&]{ m_mapEntity.clear(); });
            MyChannel.SubmitJob([&]{ m_mapGroupPerTag.clear(); });
            MyChannel.SubmitJob([&]{ m_mapTagLookup.clear(); });
            MyChannel.join();
            MyChannel.SubmitJob([&]{ m_mapGroupDescriptor.clear(); });
            MyChannel.SubmitJob([&]{ m_mapComponentDescriptor.clear(); });
            MyChannel.SubmitJob([&]{ m_mapTagDescriptor.clear(); });
            MyChannel.SubmitJob([&]{ m_PageMgr.clear(); });
            MyChannel.join();
        }

        using map_group_per_tag_t           = tools::map_fixed<component::group::guid,  std::unique_ptr<component::group::instance>,    settings::max_groups_count_v>;
        using map_group_descriptor_t        = tools::map_fixed<component::group::guid,  const component::group::descriptor*,            settings::max_groups_count_v>;
        using map_component_descriptor_t    = tools::map_fixed<type_guid,               const component::descriptor*,                   256>;
        using map_tag_descriptor_t          = tools::map_fixed<type_guid,               std::uint8_t,                                   256>;
        using map_tag_lookup_t              = tools::map_fixed<type_guid,               details::dbase::tag_entry*,                     settings::max_groups_count_v>;
        using map_delegates_t               = tools::map_fixed<delegate::guid,          delegate::details::base*,                       settings::max_delegates_count_v>;
        using map_event_t                   = tools::map_fixed<event::guid,             void*,                                          1024 >;

        xcore::lock::object<std::vector<mecs::lock_error>, xcore::lock::spin> m_LockErrors {};
        bool                                        m_bFrameStarted                 { false };
        bool                                        m_bPause                        { false };
        tools::page_mgr                             m_PageMgr                       {};
        //tools::pool< entity::map_t::entry*, 1000 >  m_DeleteList                    {m_PageMgr};
        entity::map_t                               m_mapEntity                     {};
        details::dbase                              m_TagDBase                      {};
        map_group_per_tag_t                         m_mapGroupPerTag                {};
        map_group_descriptor_t                      m_mapGroupDescriptor            {};
        map_component_descriptor_t                  m_mapComponentDescriptor        {};
        map_tag_descriptor_t                        m_mapTagDescriptor              {};
        map_tag_lookup_t                            m_mapTagLookup                  {};
        map_delegates_t                             m_mapDelegates                  {};
        map_event_t                                 m_mapEvents                     {};
        std::vector<std::unique_ptr<system::instance>>                  m_lSystems                      {};
        std::vector<std::unique_ptr<delegate::details::group_base>>     m_lDelegates                {};
        std::vector<std::unique_ptr<delegate::details::instance_base>>  m_lInstanceBaseDelegates    {};
        query::filter_bits                          m_ComponentRegistrationFreeBits {xcore::not_null};
        query::filter_bits                          m_TagRegistrationFreeBits       {xcore::not_null};
//        std::atomic<entity::map_t::entry*>          m_EntityFreeList                {nullptr};
        std::uint64_t                               m_FrameNumber                   {0};
        time                                        m_Time                          {};
        events                                      m_Events                        {};
        details::default_system*                    m_pDefaultSystem                {nullptr};
        bool                                        m_isRegistrationFinish          { false };
    };
}
