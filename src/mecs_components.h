
namespace mecs::component
{
    namespace group
    {
        struct instance;
        struct local_index  : xcore::units::type<local_index,   std::uint16_t> {using type::type;};
        struct page_index   : xcore::units::type<page_index,    std::uint16_t> {using type::type;};
        struct global_index : xcore::units::type<global_index,  std::uint32_t> {using type::type;};
    }

    enum class access_mode : std::uint8_t
    {
          READ_ONLY
        , LINEAR_WRITE
        , QUANTUM_WRITE
    };

    //struct shared           {};     // Shared component. has a guid to another entity. 
    //struct shared_grouped   {};     // Shared component group. It creates a group for each unique value of 
    //struct subgroup         {};     // Creates a different pages for each value of this component value must be able to be extracted by "u64 getValue()"
    struct quantum_mutable          {}; // Means that the component can be read and write at the same time. Can not be combine with any other type.
    struct double_buffer            {}; // Means that it has the concept of time (T0,T1) Where T0 is read only and previous state, and T1 is write and next state. Can not be combined.
    struct quantum_double_buffer    {}; // Means that it has the concept of time (T0,T1) Where T0 is read only and previous state, and T1 is write and next state. Can not be combined.
    struct tag                      {}; // It means that it will force the Entity to be in a different group. can not be combine with any other type

    //struct value_subgroup_sorted                {}; // 
    struct value_subgroup                       {}; // It creates a new group for each unique value (these groups are call subgroups). The user must supply a 'u64 getValue() const'
    struct value_subgroup_sorted_execution      {}; // Same as above with the subgroups been sorted and execution is that by the sorting order without overlaps.

    enum class type : std::uint8_t
    {
            TAG
        ,   LINEAR
        ,   DOUBLE_BUFFER
        ,   QUANTUM
        ,   QUANTUM_DOUBLE_BUFFER
        ,   ENUM_COUNT
    };

    template<typename T>
    static constexpr type type_v = std::is_base_of_v<tag                  ,T>   ? type::TAG
                                 : std::is_base_of_v<quantum_mutable      ,T>   ? type::QUANTUM
                                 : std::is_base_of_v<quantum_double_buffer,T>   ? type::QUANTUM_DOUBLE_BUFFER
                                 : std::is_base_of_v<double_buffer        ,T>   ? type::DOUBLE_BUFFER
                                 : type::LINEAR;


    /*
    enum class type : std::uint8_t
    {
            TAG
        ,   LINEAR
        ,   DOUBLE_BUFFER
        ,   QUANTUM
        ,   SHARED
        ,   SHARED_GROUPED
        ,   VALUE_GROUPED
        ,   VALUE_GROUPED_SORTED
        ,   VALUE_GROUPED_SORTED_EXECUTION
        ,   ENUM_COUNT
    };

    struct settings
    {
        type_guid                       m_Guid { 0u, 0u };
        xcore::string::const_universal  m_Name { xconst_universal_str( "Unnamed" ) };
        type                            m_Type { type::LINEAR };

        template< typename...T_ARGS > xforceinline
        constexpr settings( T_ARGS&&...Args ) noexcept
        {
            xcore::types::tuple_visit( [&]( auto V ) constexpr noexcept
            {
                using t = std::decay_t<decltype(V)>;
                if constexpr ( std::is_same_v< t, type_guid > )
                {
                    m_Guid = V;
                }
                else if constexpr ( std::is_same_v< t, xcore::string::const_universal > )
                {
                    m_Name = V;
                }
                else if constexpr ( std::is_same_v< t, mecs::component::type > )
                {
                    m_Type = V;
                }
                else
                {
                    static_assert( xcore::types::always_false_v<t>, "Unknown type" );
                }
            }, xcore::types::lvalue( std::tuple(Args ...) ) );

            // Check to make sure we have all the requirements
            // IF THERE IS A COMPILER ERROR IS BECAUSE YOU DID NOT MEET THE REQUIREMENTS
            xassert( m_Guid.isValid() );
            xassert( m_Type >= type::TAG && m_Type < type::ENUM_COUNT );
        }
    };
    */

    struct descriptor
    {
        using fn_construct  = void(void*) noexcept;
        using fn_destruct   = void(void*) noexcept;

        const type_guid                         m_Guid;
        fn_construct* const                     m_fnConstruct;
        fn_construct* const                     m_fnDestruct;
        //property::table&                      m_PropertyTable;
        const std::uint16_t                     m_Size;
        const std::uint16_t                     m_Aligment;
        const bool                              m_isDoubleBuffer;
        mutable int                             m_BitNumber;
        const xcore::string::const_universal    m_Name;
        const type                              m_Type;
    };
}