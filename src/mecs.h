
#ifndef MECS_H
    #define MECS_H
    #pragma once

    //------------------------------------------------------------------------------------
    // M.E.C.S ( Multi-core, Events, Components, & Systems )
    //
    //
    //------------------------------------------------------------------------------------

    //------------------------------------------------------------------------------------
    // External dependencies
    //------------------------------------------------------------------------------------
    #include "xcore.h"

    //------------------------------------------------------------------------------------
    // Include the user settings
    //------------------------------------------------------------------------------------
    #include "mecs_user_settings.h"

    //------------------------------------------------------------------------------------
    // Predefines
    //------------------------------------------------------------------------------------
    namespace mecs
    {
        namespace world             { struct instance; }
        namespace entity            { struct instance; }
        namespace system            { struct instance; }
        namespace component
        {
            namespace group  { struct instance; }
        }

        using type_guid = xcore::guid::unit<64,struct component_type_guid_tag >;
        namespace system            { using guid = xcore::guid::unit<64,struct mecs_system_tag>;    }
        namespace sync_point        { using guid = xcore::guid::unit<64,struct mecs_syncpoint_tag>; }
        namespace component::group  { using guid = xcore::guid::unit<64,struct mecs_group_tag>;     }

        struct lock_error
        {
            std::array<system::guid,2>  m_lSystemGuids;
            component::group::guid      m_gGroup;
            const char*                 m_pMessage;
        };
    }

    //------------------------------------------------------------------------------------
    // Public Interface
    //------------------------------------------------------------------------------------
    #include "mecs_tools.h"
    #include "mecs_events.h"
    #include "mecs_components.h"
    #include "mecs_entity.h"
    #include "mecs_sync_point.h"
    #include "mecs_group.h"
    #include "mecs_query.h"
    #include "mecs_system.h"
    #include "mecs_delegate.h"
    #include "mecs_time.h"
    #include "mecs_world.h"

    //------------------------------------------------------------------------------------
    // Inlines
    //------------------------------------------------------------------------------------
    #include "Implementation/mecs_entity_inline.h"
    #include "Implementation/mecs_system_inline.h"

#endif

