
namespace mecs::entity
{
    struct tmp_ref
    {
        constexpr bool isValid(void) const noexcept { return m_PageIndex.m_Value != 0xffff; }

        template< typename T >
        T& getComponent(type_guid TypeGuid = T::type_guid_v) noexcept;

        template< typename T >
        constexpr const T& getComponent(type_guid TypeGuid = T::type_guid_v) const noexcept;

        template< typename T >
        T* findComponent(type_guid TypeGuid = T::type_guid_v) noexcept;

        template< typename T >
        constexpr const T* findComponent(type_guid TypeGuid = T::type_guid_v) const noexcept;

        void clear( void )
        {
            m_PageIndex.m_Value = 0xffff;
        }

        component::group::instance*        m_pGroupInstance;
        component::group::local_index      m_LocalIndex;
        component::group::page_index       m_PageIndex;
    };

    constexpr static std::size_t                                max_entity_count_v = xcore::bits::RoundToNextPowOfTwo(100000000ull)
                                                                XCORE_CMD_ASSERT(  - xcore::bits::RoundToNextPowOfTwo(100000000ull)
                                                                                    + xcore::bits::RoundToNextPowOfTwo(  1000000ull));
    using guid      = xcore::guid::unit<64, struct ecs_entity_tag>;
    using map_t     = tools::map_fixed< guid, tmp_ref, max_entity_count_v >;

    struct instance
    {
        constexpr static type_guid  type_guid_v {1ull};
        constexpr static auto       name_v      = xconst_universal_str("entity");

        map_t::entry*    m_pInstance;

        xforceinline      bool            isZombie      ( void ) const noexcept{ return reinterpret_cast<std::size_t>(m_pInstance)&1; }
        xforceinline      tmp_ref         getTmpRef     ( void ) const noexcept{ return getPointer().m_Value; }
        xforceinline      map_t::entry&   getPointer    ( void ) const noexcept{ return *reinterpret_cast<map_t::entry*>(reinterpret_cast<std::size_t>(m_pInstance) & (~std::size_t(1))); }
        xforceinline      guid            getGuid       ( void ) const noexcept{ return getPointer().m_Key; }
        xforceinline      void            MarkAsZombie  ( void )       noexcept{ reinterpret_cast<std::size_t&>(m_pInstance) = reinterpret_cast<std::size_t>(m_pInstance)|1; }
    };

}