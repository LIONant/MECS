namespace mecs::commands
{
    using command_pool = containers::cmd_pool<1024>;

    namespace details
    {
    struct cmd_add_entity
    {
        entity::guid            m_Guid;
        int                     m_Size;
        component::type_guid*   m_pComponentTypes;
        void*                   m_pNewComponentData;
    };

    struct cmd_modify_components
    {
        entity::guid            m_Guid;
        int                     m_DelComponentCount{0};
        component::type_guid*   m_pDelComponentTypes{nullptr};
        int                     m_NewComponentCount{0};
        component::type_guid*   m_pNewComponentTypes{nullptr};
        void*                   m_pNewComponentData{nullptr};
    };

    struct cmd_set_components
    {
        entity::guid            m_Guid;
        component::type_guid*   m_pComponentTypes{nullptr};
        int                     m_ComponentCount{0};
        void*                   m_pNewComponentData{nullptr};
    };

    using cmds = std::variant
    <
        cmd_add_entity
        ,cmd_modify_components
        ,cmd_set_components
    >;

    template< typename T_CLASS,typename...T_ARGS >
    struct set_component_handler
    {
        using component_tuple = std::tuple< T_ARGS... >;

        T_CLASS& m_Ref;

        template< typename T >
        auto& getComponentData(void) noexcept
        {
            return std::get<T&>(*reinterpret_cast<component_tuple*>(m_Ref.m_pNewComponentData));
        }
    };

    struct modify_components_handler
    {
        cmd_modify_components&  m_Ref;
        command_pool&           m_Pool;
        command_pool::handle    m_Handel;

        template< typename...T_COMPONENTS >
        auto AddComponents(void) noexcept
        {
            using component_tuple = std::tuple<T_COMPONENTS...>;
            std::array  ComponentData{T_COMPONENTS::factory::guid_component_type_v ...};

            m_Ref.m_NewComponentCount   = sizeof...(T_COMPONENTS);
            m_Ref.m_pNewComponentData   = &m_Pool.NewData<component_tuple>(m_Handel);
            m_Ref.m_pNewComponentTypes  = m_Pool.NewData<decltype(ComponentData)>(m_Handel,ComponentData).data();
            return details::set_component_handler<cmd_modify_components,T_COMPONENTS...>{ m_Ref };
        }

        template< typename...T_COMPONENTS >
        void RemoveComponents(void) noexcept
        {
            std::array  ComponentData{T_COMPONENTS::factory::guid_component_type_v ...};
            m_Ref.m_DelComponentCount   = sizeof...(T_COMPONENTS);
            m_Ref.m_pDelComponentTypes  = m_Pool.NewData<decltype(ComponentData)>(m_Handel,ComponentData).data();
        }
    };
    }

    struct command_buffer
    {
        template< typename...T_COMPONENTS >
        auto AddEntity(entity::guid Guid) noexcept
        {
            using component_tuple = std::tuple< T_COMPONENTS... >;

            auto&       EntityCmd           = m_Pool.NewCmd<details::cmd_add_entity>(m_Handle);
            std::array  ComponentData{T_COMPONENTS::factory::guid_component_type_v ...};

            EntityCmd.m_Guid                = Guid;
            EntityCmd.m_pComponentTypes     = m_Pool.NewData<decltype(ComponentData)>(m_Handle,ComponentData).data();
            EntityCmd.m_Size                = ComponentData.size();
            EntityCmd.m_pData               = &m_Pool.NewData<component_tuple>(m_Handle);

            return details::set_component_handler<details::cmd_add_entity,T_COMPONENTS...>{ EntityCmd };
        }

        auto EntityModifyComponets(entity::guid Guid) noexcept
        {
            auto& ModifyCmd = m_Pool.NewCmd<details::cmd_modify_components>(m_Handle);
            ModifyCmd.m_Guid = Guid;
            return details::modify_components_handler{ModifyCmd,m_Pool,m_Handle};
        }

        template< typename...T_COMPONENTS >
        void SetComponents(entity::guid Guid) noexcept
        {
            using component_tuple = std::tuple< T_COMPONENTS... >;

            auto&       SetCmd      = m_Pool.NewCmd<details::cmd_set_components>(m_Handle);
            std::array  ComponentData{T_COMPONENTS::factory::guid_component_type_v ...};

            SetCmd.m_Guid                   = Guid;
            SetCmd.m_ComponentCount         = sizeof...(T_COMPONENTS);
            SetCmd.m_pComponentTypes        = m_Pool.NewData<decltype(ComponentData)>(m_Handle,ComponentData).data();
            SetCmd.m_pNewComponentData      = &m_Pool.NewData<component_tuple>(m_Handle);

            return details::set_component_handler<details::cmd_set_components,T_COMPONENTS...>{ SetCmd };
        }

        command_pool&           m_Pool;
        command_pool::handle    m_Handle;
    };
}