namespace mecs::sync_point
{
    struct instance;

    struct events
    {
        using done  = xcore::types::make_unique< xcore::event::type<sync_point::instance&>, struct done_tag >;
        using start = xcore::types::make_unique< xcore::event::type<sync_point::instance&>, struct start_tag >;

        start m_Start;
        done  m_Done;
    };

    struct instance : xcore::scheduler::trigger<settings::max_systems_v>
    {
        events          m_Events;
        bool            m_bDisable = false;
        int             m_FreethCount{0};
        std::uint32_t   m_Tick{ 0 };

        void FreezCount() noexcept
        {
            m_FreethCount = m_NotificationCounter.load();
        }

        virtual void qt_onTriggered ( void ) noexcept override
        {
            m_NotificationCounter = m_FreethCount;
            m_Events.m_Done.NotifyAll(*this);
            m_Tick++;
            if(m_bDisable == false)
            {
                m_Events.m_Start.NotifyAll( *this );
                xcore::scheduler::trigger<settings::max_systems_v>::qt_onTriggered();
            }
        }

        ~instance()
        {
            m_NotificationCounter = 0;
        }

    };
}