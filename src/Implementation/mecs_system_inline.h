
namespace mecs::system
{
    //---------------------------------------------------------------------------------
    // SYSTEM::INSTANCE 
    //---------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------
    template< typename...T_COMPONENTS_AND_TAGS > inline
    component::group::instance& instance::getOrCreateGroup( void ) noexcept
    {
        constexpr static component::group::details::guid_arrays_from_types_with_duplication<T_COMPONENTS_AND_TAGS...> arrays_v;

        //
        // Look in the cache first
        //
        {
            xcore::lock::scope Lk( std::as_const(m_Cache.m_Lock) );
            for( auto& Line : m_Cache.m_Lines )
            {
                if( Line.m_Guid == arrays_v.group_guid_v )
                    return *Line.m_pGroup;
            }
        }

        xcore::lock::scope Lk( m_Cache.m_Lock );
        for( auto& Line : m_Cache.m_Lines )
        {
            if( Line.m_Guid == arrays_v.group_guid_v )
                return *Line.m_pGroup;
        }

        auto& Group = m_World.getOrCreateGroup<T_COMPONENTS_AND_TAGS...>();
        xassert( Group.m_Guid == arrays_v.group_guid_v );
        std::as_const(Group.m_SemaphoreLock).lock();
        m_Cache.m_Lines.push_back( details::cache::line{ arrays_v.group_guid_v, &Group } );

        return Group;
    }


    //---------------------------------------------------------------------------------

    template< typename T > inline
    bool instance::getGroup( component::group::guid gGroup, T&& CallBack ) noexcept
    {
        //
        // Look in the cache first
        //
        std::as_const(m_Cache.m_Lock).lock();
        for( auto& Line : m_Cache.m_Lines )
        {
            if( Line.m_Guid == gGroup )
            {
                CallBack( *Line.m_pGroup );
                std::as_const(m_Cache.m_Lock).unlock();
                return true;
            }
        }
        std::as_const(m_Cache.m_Lock).unlock();

        m_Cache.m_Lock.lock();
        for( auto& Line : m_Cache.m_Lines )
        {
            if( Line.m_Guid == gGroup )
            {
                CallBack( *Line.m_pGroup );
                m_Cache.m_Lock.unlock();
                return true;
            }
        }

        return m_World.m_mapGroupPerTag.getExlusive( gGroup, [&](std::unique_ptr<component::group::instance>& upGroup )
        {
            // Ok we need to fill the cache
            m_Cache.m_Lines.push_back( details::cache::line{ gGroup, upGroup.get() } );
            std::as_const(upGroup->m_SemaphoreLock).lock();
            // Handle the callback
            CallBack( *upGroup );
        });
        m_Cache.m_Lock.unlock();
    }

    //---------------------------------------------------------------------------------

    template< typename...T_COMPONENTS_AND_TAGS, typename T_INIT_ENTITY_CALLBACK > xforceinline
    void instance::createEntity( T_INIT_ENTITY_CALLBACK&& InitCallback, entity::guid gEntity ) noexcept
    {
        auto& Group = getOrCreateGroup<T_COMPONENTS_AND_TAGS...>();
        createEntity( Group, std::forward<T_INIT_ENTITY_CALLBACK>(InitCallback), gEntity );
    }

    //---------------------------------------------------------------------------------

    template< typename...T_COMPONENTS_AND_TAGS, typename T_GET_CALLBACK, typename T_INIT_ENTITY_CALLBACK > xforceinline
    void instance::getOrCreatEntity( entity::guid gEntity, T_GET_CALLBACK&& GetCallback, T_INIT_ENTITY_CALLBACK&& InitCallback ) noexcept
    {
        auto& Group = getOrCreateGroup<T_COMPONENTS_AND_TAGS...>();
        m_World.getOrCreateEntity( *this, Group, GetCallback, InitCallback, gEntity );
    }

    //---------------------------------------------------------------------------------

    template< typename...T_COMPONENTS_AND_TAGS, typename T_GET_CALLBACK, typename T_INIT_ENTITY_CALLBACK > xforceinline
    void instance::getOrCreatEntityRelax( entity::guid gEntity, T_GET_CALLBACK&& GetCallback, T_INIT_ENTITY_CALLBACK&& InitCallback ) noexcept
    {
        auto& Group = getOrCreateGroup<T_COMPONENTS_AND_TAGS...>();
        m_World.getOrCreateEntity( *this, Group, GetCallback, InitCallback, gEntity );
    }

    //---------------------------------------------------------------------------------

    template< typename T_SYSTEM, typename T_EVENT, typename...T_ARGS > xforceinline 
    void instance::GeneralEventNotify( T_ARGS&&...Args ) noexcept
    {
        static_assert( std::is_base_of_v< mecs::system::instance, T_SYSTEM  >, "The T_SYSTEM needs to be the system where you are sending the event" );
        static_assert( std::is_base_of_v< mecs::event::instance,  T_EVENT   >, "The T_EVENT  needs to be derived from event::instance" );

        auto& This   = *reinterpret_cast<details::custom_system<T_SYSTEM>*>(this);
        auto& Event  = std::get<xcore::types::make_unique<typename T_EVENT::event_t, T_EVENT >>(This.m_Events);
        Event.NotifyAll( *this, std::forward<T_ARGS>(Args)... );
    }

    //---------------------------------------------------------------------------------

    template< typename T_EVENT, typename...T_ARGS > xforceinline 
    void instance::EventNotify( T_ARGS&&...Args ) noexcept
    {
        GeneralEventNotify<T_EVENT::system_t, T_EVENT>( std::forward<T_ARGS>(Args)... );
    }

}