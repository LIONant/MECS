
namespace mecs::system::details
{
    //-----------------------------------------------------------------------------------------

    bool LockQueryComponents( system::instance& System, query::instance& Query ) noexcept
    {
        bool bErrors = false;
        
        xassert(Query.m_lResults.size());

        // Lock all the groups that we need to work with so that no one tries to add/remove entities
        for( const auto& E : Query.m_lResults )
        {
            auto& Group = const_cast<component::group::instance&>(*E.m_pGroup);

            // TODO: Must lock each component base on r/w also must lock the group
            
            // We lock as read only even if one of our components is going to write to it since really what we are saying is that we 
            // are going to use the group. Unlock will happen when we are done using this group.
            std::as_const(Group.m_SemaphoreLock).lock();
                
            // Now we are going to tell the system which components in fact we are going to use. 
            // This is mainly for debugging purposes to make sure that we don't have two system writing to the same component
            static_assert( std::is_const_v<decltype(Group)> == false, "We need to lock here as read only" );
            xcore::lock::scope Lk( Group.m_lComponentLocks );
            auto& ComponentLockArray = Group.m_lComponentLocks.get();

            // Lock components
            for( std::uint8_t end = E.m_nRemaps, I=0u; I<end; ++I )
            {
                const auto iComp = E.m_lLockRemaps[I];
                if( iComp == 0xff ) continue;

                auto&      CLock = ComponentLockArray[iComp];

                if( E.m_AccessMode[I] == component::access_mode::LINEAR_WRITE )
                {
                    // I am trying to lock the entity... so lets see...
                    if( iComp == 0 )
                    {
                        // If I am locking the entity no one can have any of my components locked
                        // Is the Entity already locked?
                        if( CLock.m_gSystem.isValid() )
                        {
                            if( CLock.m_gSystem != System.getGuid() )
                            {
                                bErrors = true;
                                mecs::lock_error Error;

                                Error.m_lSystemGuids[0] = CLock.m_gSystem;
                                Error.m_lSystemGuids[1] = System.getGuid();
                                Error.m_pMessage        = "Error: We have two systems that are trying to lock the entity component";

                                xcore::lock::scope S( System.m_World.m_LockErrors );
                                System.m_World.m_LockErrors.get().push_back( Error );
                            }

                            for( int i=1; i<Group.m_GroupDescriptor.m_nComponents; i++ )
                            {
                                if( ComponentLockArray[i].m_gSystem != System.getGuid() )
                                {
                                    bErrors = true;
                                    mecs::lock_error Error;

                                    Error.m_lSystemGuids[0] = CLock.m_gSystem;
                                    Error.m_lSystemGuids[1] = System.getGuid();
                                    Error.m_pMessage        = "Error: We are trying to lock the entity but someone is writing some of its components";

                                    xcore::lock::scope S( System.m_World.m_LockErrors );
                                    System.m_World.m_LockErrors.get().push_back( Error );
                                }
                            }
                        }

                        if( bErrors == false )
                        {
                            CLock.m_gSystem = System.getGuid();
                        }
                    }
                    else 
                    {
                        if( ComponentLockArray[0].m_gSystem.isValid() && ComponentLockArray[0].m_gSystem != System.getGuid() )
                        {
                            bErrors = true;
                            mecs::lock_error Error;

                            Error.m_lSystemGuids[0] = ComponentLockArray[0].m_gSystem;
                            Error.m_lSystemGuids[1] = System.getGuid();
                            Error.m_pMessage        = "Error: We are trying to write to some components by someone has the entity already locked";

                            xcore::lock::scope S( System.m_World.m_LockErrors );
                            System.m_World.m_LockErrors.get().push_back( Error );
                        }

                        if( CLock.m_gSystem.isValid() && CLock.m_gSystem != System.getGuid() )
                        {
                            bErrors = true;
                            mecs::lock_error Error;

                            Error.m_lSystemGuids[0] = CLock.m_gSystem;
                            Error.m_lSystemGuids[1] = System.getGuid();
                            Error.m_pMessage        = "Error: We are trying to write to a component but another system already has it locked";

                            xcore::lock::scope S( System.m_World.m_LockErrors );
                            System.m_World.m_LockErrors.get().push_back( Error );
                        }

                        if( CLock.m_nReaders )
                        {
                            bErrors = true;
                            mecs::lock_error Error;

                            Error.m_lSystemGuids[0] = CLock.m_gSystem;
                            Error.m_lSystemGuids[1] = System.getGuid();
                            Error.m_pMessage        = "Error: We are trying to write to a component but there other system marked the component as read only";

                            xcore::lock::scope S( System.m_World.m_LockErrors );
                            System.m_World.m_LockErrors.get().push_back( Error );
                        }

                        if( bErrors == false )
                        {
                            CLock.m_gSystem = System.getGuid();
                        }
                    }
                }
                else
                {
                    if( CLock.m_gSystem.isValid() && CLock.m_gSystem != System.getGuid() )
                    {
                        bErrors = true;
                        mecs::lock_error Error;

                        Error.m_lSystemGuids[0] = CLock.m_gSystem;
                        Error.m_lSystemGuids[1] = System.getGuid();
                        Error.m_pMessage        = "Error: We are trying to mark a component as read only but another system locked it for writing";

                        xcore::lock::scope S( System.m_World.m_LockErrors );
                        System.m_World.m_LockErrors.get().push_back( Error );
                    }

                    if( bErrors == false )
                    {
                        CLock.m_nReaders++;
                    }
                }
            }
        }

        return bErrors == false;
    }

    //-----------------------------------------------------------------------------------------

    void UnlockQueryComponents( system::instance& System, query::instance& Query ) noexcept
    {
        for( const auto& E : Query.m_lResults )
        {
            auto& Group = const_cast<component::group::instance&>(*E.m_pGroup);

            // Release each of the components that we locked before
            static_assert( std::is_const_v<decltype(Group)> == false, "We need to lock here as read only" );
            xcore::lock::scope Lk( Group.m_lComponentLocks );
            auto& ComponentLockArray = Group.m_lComponentLocks.get();

            // Unlock component
            for( std::uint8_t end = E.m_nRemaps, I=0u; I<end; ++I )
            {
                const auto iComp = E.m_lLockRemaps[I];
                if( iComp == 0xff ) continue;

                auto&      CLock = ComponentLockArray[iComp];
                if( E.m_AccessMode[I] == component::access_mode::LINEAR_WRITE )
                {
                    xassert( CLock.m_gSystem == System.getGuid() );
                    CLock.m_gSystem.setNull();
                }
                else
                {
                    xassert(CLock.m_nReaders);
                    CLock.m_nReaders--;
                }
            }

            // We officially release this group
            std::as_const(Group.m_SemaphoreLock).unlock();
        }
    }
}


