namespace mecs::entity
{
    template< typename T > xforceinline
    T& tmp_ref::getComponent(type_guid TypeGuid) noexcept
    {
        if constexpr ( std::is_same_v<T, const T> )
        {
            auto p = reinterpret_cast<T*>(m_pGroupInstance->findComponentReadOnly(*this, TypeGuid));
            assert(p);
            return *p;
        }
        else
        {
            auto p = reinterpret_cast<T*>(m_pGroupInstance->findComponentMutable(*this, TypeGuid));
            assert(p);
            return *p;
        }
    }

    template< typename T > xforceinline
    constexpr const T& tmp_ref::getComponent(type_guid TypeGuid) const noexcept
    {
        if constexpr ( std::is_same_v<T, const T> )
        {
            auto p = reinterpret_cast<T*>(m_pGroupInstance->findComponentReadOnly(*this, TypeGuid));
            assert(p);
            return *p;
        }
        else
        {
            auto p = reinterpret_cast<T*>(m_pGroupInstance->findComponentMutable(*this, TypeGuid));
            assert(p);
            return *p;
        }
    }

    template< typename T > xforceinline
    T* tmp_ref::findComponent(type_guid TypeGuid) noexcept
    {
        if constexpr ( std::is_same_v<T, const T> )
        {
            return reinterpret_cast<T*>(m_pGroupInstance->findComponentReadOnly(*this, TypeGuid));
        }
        else
        {
            return reinterpret_cast<T*>(m_pGroupInstance->findComponentMutable(*this, TypeGuid));
        }
    }

    template< typename T > xforceinline
    constexpr const T* tmp_ref::findComponent(type_guid TypeGuid) const noexcept
    {
        if constexpr ( std::is_same_v<T, const T> )
        {
            return reinterpret_cast<T*>(m_pGroupInstance->findComponentReadOnly(*this, TypeGuid));
        }
        else
        {
            return reinterpret_cast<T*>(m_pGroupInstance->findComponentMutable(*this, TypeGuid));
        }
    }
}