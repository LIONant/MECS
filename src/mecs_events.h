namespace mecs::event
{
    using guid      = xcore::guid::unit<64,struct mecs_event_tag>;

    template< typename ...T_ARGS >
    struct type
    {
        using callback              = xcore::function::buffer< 1, void(T_ARGS&&...) >;

        using arguments_tuple_t     = std::tuple< T_ARGS... >;

        std::vector<callback>       m_lDelegates;

        template< auto T_CALLBACK, typename T >
        xforceinline void AddDelegate( T* pThis ) noexcept
        {
            m_lDelegates.push_back( [pThis]( T_ARGS&&... Args ) constexpr noexcept 
            {
                std::invoke( T_CALLBACK, pThis, std::forward<T_ARGS>(Args) ... );
            });
        }

        xforceinline constexpr bool hasSubscribers( void ) const noexcept { return m_lDelegates.empty() == false; }
        xforceinline constexpr void NotifyAll( T_ARGS... Args ) const noexcept
        {
            for( auto& E : m_lDelegates ) E( std::forward<T_ARGS>(Args) ... );
        }
    };

    namespace details
    {
        template< typename T_EVENT >
        constexpr bool EventContract() noexcept
        {
            static_assert(      xcore::types::is_specialized_v<type, typename T_EVENT::event_t >
                            ,   "Please use xcore::event::type<...> to define your event" );

            static_assert(      std::is_same_v<decltype(T_EVENT::guid_v), const event::guid >
                            ,   "Make sure your event has a defined a guid_v of the right type (mecs::event::guid)" );

            static_assert(      std::is_same_v<decltype(T_EVENT::name_v), const xcore::string::const_universal >
                            ,   "Make sure your event has a defined a name_v of the right type (xcore::string::const_universal)" );

            static_assert(      T_EVENT::guid_v.isValid()
                            ,   "Make sure your event has a valid guid_v. Did you know you can initialize them with a string?" );

            return true;
        };

        struct group_event
        {
            using                       event_t         = type< mecs::entity::instance&, mecs::system::instance& >;
            constexpr static auto       type_guid_v     = mecs::type_guid       { "mecs::event::group_event" };
        };

        namespace detail
        {
            template< std::size_t T_SIZE, typename...T_COMPONENTS >
            struct choose
            {
                static_assert(T_SIZE > 0 );
                constexpr static auto       components_v    = std::array{ T_COMPONENTS::type_guid_v ... };
            };

            template<>
            struct choose<1,void>
            {
                constexpr static auto       components_v    = std::span<type_guid>  { nullptr, std::size_t{0u} };
            };

            template<>
            struct choose<0>
            {
                constexpr static auto       components_v    = std::span<type_guid>  { nullptr, std::size_t{0u} };
            };
        }

        template< typename...T_COMPONENTS >
        static constexpr auto choose_v = detail::choose<sizeof...(T_COMPONENTS), T_COMPONENTS...>::components_v;
    }

    struct instance
    {
        constexpr static auto       type_guid_v     = mecs::type_guid       { "mecs::event::system_event" };
    };

    struct create_entity final : details::group_event
    {
        constexpr static auto       guid_v          = mecs::event::guid     { "mecs::event::create_entity" };
        constexpr static auto       name_v          = xconst_universal_str  ( "mecs::event::create_entity" );
    };

    struct destroy_entity final : details::group_event
    {
        constexpr static auto       guid_v          = mecs::event::guid     { "mecs::event::destroy_entity" };
        constexpr static auto       name_v          = xconst_universal_str  ( "mecs::event::destroy_entity" );
    };

    template< typename...T_COMPONENTS >
    struct moved_in final : details::group_event
    {
//        constexpr static auto       components_v    = std::array{ T_COMPONENTS::type_guid_v ... };
        constexpr static auto       guid_v          = mecs::event::guid     { "mecs::event::moved_in" };
        constexpr static auto       name_v          = xconst_universal_str  ( "mecs::event::moved_in" );
        using                       components_t    = std::tuple<T_COMPONENTS...>;
    };

    template< typename...T_COMPONENTS >
    struct moved_out final : details::group_event
    {
//        constexpr static auto       components_v    = std::array{ T_COMPONENTS::type_guid_v ... };
        constexpr static auto       guid_v          = mecs::event::guid     { "mecs::event::moved_out" };
        constexpr static auto       name_v          = xconst_universal_str  ( "mecs::event::moved_out" );
        using                       components_t    = std::tuple<T_COMPONENTS...>;
    };

    template< typename...T_COMPONENTS >
    struct updated_component final : details::group_event
    {
        constexpr static auto       components_v    = details::choose_v< T_COMPONENTS... >;
        constexpr static auto       guid_v          = mecs::event::guid     { "mecs::event::updated_component" };
        constexpr static auto       name_v          = xconst_universal_str  ( "mecs::event::updated_component" );
    };

    using updated_entity = updated_component<>;
}
