namespace mecs::query
{
    //---------------------------------------------------------------------------------
    // QUERY::FILTER_BITS
    //---------------------------------------------------------------------------------
    using filter_bits = xcore::types::make_unique< tools::bits, struct filter_bits_tag >;

    //---------------------------------------------------------------------------------
    // QUERY::ALL, NONE, ANY
    //---------------------------------------------------------------------------------
    template< typename...T_COMPONENTS_AND_TAGS >
    struct all : component::group::details::guid_arrays_from_types< T_COMPONENTS_AND_TAGS... >
    {
        static_assert( ((std::is_reference_v<T_COMPONENTS_AND_TAGS> == false) && ...) );
        static_assert( ((std::is_const_v<T_COMPONENTS_AND_TAGS>     == false) && ...) );
    };

    template< typename...T_COMPONENTS_AND_TAGS >
    struct none : component::group::details::guid_arrays_from_types< T_COMPONENTS_AND_TAGS... >
    {
        static_assert( ((std::is_reference_v<T_COMPONENTS_AND_TAGS> == false) && ...) );
        static_assert( ((std::is_const_v<T_COMPONENTS_AND_TAGS>     == false) && ...) );
    };

    template< typename...T_COMPONENTS_AND_TAGS >
    struct any : component::group::details::guid_arrays_from_types< T_COMPONENTS_AND_TAGS... >
    {
        static_assert( ((std::is_reference_v<T_COMPONENTS_AND_TAGS> == false) && ...) );
        static_assert( ((std::is_const_v<T_COMPONENTS_AND_TAGS>     == false) && ...) );
    };

    //---------------------------------------------------------------------------------
    // QUERY::ACCESS
    //---------------------------------------------------------------------------------
    template< typename...T_COMPONENTS_WITH_CONST_AND_REFERENCES >
    struct access
    {
        static_assert( (std::remove_pointer_t<std::decay_t<T_COMPONENTS_WITH_CONST_AND_REFERENCES>>::type_guid_v.isValid() && ...), "You must pass a valid component" );
        static_assert( (( std::is_reference_v<T_COMPONENTS_WITH_CONST_AND_REFERENCES>
                       || std::is_pointer_v<T_COMPONENTS_WITH_CONST_AND_REFERENCES>   ) && ...), "You must specify the access type if reference '&' or pointer '*' for each component" );

        using tuple_t = std::tuple<T_COMPONENTS_WITH_CONST_AND_REFERENCES ...>;

        tuple_t m_Tuple;

        template< typename T > xforceinline constexpr
        bool isValid( void ) const noexcept
        {
            static_assert( std::is_pointer_v<T> );
            return std::get<T>(m_Tuple) != nullptr;
        }

        template< typename T > xforceinline constexpr
        auto& get( void ) const noexcept
        {
            static_assert( std::is_reference_v<T> || std::is_pointer_v<T> );
            if constexpr ( std::is_reference_v<T> )
            {
                return std::get<T>(m_Tuple);
            }
            else
            {
                xassume(std::get<T>(m_Tuple));
                return *std::get<T>(m_Tuple);
            }
        }
    };

    //---------------------------------------------------------------------------------
    // QUERY::SYSTEM FUNCTION
    //---------------------------------------------------------------------------------
    namespace details
    {
        template<typename...T_ARGS>
        constexpr auto TypeArray( std::tuple<T_ARGS...>* ) noexcept
        {
            static_assert( ((std::is_reference_v<T_ARGS> || std::is_pointer_v<T_ARGS>) && ...) );
            return std::array
            {
                xcore::types::decay_full_t<T_ARGS>::type_guid_v ...
            };
        }

        template<>
        constexpr auto TypeArray( std::tuple<>* ) noexcept
        {
            return std::span<type_guid>
            {
                nullptr
                , static_cast<std::size_t>(0)
            };
        }

        template<typename...T_ARGS>
        constexpr auto ConstArray( std::tuple<T_ARGS...>* ) noexcept
        {
            static_assert( ((std::is_reference_v<T_ARGS> || std::is_pointer_v<T_ARGS>) && ...) );
            return std::array
            {
                (
                    (std::is_const_v<std::remove_pointer_t<std::remove_reference_t<T_ARGS>>>)                         ? component::access_mode::READ_ONLY
                    : (   std::is_base_of_v< component::quantum_mutable,       xcore::types::decay_full_t<T_ARGS> >
                       || std::is_base_of_v< component::quantum_double_buffer, xcore::types::decay_full_t<T_ARGS> > ) ? component::access_mode::QUANTUM_WRITE
                      :                                                                                                 component::access_mode::LINEAR_WRITE
                ) ... 
            };
        }

        template< typename T > struct remove_references;
        template< typename...T_ARGS >
        struct remove_references< std::tuple< T_ARGS &... >>
        {
            using type = std::tuple<T_ARGS...>;
        };

        template< std::size_t T_SIZE, typename T_FUNCTION, typename T_CONST_FUNCTION, typename T_SORTED, typename T_CONST_SORTED >
        constexpr auto remap( T_FUNCTION& Fun, T_CONST_FUNCTION& isConstFun, T_SORTED& Sort, T_CONST_SORTED& isConstSort ) noexcept
        {
            std::array<int,T_SIZE> Array{};
            for( int i=0; i<Array.size(); ++i )
            {
                for( int j=0; j<Array.size(); ++j )
                    if( Sort[i] == Fun[j] && isConstSort[i] == isConstFun[j] ) 
                    {
                        Array[i] = j - i;
                    }
            }
            return Array;
        }

        namespace details
        {
            template< template< typename... > class T_CLASS, typename T, typename...T_ARGS>
            struct get_compound_type;

            template< template< typename... > class T_CLASS >
            struct get_compound_type< T_CLASS, void >
            {
                using type = T_CLASS<>;
            };

            template< template< typename... > class T_CLASS, typename T, typename...T_ARGS>
            struct get_compound_type
            {
                using type = typename std::conditional_t
                <
                        xcore::types::is_specialized_v< T_CLASS, T >
                    , T
                    , typename get_compound_type<T_CLASS, T_ARGS...>::type
                >;
            };
        }

        template< template< typename... > class T_CLASS, typename...T_ARGS >
        using get_compound_type_t = typename details::get_compound_type<T_CLASS, T_ARGS... , void>::type;

        template< typename...T_ARGS >
        xcore::types::tuple_cat_t< std::conditional_t< std::is_pointer_v<T_ARGS>, std::tuple<T_ARGS>, std::tuple<>> ... > Any(std::tuple<T_ARGS...>*);
        
        template< typename...T_ARGS >
        xcore::types::tuple_cat_t< std::conditional_t< std::is_reference_v<T_ARGS>, std::tuple<T_ARGS>, std::tuple<>> ... > All(std::tuple<T_ARGS...>*);
    }

    template< typename T >
    struct system_function
    {
        using function_t    = typename xcore::function::traits<T>;
        using args_t        = typename function_t::args_tuple;
        using sorted_args_t = typename xcore::types::tuple_sort_t< component::group::details::compare, args_t >;
        constexpr static auto list_v                        { details::TypeArray ( reinterpret_cast<args_t*>(nullptr) ) };
        constexpr static auto access_mode_list_v            { details::ConstArray( reinterpret_cast<args_t*>(nullptr) ) };
        constexpr static auto access_mode_sorted_list_v     { details::ConstArray( reinterpret_cast<sorted_args_t*>(nullptr) ) };
        constexpr static auto sorted_list_v                 { details::TypeArray ( reinterpret_cast<sorted_args_t*>(nullptr) ) };
        constexpr static auto remap_v                       { details::remap<list_v.size()>( list_v, access_mode_list_v, sorted_list_v, access_mode_sorted_list_v ) };

        using any_t = decltype( details::Any(reinterpret_cast<args_t*>(nullptr)) );
        using all_t = decltype( details::All(reinterpret_cast<args_t*>(nullptr)) );

        constexpr static auto any_v                         { details::TypeArray ( reinterpret_cast<any_t*>(nullptr) ) };
        constexpr static auto all_v                         { details::TypeArray ( reinterpret_cast<all_t*>(nullptr) ) };
    };

    //---------------------------------------------------------------------------------
    // QUERY::DEFINE
    //---------------------------------------------------------------------------------
    struct define_data
    {
        struct query
        {
            std::span<const type_guid>              m_All;            // Which components/tags we must have
            std::span<const type_guid>              m_Any;            // Must have at least one component/tag from the set
            std::span<const type_guid>              m_None;           // Which components/tags you must not have
        };

        struct access
        {
            std::span<const type_guid>              m_Components;     // Which components/tags we must have
            std::span<const component::access_mode> m_AccessMode;     // Which components/tags we must have
        };

        query   m_ComponentQuery;
        query   m_TagQuery;
    };

    template< typename...T_QUERY_FILTERS >
    struct define : define_data
    {
        using all_filter    = details::get_compound_type_t< mecs::query::all,    T_QUERY_FILTERS... >;
        using none_filter   = details::get_compound_type_t< mecs::query::none,   T_QUERY_FILTERS... >;
        using any_filter    = details::get_compound_type_t< mecs::query::any,    T_QUERY_FILTERS... >;

        template< typename T >
        constexpr static void Initializer( define& This ) noexcept
        {
            if constexpr ( xcore::types::is_specialized_v<mecs::query::all, T> ) 
            {
                if(T::tag_guid_list_v.size()>1)         This.m_TagQuery.m_All           = std::span<const type_guid>(&T::tag_guid_list_v[1],       T::tag_guid_list_v.size()-1);
                if(T::component_guid_list_v.size())     This.m_ComponentQuery.m_All     = std::span<const type_guid>(&T::component_guid_list_v[0], T::component_guid_list_v.size());
            }
            else if constexpr ( xcore::types::is_specialized_v<mecs::query::any, T> )
            {
                if(T::tag_guid_list_v.size()>1)         This.m_TagQuery.m_Any           = std::span<const type_guid>(&T::tag_guid_list_v[1],       T::tag_guid_list_v.size()-1);
                if(T::component_guid_list_v.size())     This.m_ComponentQuery.m_Any     = std::span<const type_guid>(&T::component_guid_list_v[0], T::component_guid_list_v.size());
            }
            else if constexpr ( xcore::types::is_specialized_v<mecs::query::none, T> )
            {
                if(T::tag_guid_list_v.size()>1)         This.m_TagQuery.m_None          = std::span<const type_guid>(&T::tag_guid_list_v[1],       T::tag_guid_list_v.size()-1);
                if(T::component_guid_list_v.size())     This.m_ComponentQuery.m_None    = std::span<const type_guid>(&T::component_guid_list_v[0], T::component_guid_list_v.size() );
            }
        };

        constexpr define( void ) noexcept
        {
            ( Initializer<T_QUERY_FILTERS>( *this ), ... );
        }
    };

    //---------------------------------------------------------------------------------
    // QUERY::RESULT ENTRY
    //---------------------------------------------------------------------------------
    struct result_entry
    {
        component::group::instance*                         m_pGroup;
        std::array<std::uint16_t,15>                        m_lRemaps;      // Offsets into the actual page where the component is
        std::array<std::uint8_t,15>                         m_lLockRemaps;  // Offsets into the actual page where the component is
        std::span<const component::access_mode>             m_AccessMode;
        std::uint8_t                                        m_nRemaps;
    };

    //---------------------------------------------------------------------------------
    // QUERY::INSTANCE
    //---------------------------------------------------------------------------------
    struct instance
    {
        struct component_query
        {
            filter_bits                 m_All                   {nullptr};
            filter_bits                 m_Any                   {nullptr};
            filter_bits                 m_None                  {nullptr};
        };

        struct tag_query
        {
            filter_bits                 m_All                   {nullptr};
            filter_bits                 m_Any                   {nullptr};
            filter_bits                 m_None                  {nullptr};
        };

        xcore::vector<result_entry>     m_lResults              {};
        bool                            m_isInitialized         {false};
//        filter_bits                     m_Function              {nullptr};
        filter_bits                     m_Write                 {nullptr};
        component_query                 m_ComponentQuery        {};
        tag_query                       m_TagQuery              {};
    };
}

