namespace mecs::component::group
{
    //---------------------------------------------------------------------------------
    // GROUP::DETAILS
    //---------------------------------------------------------------------------------
    namespace details
    {
        //---------------------------------------------------------------------------------

        template< typename...T_COMPONENTS >
        constexpr auto MakeArrayTypeGuids( std::tuple<T_COMPONENTS...>* ) noexcept
        {
            return std::array{ T_COMPONENTS::type_guid_v ... };
        }  

        //---------------------------------------------------------------------------------

        template<>
        constexpr auto MakeArrayTypeGuids( std::tuple<>* ) noexcept
        {
            return std::span<type_guid>{ nullptr, std::size_t{0u} };
        }  

        //---------------------------------------------------------------------------------

        template< typename T_COMPONENT >
        std::enable_if_t
        <
                true == std::is_base_of_v<mecs::component::double_buffer,               T_COMPONENT>
            ||  true == std::is_base_of_v<mecs::component::quantum_double_buffer,       T_COMPONENT>
            , std::tuple<T_COMPONENT, T_COMPONENT>
        > FilterOutTagsExtendDoubleBuffer();

        template< typename T_COMPONENT >
        std::enable_if_t
        <
                false == std::is_base_of_v<mecs::component::double_buffer,              T_COMPONENT>
            &&  false == std::is_base_of_v<mecs::component::quantum_double_buffer,      T_COMPONENT>
            &&  false == std::is_base_of_v<mecs::component::tag,                        T_COMPONENT>
            , std::tuple<T_COMPONENT>
        > FilterOutTagsExtendDoubleBuffer();

        template< typename T_COMPONENT >
        std::enable_if_t
        <
                false == std::is_base_of_v<mecs::component::double_buffer,              T_COMPONENT>
            &&  false == std::is_base_of_v<mecs::component::quantum_double_buffer,      T_COMPONENT>
            &&  true  == std::is_base_of_v<mecs::component::tag,                        T_COMPONENT>
            , std::tuple<>
        > FilterOutTagsExtendDoubleBuffer();

        template< typename T_COMPONENT >
        std::enable_if_t
        <
                true == std::is_base_of_v<mecs::component::tag,   T_COMPONENT>
            , std::tuple<T_COMPONENT>
        > FilterOutComponents();

        template< typename T_COMPONENT >
        std::enable_if_t
        <
                false == std::is_base_of_v<mecs::component::tag,  T_COMPONENT>
            , std::tuple<>
        > FilterOutComponents();

        template< typename T_COMPONENT >
        std::enable_if_t
        <
                false == std::is_base_of_v<mecs::component::tag,   T_COMPONENT>
            , std::tuple<T_COMPONENT>
        > FilterOutTags();

        template< typename T_COMPONENT >
        std::enable_if_t
        <
                true == std::is_base_of_v<mecs::component::tag,  T_COMPONENT>
            , std::tuple<>
        > FilterOutTags();

        //---------------------------------------------------------------------------------

        template< typename T_A, typename T_B >
        struct compare
        {
            constexpr static bool value = xcore::types::decay_full_t<T_A>::type_guid_v.m_Value
                                        < xcore::types::decay_full_t<T_B>::type_guid_v.m_Value;
        };

        //---------------------------------------------------------------------------------

        template< typename...T_COMPONENTS_AND_TAGS >
        struct guid_arrays_from_types
        {
            constexpr static auto component_guid_list_v = MakeArrayTypeGuids
            ( reinterpret_cast
                <
                    xcore::types::tuple_sort_t
                    < 
                        compare, xcore::types::tuple_cat_t
                        < 
                            decltype(FilterOutTags<std::decay_t<T_COMPONENTS_AND_TAGS>>())...
                        >
                    >*
                >(nullptr)
            );

            constexpr static auto tag_guid_list_v = MakeArrayTypeGuids
            ( reinterpret_cast
                <
                    xcore::types::tuple_sort_t
                    < 
                        compare, xcore::types::tuple_cat_t
                        <
                            decltype(FilterOutComponents<std::decay_t<T_COMPONENTS_AND_TAGS>>())...
                        >
                    >*
                >(nullptr)
            );
        };

        //---------------------------------------------------------------------------------

        template< typename...T_COMPONENTS_AND_TAGS >
        struct guid_arrays_from_types_with_duplication
        {
            constexpr static auto component_guid_list_v = component::group::details::MakeArrayTypeGuids
            ( reinterpret_cast
                <
                    xcore::types::tuple_sort_t
                    < 
                        compare, xcore::types::tuple_cat_t
                        < 
                            decltype(FilterOutTagsExtendDoubleBuffer<T_COMPONENTS_AND_TAGS>())...
                        >
                    >*
                >(nullptr)
            );

            constexpr static auto tag_guid_list_v = component::group::details::MakeArrayTypeGuids
            ( reinterpret_cast
                <
                    xcore::types::tuple_sort_t
                    < 
                        compare, xcore::types::tuple_cat_t
                        <
                            decltype(FilterOutComponents<T_COMPONENTS_AND_TAGS>())...
                        >
                    >*
                >(nullptr)
            );

            constexpr static auto component_only_group_guid_v = []() constexpr 
            {
                std::uint64_t Key = 0; 
                for (auto E : component_guid_list_v) 
                {
                    xassert( E != entity::instance::type_guid_v );
                    Key += E.m_Value;
                }
                return component::group::guid{ Key + entity::instance::type_guid_v.m_Value }; 
            }();

            constexpr static auto tag_only_group_guid_v = []() constexpr 
            {
                std::uint64_t Key = 0;
                for (auto E : tag_guid_list_v) Key += E.m_Value;
                return component::group::guid{ Key + entity::instance::type_guid_v.m_Value }; 
            }();

            constexpr static auto group_guid_v = component::group::guid{ component_only_group_guid_v.m_Value + tag_only_group_guid_v.m_Value };
        };
    }

    //---------------------------------------------------------------------------------
    // GROUP::ITERATOR
    //---------------------------------------------------------------------------------
    struct iterator : entity::tmp_ref
    {
        inline              iterator        operator++  (void)                      noexcept;
        inline constexpr    bool            operator!=  (std::nullptr_t)    const   noexcept;
        inline constexpr    entity::tmp_ref operator*   (void)              const   noexcept { return *this; }
        inline              entity::tmp_ref operator*   (void)                      noexcept { return *this; }
    };

    //---------------------------------------------------------------------------------
    // GROUP::INSTANCE::DETAIL
    //---------------------------------------------------------------------------------
    namespace details
    {
        struct offsets
        {
            std::uint16_t   m_Offset;           // Offset   - Where in the page is this component
            std::uint16_t   m_Size      :15     // Size     - Size of the component
                            , m_HasT1   :1;     // HasT1    - Does this component has a double buffer?
        };

        struct delete_entity
        {
            component::group::local_index      m_LocalIndex;
            component::group::page_index       m_PageIndex;
        };

        struct command_list
        {
            using commands = std::variant
            <
                delete_entity
            >;


            inline void clear( void ) noexcept { m_lCmds.clear(); }

            command_list(tools::page_mgr& Mgr) : m_lCmds{Mgr} {}

            tools::cmd_pool<commands>   m_lCmds;
        };
    }

    //---------------------------------------------------------------------------------
    // GROUP::DEPENDENCY
    //---------------------------------------------------------------------------------
    template< auto& T_GROUP_GUID_V, typename...T_COMPONENTS >
    struct dependency
    {
        constexpr static type_guid  m_gGroup;
        constexpr static std::array m_ComponentsTypes{ T_COMPONENTS::type_guid_v };
    };

    //---------------------------------------------------------------------------------
    // GROUP::DESCRIPTOR
    //---------------------------------------------------------------------------------
    struct descriptor
    {
        constexpr descriptor( xcore::span<const component::descriptor*> lComponetDesc ) noexcept
            : m_Guid                    { [&]() constexpr { std::uint64_t K = entity::instance::type_guid_v.m_Value; for( const auto p:lComponetDesc) K += p->m_Guid.m_Value; return type_guid{K};      }() }
            , m_lComponentTypeGuids     { [&]() constexpr { std::array<type_guid,64>                    D{}; for (int i = 0; i < lComponetDesc.size(); ++i) D[i] = lComponetDesc[i]->m_Guid;  return D; }() }
            , m_lComponentDescriptors   { [&]() constexpr { std::array<const component::descriptor*,64> D{}; for (int i = 0; i < lComponetDesc.size(); ++i) D[i] = lComponetDesc[i];          return D; }() }
            , m_nComponents             { static_cast<std::uint16_t>(lComponetDesc.size()) }
            , m_EntriesPerPage          { [&]() constexpr
                                            {
                                                const std::uint32_t     SizeOfOne           = [&]() constexpr { std::uint32_t S = 0; for (auto p : lComponetDesc) S += p->m_Size; return S; }();
                                                const std::uint32_t     OriginalAddress     = std::alignment_of_v<tools::page>;
                                                std::uint32_t           WastedToAligment    = 0;
                                                std::uint32_t           p                   = OriginalAddress;

                                                for( auto pE : lComponetDesc )
                                                {
                                                    WastedToAligment += xcore::bits::Align(p, pE->m_Aligment) - p;
                                                    p += pE->m_Size;
                                                }

                                                return static_cast<std::uint16_t>((sizeof(tools::page) - WastedToAligment) / SizeOfOne);
                                            }()
                                        }
            , m_lComponentOffsets       { [&]() constexpr 
                                            {
                                                std::array<details::offsets, 64>      lComponentOffsets{};
                                                const std::uint32_t                   OriginalAddress     = std::alignment_of_v<tools::page>;
                                                std::uint32_t                         p                   = OriginalAddress;

                                                for (int i = 0; i < lComponetDesc.size(); i++)
                                                {
                                                    auto& D     = lComponentOffsets[i];
                                                    auto& S     = *lComponetDesc[i];
                                                    D.m_HasT1   = S.m_isDoubleBuffer;
                                                    D.m_Size    = S.m_Size;
                                                    D.m_Offset  = xcore::bits::Align( p, S.m_Aligment ) - OriginalAddress;
                                                    p          += m_EntriesPerPage * D.m_Size;
                                                }
                                                assert((p - OriginalAddress) <= sizeof(tools::page));
                                                return lComponentOffsets;
                                            }()
                                        }
            {}

            
        xforceinline entity::instance& getEntity( tools::page* Page, group::local_index LocalIndex ) const noexcept
        {
            xassert(LocalIndex.m_Value < m_EntriesPerPage );
            auto&       E       = m_lComponentOffsets[0];
            const auto  Index   = E.m_Offset + static_cast<std::uint32_t>(LocalIndex.m_Value) * E.m_Size;
            return *reinterpret_cast<entity::instance*>( &Page->m_RawData[Index] );
        }

        xforceinline void ConstructOnlyEntityComponent( tools::page* Page, group::local_index LocalIndex, entity::map_t::entry* pEntry ) const noexcept
        {
            // we don't construct the entity case here
            getEntity( Page, LocalIndex ).m_pInstance = pEntry;
        }

        xforceinline void ConstructOnlyComponent( tools::page* Page, group::local_index LocalIndex, int iComponent ) const noexcept
        {
            // we don't construct the entity case here
            xassert(iComponent!=0);
            auto&       E       = m_lComponentOffsets[iComponent];
            const auto  Index   = E.m_Offset + static_cast<std::uint32_t>(LocalIndex.m_Value) * E.m_Size;
            m_lComponentDescriptors[iComponent]->m_fnConstruct( &Page->m_RawData[Index] );
        }

        xforceinline void ConstructEntity( tools::page* Page, group::local_index LocalIndex, entity::map_t::entry* pEntry ) const noexcept
        {
            ConstructOnlyEntityComponent( Page, LocalIndex, pEntry );
            for( int i=1; i<m_nComponents; ++i )
            {
                ConstructOnlyComponent(Page,LocalIndex,i);
            }
        }

        xforceinline void DestructEntity( tools::page* Page, std::uint32_t LocalIndex ) const noexcept
        {
            // Entity component does not need any destructor
            for( int i=1; i<m_nComponents; ++i )
            {
                auto&       E       = m_lComponentOffsets[i];
                const auto  Index   = E.m_Offset + LocalIndex * E.m_Size;
                m_lComponentDescriptors[i]->m_fnDestruct( &Page->m_RawData[Index] );
            }
        }

        /*
        xforceinline constexpr const details::offsets* findMutableOffset( const entity::tmp_ref& Ref, type_guid TypeGuid ) const noexcept
        {
            for (int i = 0; i <m_nComponents; ++i)
            {
                if (m_lComponentTypeGuids[i] >= TypeGuid)
                {
                    if (m_lComponentTypeGuids[i] > TypeGuid) break;

                    const auto& Offsets = m_lComponentOffsets[i];
                    const auto  T1      = Offsets.m_HasT1 - Offsets.m_T0;
                    return &m_lComponentOffsets[i + T1];
                }
            }
            return nullptr;
        }

        xforceinline constexpr const details::offsets* findReadOnlyOffset( const entity::tmp_ref& Ref, type_guid TypeGuid ) const noexcept
        {
            for (int i = 0; i <m_nComponents; ++i)
            {
                if (m_lComponentTypeGuids[i] >= TypeGuid)
                {
                    if (m_lComponentTypeGuids[i] > TypeGuid) break;
                    const auto& Offsets = m_lComponentOffsets[i];
                    return &m_lComponentOffsets[i + Offsets.m_T0];
                }
            }
            return nullptr;
        }
        */

        const type_guid                                                                             m_Guid;
        const std::uint16_t                                                                         m_EntriesPerPage;
        const std::uint16_t                                                                         m_nComponents;
        const std::array<type_guid,                     mecs::settings::max_components_in_entity_v> m_lComponentTypeGuids;
        const std::array<const component::descriptor*,  mecs::settings::max_components_in_entity_v> m_lComponentDescriptors;
        const std::array<details::offsets,              mecs::settings::max_components_in_entity_v> m_lComponentOffsets;
    };

    //---------------------------------------------------------------------------------
    // GROUP::EVENTS
    //---------------------------------------------------------------------------------
    struct events
    {
        using created_entity    = xcore::types::make_unique< event::create_entity::event_t,           struct new_entity_tag           >;
        using deleted_entity    = xcore::types::make_unique< event::destroy_entity::event_t,          struct delete_entity_tag        >;
        using move_out_entity   = xcore::types::make_unique< event::moved_out<void>::event_t,         struct move_out_entity_tag      >;
        using move_in_entity    = xcore::types::make_unique< event::moved_in<void>::event_t,          struct move_in_entity_tag       >;

        struct updated_component
        {
            using event_t = xcore::types::make_unique< event::updated_component<void>::event_t, struct updated_component_tag    >;
            event_t                     m_Event;
            std::vector<tools::bits>    m_Bits;
        };

        created_entity              m_CreatedEntity;
        deleted_entity              m_DeletedEntity;
        move_out_entity             m_MovedOutEntity;
        move_in_entity              m_MovedInEntity;
        updated_component           m_UpdateComponent;
    };

    //---------------------------------------------------------------------------------
    // GROUP::INSTANCE
    // The contract for events sent to a group.
    //      All events sent to a group must respect the order in which the local thread
    //      Issue the command. However it can be intermix with other events sent from
    //      other threads. 
    //      Deletion of entities should happen at the memory barriers. 
    //          The Guid of an entity is clear at the end of the frame.
    //          It is the responsibility of a system to check for the validity of an entity.
    //
    // Key Commands for a group:
    //
    //  Add Entity - This appends an entity to the back of the entity list as a temporary storage.
    //               The official insertion will be done at the barrier. Here is where we will 
    //               Add the entity to the world.
    //  Del Entity - This is queued as a message and it wont execute until the memory barrier.
    //               Entities at the back of the list may be moved in the holes. 
    //               World will get updated base on the new location of the entity.
    //               The World will keep an active reference of the entity until the end of the frame.
    //               Then the world will remove the real entity. This allows temporary reference to 
    //               Entities to exists until the end of the frame. If the user needs a longer reference
    //               to an entity it should store the guid.
    //  Remove/Add Component - This will require that both groups will be locked at the same time in the memory
    //                         Barrier. One of the groups will be added in the other one will be erase.
    //                         The world will update its entity pointer at the memory barrier.
    //  getComponent - Will access the component for writing/reading. This can happen in a the system update.
    //                 However if this happens the group should be locked as read/write. Which means that if the
    //                 user gets a entity::tmp_ref it must write lock the group.
    // Other details:
    // System should specify the group which components are going to be reading and which are going to be written.
    // The memory barrier can only work when there is no reading or written systems.
    //---------------------------------------------------------------------------------
    struct instance : xcore::property::base
    {
        instance( group::guid Guid, tools::page_mgr& PageMgr, const descriptor& GroupDescriptor ) noexcept
            : m_Guid                { Guid }
            , m_PageMgr             { PageMgr }
            , m_GroupDescriptor     { GroupDescriptor }
            , m_DeleteList          { m_PageMgr }
            {}

        ~instance() noexcept
        {
            xcore::scheduler::channel Channel( xconst_universal_str("mecs::group::~instance") );
            const auto c    = m_nPages.load(std::memory_order_relaxed).m_Value;
            auto  nEntities = m_Count;
            for( std::uint16_t i=0; i<c; ++i )
            {
                auto        pPage    = m_lPages[i];
                const auto  n        = static_cast<std::uint16_t>(std::min( (int)m_GroupDescriptor.m_EntriesPerPage, (int)nEntities ));
                Channel.SubmitJob( [pPage, n, this]
                {
                    for( std::uint16_t i=0; i<n; ++i)
                    {
                        m_GroupDescriptor.DestructEntity( pPage, i );
                    }
                    
                    m_PageMgr.FreePage( pPage );
                });

                nEntities -= m_GroupDescriptor.m_EntriesPerPage;
            }
            Channel.join();
        }

        constexpr   auto        size()                  const   noexcept { return m_Count; }
        inline      auto        newSize()               const   noexcept { return m_NewCount.load(std::memory_order_relaxed); }

        /*
        xforceinline
        void* findComponent(const entity::tmp_ref& Ref, type_guid TypeGuid) noexcept
        {
            if( auto pOffset = findMutableOffset(Ref,TypeGuid); pOffset )
            {
                const auto  Index   = pOffset->m_Offset + Ref.m_LocalIndex.m_Value * pOffset->m_Size;
                std::byte*  p       = &reinterpret_cast<std::byte*>(m_lPages[Ref.m_PageIndex.m_Value])[Index];
                return p;
            }

            return nullptr;
        }

        xforceinline
        void* findComponent(const entity::tmp_ref& Ref, type_guid TypeGuid) const noexcept
        {
            if( auto pOffset = findReadOnlyOffset(Ref,TypeGuid); pOffset )
            {
                const auto  Index   = pOffset->m_Offset + Ref.m_LocalIndex.m_Value * pOffset->m_Size;
                std::byte*  p       = &reinterpret_cast<std::byte*>(m_lPages[Ref.m_PageIndex.m_Value])[Index];
                return p;
            }

            return nullptr;
        }
        */

        xforceinline
        void* findComponentMutable(const entity::tmp_ref& Ref, type_guid TypeGuid) noexcept
        {
            if( auto pOffset = findMutableOffset(Ref,TypeGuid); pOffset )
            {
                const auto  Index   = pOffset->m_Offset + Ref.m_LocalIndex.m_Value * pOffset->m_Size;
                std::byte*  p       = &reinterpret_cast<std::byte*>(m_lPages[Ref.m_PageIndex.m_Value])[Index];
                return p;
            }

            return nullptr;
        }

        xforceinline
        void* findComponentReadOnly(const entity::tmp_ref& Ref, type_guid TypeGuid) const noexcept
        {
            if( auto pOffset = findReadOnlyOffset(Ref,TypeGuid); pOffset )
            {
                const auto  Index   = pOffset->m_Offset + Ref.m_LocalIndex.m_Value * pOffset->m_Size;
                std::byte*  p       = &reinterpret_cast<std::byte*>(m_lPages[Ref.m_PageIndex.m_Value])[Index];
                return p;
            }

            return nullptr;
        }

        xforceinline constexpr const details::offsets* findMutableOffset( const entity::tmp_ref& Ref, type_guid TypeGuid ) const noexcept
        {
            for (std::uint16_t i = 0; i <m_GroupDescriptor.m_nComponents; ++i)
            {
                if (m_GroupDescriptor.m_lComponentTypeGuids[i] >= TypeGuid)
                {
                    if (m_GroupDescriptor.m_lComponentTypeGuids[i] > TypeGuid) break;

                    const auto& Offsets = m_GroupDescriptor.m_lComponentOffsets[i];
                    const auto  T1      = Offsets.m_HasT1 - static_cast<std::uint16_t>((m_DoubleBufferInfo.m_StateBits>>i)&1);
                    return &m_GroupDescriptor.m_lComponentOffsets[i + T1];
                }
            }
            return nullptr;
        }

        xforceinline constexpr const details::offsets* findReadOnlyOffset( const entity::tmp_ref& Ref, type_guid TypeGuid ) const noexcept
        {
            for (std::uint16_t i = 0; i <m_GroupDescriptor.m_nComponents; ++i)
            {
                if (m_GroupDescriptor.m_lComponentTypeGuids[i] >= TypeGuid)
                {
                    if (m_GroupDescriptor.m_lComponentTypeGuids[i] > TypeGuid) break;
                    const auto& Offsets = m_GroupDescriptor.m_lComponentOffsets[i];
                    const auto  T0      = static_cast<std::uint16_t>((m_DoubleBufferInfo.m_StateBits>>i)&1);
                    return &m_GroupDescriptor.m_lComponentOffsets[i + T0];
                }
            }
            return nullptr;
        }


        entity::tmp_ref AllocEntity( entity::map_t::entry* pMapEntry ) noexcept
        {
            const auto          Index           = m_Count + m_NewCount++;
            const local_index   RelativeIndex   { Index % m_GroupDescriptor.m_EntriesPerPage };
            const page_index    PageIndex       { static_cast<std::uint16_t>(Index / m_GroupDescriptor.m_EntriesPerPage) };

            do
            {
                const auto nPages   = m_nPages.load(std::memory_order_relaxed);

                if( PageIndex < nPages )
                {
                    // Create entry here
                    m_GroupDescriptor.ConstructEntity( m_lPages[PageIndex.m_Value], RelativeIndex, pMapEntry );
                    break;
                }
                else if ( RelativeIndex.m_Value == 0 )
                {
                    if( nPages == PageIndex )
                    {
                        xassert( nPages.m_Value != 0xffff );
                        xassert( nPages == PageIndex );
                        xassert(m_nPages.load() == nPages);
                        xassert(m_nPages.load() == PageIndex );

                        m_lPages[nPages.m_Value] = m_PageMgr.AllocPage();
                        m_nPages.store(page_index{static_cast<std::uint16_t>(nPages.m_Value+1)}, std::memory_order_relaxed );
                        m_GroupDescriptor.ConstructEntity( m_lPages[PageIndex.m_Value], RelativeIndex, pMapEntry );
                        break;
                    }
                }
                else
                {
                    // We will have to wait...
                    XCORE_PERF_ZONE_SCOPED_NC( "CreateEntity::Wasting Time", tracy::Color::ColorType::Red )
                    XCORE_CMD_PROFILER( while (PageIndex >= m_nPages.load(std::memory_order_relaxed)); )
                }

            } while (true);

            // Set officially the value for the entity
            return { this, local_index{RelativeIndex}, page_index{PageIndex} };
        }

        xforceinline
        void DeleteEntity( entity::instance& Entity ) noexcept
        {
            assert(Entity.isZombie() == false);

            auto& Entry         = m_DeleteList.append();
            auto TmpRef         = Entity.getTmpRef();
            Entry.m_LocalIndex  = TmpRef.m_LocalIndex;
            Entry.m_PageIndex   = TmpRef.m_PageIndex;
                    
            // Mark entity as dead/zombie
            reinterpret_cast<std::size_t&>(Entity.m_pInstance) |= 1;
        }

        void DeletePendingEntries( void ) noexcept
        {
            XCORE_PERF_ZONE_SCOPED_N("mecs::DeletePendingEntries")
            std::uint32_t   nDelete     = m_DeleteList.size();
            const auto      WaterMark   = m_Count - nDelete;

            // Are we erasing everything?
            if( WaterMark == 0 )
            {
                local_index     I{0};
                page_index      P{0};
                while( nDelete-- )
                {
                    m_GroupDescriptor.DestructEntity( m_lPages[P.m_Value], I.m_Value );
                    I.m_Value++;
                    if( I.m_Value == m_GroupDescriptor.m_EntriesPerPage )
                    {
                        P.m_Value++;
                        I.m_Value = 0;
                    }
                }

                xassert( (m_GroupDescriptor.m_EntriesPerPage * P.m_Value + I.m_Value ) == m_Count );
            }
            else
            {
                std::uint16_t   iTopPage    = static_cast<std::uint16_t>(m_Count/m_GroupDescriptor.m_EntriesPerPage);
                std::uint16_t   iTopIndex   = static_cast<std::uint16_t>(m_Count%m_GroupDescriptor.m_EntriesPerPage);
                xassert( (iTopPage * m_GroupDescriptor.m_EntriesPerPage + iTopIndex) == m_Count );
                xassert( (static_cast<std::uint32_t>(iTopPage) * m_GroupDescriptor.m_EntriesPerPage + iTopIndex) >= WaterMark );

                auto ProcessFromTheTop = [&]
                {
                    do  
                    {
                        xassert_block_basic()
                        {
                            auto Index = (static_cast<std::uint32_t>(iTopPage) * m_GroupDescriptor.m_EntriesPerPage + iTopIndex);
                            xassert(Index >= WaterMark );
                        }

                        iTopIndex--;
                        if( iTopIndex == 0xffff )
                        {
                            xassert(iTopPage);
                            iTopPage--;
                            iTopIndex = m_GroupDescriptor.m_EntriesPerPage-1;
                        }

                        if( false == m_GroupDescriptor.getEntity( m_lPages[iTopPage], local_index{iTopIndex} ).isZombie() ) break;

                        m_GroupDescriptor.DestructEntity( m_lPages[iTopPage], iTopIndex );
                        --nDelete;

                    } while( nDelete );
                };

                // Find the real top
                ProcessFromTheTop();

                for( auto E : m_DeleteList )
                {
                    if( E.m_PageIndex.m_Value >= iTopPage )
                    {
                        if( E.m_PageIndex.m_Value > iTopPage || E.m_LocalIndex.m_Value > iTopIndex ) 
                        {
                            xassert( m_GroupDescriptor.getEntity( m_lPages[E.m_PageIndex.m_Value], E.m_LocalIndex ).isZombie() );
                            continue;
                        }
                    }

                    if( nDelete-- == 0 ) break;
                    xassert( (static_cast<std::uint32_t>(iTopPage) * m_GroupDescriptor.m_EntriesPerPage + iTopIndex) >= WaterMark );

                    // This entity is completely gone at this point
                    xassert( m_GroupDescriptor.getEntity( m_lPages[E.m_PageIndex.m_Value], E.m_LocalIndex ).isZombie() );
                    m_GroupDescriptor.DestructEntity( m_lPages[E.m_PageIndex.m_Value], E.m_LocalIndex.m_Value );

                    // Move components to new location
                    for( int i=0; i<m_GroupDescriptor.m_nComponents; i++ )
                    {
                        const auto& ComponentDetails = m_GroupDescriptor.m_lComponentOffsets[i];
                        auto*       pDestinaltion    = &m_lPages[E.m_PageIndex.m_Value]->m_RawData[ ComponentDetails.m_Offset + E.m_LocalIndex.m_Value * ComponentDetails.m_Size ];
                        const auto* pSource          = &m_lPages[iTopPage]             ->m_RawData[ ComponentDetails.m_Offset + iTopIndex              * ComponentDetails.m_Size ];
                        std::memcpy( pDestinaltion, pSource, ComponentDetails.m_Size );
                    }

                    // Update the moved entity reference
                    {
                        const auto& ComponentDetails = m_GroupDescriptor.m_lComponentOffsets[0];
                        const auto& MovedEntity      = m_GroupDescriptor.getEntity( m_lPages[E.m_PageIndex.m_Value], E.m_LocalIndex );
                        assert( MovedEntity.isZombie() == false );
                        MovedEntity.m_pInstance->m_Value.m_LocalIndex = E.m_LocalIndex;
                        MovedEntity.m_pInstance->m_Value.m_PageIndex  = E.m_PageIndex;
                    }

                    // Update the top count
                    ProcessFromTheTop();
                }

                xassert_block_basic()
                {
                    local_index     I{0};
                    page_index      P{0};
                    for( auto E = WaterMark; E--; )
                    {
                        xassert( false == m_GroupDescriptor.getEntity( m_lPages[P.m_Value], I ).isZombie() );
                        I.m_Value++;
                        if( I.m_Value == m_GroupDescriptor.m_EntriesPerPage )
                        {
                            P.m_Value++;
                            I.m_Value = 0;
                        }
                    }
                }
            }

            // Clear all the list
            m_DeleteList.clear();

            // Set the new count of entries
            m_Count = WaterMark;
        }

        void Start( void ) noexcept
        {
            //
            // Officially add all the new entries
            //
            m_Count += m_NewCount.load(std::memory_order_relaxed);
            m_NewCount.store(0,std::memory_order_relaxed );

            //
            // Update double buffer state if we have any
            //
            std::uint16_t DirtyBits=0;
            for (std::uint16_t i = 0; i <m_GroupDescriptor.m_nComponents; ++i)
            {
                const auto& Offsets = m_GroupDescriptor.m_lComponentOffsets[i];
                DirtyBits |= (static_cast<std::uint16_t>(Offsets.m_HasT1) << i);
            }
            m_DoubleBufferInfo.m_StateBits ^= DirtyBits;
            m_DoubleBufferInfo.m_DirtyBits.store(0,std::memory_order_relaxed);

            //
            // Delete all pending entities
            //
            if( m_DeleteList.size() )
            {
                DeletePendingEntries();

                //
                // Free any pages that we are not using any more
                //
                {
                    const std::uint16_t nPages      = m_nPages.load( std::memory_order_relaxed ).m_Value;
                    const std::uint16_t NeededPages = (m_Count / m_GroupDescriptor.m_EntriesPerPage) + 1u;

                    xassert( nPages >= NeededPages );
                    for( auto i=NeededPages; i<nPages; i++ )
                    {
                        m_PageMgr.FreePage( m_lPages[i] );
                    }

                    m_nPages.store( group::page_index{NeededPages}, std::memory_order_relaxed );
                }
            }
        }

        void MemoryBarrierSync( sync_point::instance& SyncPoint ) noexcept
        {
            XCORE_PERF_ZONE_SCOPED_N("mecs::MemoryBarrierSync")

            //
            // Avoid updating multiple times for a given sync_point done
            //
            if( m_pLastSyncPoint == &SyncPoint && m_LastTick == SyncPoint.m_Tick )
                return;

            // Save state of previous update to make sure no one ask us to update twice
            m_pLastSyncPoint = &SyncPoint;
            m_LastTick       = SyncPoint.m_Tick;

            //
            // Make sure to lock for writing
            //
            xcore::lock::scope Lock(m_SemaphoreLock);

            //
            // Officially add all the new entries
            //
            m_Count += m_NewCount.load(std::memory_order_relaxed);
            m_NewCount.store(0,std::memory_order_relaxed );

            //
            // Update double buffer state if we have any
            //
            m_DoubleBufferInfo.m_StateBits ^= m_DoubleBufferInfo.m_DirtyBits.load(std::memory_order_relaxed);
            m_DoubleBufferInfo.m_DirtyBits.store(0,std::memory_order_relaxed);

            //
            // Delete all pending entities
            //
            if( m_DeleteList.size() )
            {
                DeletePendingEntries();

                //
                // Free any pages that we are not using any more
                //
                {
                    const std::uint16_t nPages      = m_nPages.load( std::memory_order_relaxed ).m_Value;
                    const std::uint16_t NeededPages = (m_Count / m_GroupDescriptor.m_EntriesPerPage) + 1u;

                    xassert( nPages >= NeededPages );
                    for( auto i=NeededPages; i<nPages; i++ )
                    {
                        m_PageMgr.FreePage( m_lPages[i] );
                    }

                    m_nPages.store( group::page_index{NeededPages}, std::memory_order_relaxed );
                }
            }
        }

        constexpr static std::size_t max_pages_v = 1024*32;
        using page_list = xcore::array<tools::page*, max_pages_v>;

        struct component_lock_entry
        {
            system::guid    m_gSystem{0u,0u};   // Which system has this component locked for writing
            std::uint8_t    m_nReaders{0};              // How many systems have this group locked for reading
        };

        struct double_buffer_info
        {
            std::uint64_t               m_StateBits{0};         // Tells which of the two columns is T0. Index is same as the component.
            std::atomic<std::uint64_t>  m_DirtyBits{0};         // Tells which bits in the StateBits need to change state.
        };
        using component_lock_list_t = xcore::lock::object<std::array<component_lock_entry,64>,xcore::lock::semaphore>;

        group::guid                                                                 m_Guid;
        tools::page_mgr&                                                            m_PageMgr;
        tools::pool<details::delete_entity,max_pages_v>                             m_DeleteList;
 //       std::array<std::uint64_t,4>                                                 m_SubgroupKeys      {};
        xcore::lock::semaphore                                                      m_SemaphoreLock     {};
        double_buffer_info                                                          m_DoubleBufferInfo  {};
        std::atomic<std::uint32_t>                                                  m_NewCount          { 0 };
        std::uint32_t                                                               m_Count             { 0 };
        std::atomic<group::page_index>                                              m_nPages            { group::page_index{0} };
        page_list                                                                   m_lPages            {};
        component_lock_list_t                                                       m_lComponentLocks   {};
        events                                                                      m_Events            {};
        const group::descriptor&                                                    m_GroupDescriptor;
        sync_point::instance*                                                       m_pLastSyncPoint    { nullptr };
        std::uint32_t                                                               m_LastTick          { 0 };
        void*                                                                       m_pDataBaseRef      { nullptr };

        property_vtable();
    };
}

property_begin_name( mecs::component::group::instance, "MECS-Group" )
{
    property_var_fnbegin( "EntityCount", int )
    {
        if( isRead ) InOut        = static_cast<int>(Self.m_Count);
    }
    property_var_fnend()
        .Flags( xcore::property::flags::SHOW_READONLY | xcore::property::flags::DONTSAVE )

    , property_var_fnbegin( "PageCount", int )
    {
        if( isRead ) InOut        = static_cast<int>( Self.m_nPages.load(std::memory_order_relaxed).m_Value );
    }
    property_var_fnend()
        .Flags( xcore::property::flags::SHOW_READONLY | xcore::property::flags::DONTSAVE )
    , property_var_fnbegin( "Memory", string_t )
    {
        if( isRead )
        {
            auto v = static_cast<float>( Self.m_nPages.load(std::memory_order_relaxed).m_Value * sizeof(mecs::tools::page)/1000.0f );

            // Mega bytes
            if(v>=1000000.0f)   xcore::string::sprintf(InOut, "%.2fGb", v/1000000.0f);
            else if(v>=1000.0f) xcore::string::sprintf(InOut, "%.2fMb", v/1000.0f);
            else                xcore::string::sprintf(InOut, "%.2fKb", v);
        }
    }
    property_var_fnend()
        .Flags( xcore::property::flags::SHOW_READONLY | xcore::property::flags::DONTSAVE )
        .EDStyle(property::edstyle<string_t>::Button())

    , property_scope_begin("Events")
    {
        property_var_fnbegin( "CreateEntity", int )
        {
            if( isRead ) InOut        = static_cast<int>( Self.m_Events.m_CreatedEntity.m_lDelegates.size() );
        }
        property_var_fnend()
        
        , property_var_fnbegin( "DeleteEntity", int )
        {
            if( isRead ) InOut        = static_cast<int>( Self.m_Events.m_DeletedEntity.m_lDelegates.size() );
        }
        property_var_fnend()

        , property_var_fnbegin( "MoveInEntity", int )
        {
            if( isRead ) InOut        = static_cast<int>( Self.m_Events.m_MovedInEntity.m_lDelegates.size() );
        }
        property_var_fnend()

        , property_var_fnbegin( "MoveOutEntity", int )
        {
            if( isRead ) InOut        = static_cast<int>( Self.m_Events.m_MovedOutEntity.m_lDelegates.size() );
        }
        property_var_fnend()

        , property_var_fnbegin( "UpdateComponent", int )
        {
            if( isRead ) InOut        = static_cast<int>( Self.m_Events.m_UpdateComponent.m_Event.m_lDelegates.size() );
        }
        property_var_fnend()

    }
    property_scope_end()
        .Flags( xcore::property::flags::SHOW_READONLY | xcore::property::flags::DONTSAVE )
}
property_vend_h(mecs::component::group::instance);

