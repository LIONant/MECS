namespace mecs::system
{
    //---------------------------------------------------------------------------------
    // SYSTEM::DETAILS
    //---------------------------------------------------------------------------------
    namespace details
    {
        //-----------------------------------------------------------------------------------------
        namespace callfunwith_details
        {
            //-----------------------------------------------------------------------------------------
            template< typename T_ACCESS, typename T_TMP_REF, typename T_CALLBACK, typename...T_ARG, typename...T_EXTRA_ARGS > xforceinline constexpr 
            void CallFunctionByAccess( T_TMP_REF& Ref, T_CALLBACK&& Function, std::tuple<T_ARG...>*, T_EXTRA_ARGS&&...ExtraArgs ) noexcept
            {
                Function
                (
                        std::forward<T_EXTRA_ARGS>(ExtraArgs) ...
                    ,   xcore::types::lvalue( T_ACCESS{ std::tuple<T_ARG&...>{const_cast<std::decay_t<T_ARG>&>(Ref.getComponent<std::decay_t<T_ARG>>()) ...} } )
                );
            }

            //-----------------------------------------------------------------------------------------
            template< typename T_TMP_REF, typename T_CALLBACK, typename...T_ARGS, typename...T_EXTRA_ARGS > xforceinline constexpr
            void CallFunctionByArgs( T_TMP_REF& Ref, T_CALLBACK&& Function, std::tuple<T_ARGS...>*, T_EXTRA_ARGS&&...ExtraArgs ) noexcept
            {
                constexpr static bool tentative_v = (std::is_pointer_v<T_ARGS> || ...);

                if constexpr (tentative_v)
                {
                    Function
                    (
                            std::forward<T_EXTRA_ARGS>(ExtraArgs) ...
                        ,   ([&]()
                            {
                                if( std::is_pointer<T_ARGS> )
                                {
                                    return Ref.findComponent<std::remove_pointer_t<T_ARGS>>();
                                }
                                else
                                {
                                    return Ref.getComponent<std::remove_reference_t<T_ARGS>>();
                                }
                            }()) ...
                    );
                }
                else
                {
                    Function
                    (
                            std::forward<T_EXTRA_ARGS>(ExtraArgs) ...
                        ,   Ref.getComponent<std::remove_reference_t<T_ARGS>>() ...
                    );
                }
            }
        }

        template< typename T_CALLBACK, typename T_TMP_REF, typename...T_EXTRA_ARGS > xforceinline constexpr
        void CallFunctionWithComponents( T_TMP_REF& Ref, T_CALLBACK&& Function, T_EXTRA_ARGS&&...ExtraArgs ) noexcept
        {
            if constexpr ( xcore::function::traits<T_CALLBACK>::arg_count_v == 0 )
            {
                Function( std::forward<T_EXTRA_ARGS>(ExtraArgs) ... );
            }
            else
            {
                static_assert( xcore::function::traits<T_CALLBACK>::arg_count_v >= 1 );
                using first_arg_t = std::decay_t<typename xcore::function::traits<T_CALLBACK>::template arg_t<0>>;

                if constexpr ( std::is_same_v<entity::tmp_ref, first_arg_t> )
                {
                    Function( Ref, std::forward<T_EXTRA_ARGS>(ExtraArgs) ... );
                }
                else if constexpr ( xcore::types::is_specialized_v< query::access, first_arg_t > )
                {
                    static_assert( xcore::function::traits<T_CALLBACK>::arg_count_v == 1 );
                    callfunwith_details::CallFunctionByAccess<first_arg_t>
                    ( 
                          Ref
                        , std::forward<T_CALLBACK>(Function)
                        , reinterpret_cast<typename first_arg_t::tuple_t*>(nullptr)
                        , std::forward<T_EXTRA_ARGS>(ExtraArgs) ... 
                    );
                }
                else
                {
                    using args_tuple_t = typename xcore::function::traits<T_CALLBACK>::args_tuple;
                    callfunwith_details::CallFunctionByArgs
                    ( 
                          Ref
                        , std::forward<T_CALLBACK>(Function)
                        , reinterpret_cast<args_tuple_t*>(nullptr)
                        , std::forward<T_EXTRA_ARGS>(ExtraArgs) ... 
                    );
                }
            }
        }
        
        //-----------------------------------------------------------------------------------------

        bool    LockQueryComponents     ( system::instance& System, query::instance& Query ) noexcept;
        void    UnlockQueryComponents   ( system::instance& System, query::instance& Query ) noexcept;

        //-----------------------------------------------------------------------------------------

        template< typename T_SYSTEM, typename T_FUNCTOR >
        struct params
        {
            using functor_t = T_FUNCTOR;

            T_SYSTEM&                       m_System;
            T_FUNCTOR&                      m_Functor;
            component::group::instance&     m_Group;
            query::result_entry&            m_ResultEntry;
            int                             m_nEntriesPerJob;
            std::array<std::uint8_t,32>     m_Delegates;
            std::uint8_t                    m_nDelegates;
        };

        //-----------------------------------------------------------------------------------------
        template< typename T_PARAMS, typename...T_ARGS >
        void ProcessGroup( T_PARAMS& Params, xcore::scheduler::channel& Channel ) noexcept
        {
            using                       function_t      = query::system_function<T_PARAMS::functor_t>;
            using                       pool_tuple      = typename std::tuple< std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*... >;
            static constexpr auto       writing_v       = ( (std::is_same_v< T_ARGS, const T_ARGS > == false) || ... );  // check to see if we have non cost types
            static constexpr auto       has_pointers_v  = ( std::is_pointer_v< T_ARGS > || ... );

            if constexpr (writing_v) if( Params.m_Group.m_Events.m_UpdateComponent.m_Event.hasSubscribers() )
            {
                static const tools::bits Bits{[&]
                {
                    tools::bits Bits{nullptr};
                    ( ((std::is_same_v< T_ARGS, const T_ARGS > == false) 
                        ?   (Bits.AddBit( Params.m_System.m_World.m_mapComponentDescriptor.get( std::remove_pointer_t<std::remove_reference_t<T_ARGS>>::type_guid_v )->m_BitNumber ),1)
                        :   0), ... );
                    return Bits;
                }()};

                for( int end = static_cast<int>(Params.m_Group.m_Events.m_UpdateComponent.m_Bits.size()), i = 0; i!= end; ++i )
                {
                    if (Params.m_Group.m_Events.m_UpdateComponent.m_Bits[i].isMatchingBits(Bits) )
                        Params.m_Delegates[Params.m_nDelegates++] = static_cast<std::uint8_t>(i);
                }
            }

            auto    TotalEntries    = Params.m_Group.m_Count;
            auto    Index           = 0;
            xassert( TotalEntries );
            do 
            {
                const auto  nEntities  = std::min( (int)Params.m_nEntriesPerJob, (int)TotalEntries );

                Channel.SubmitJob( [ &Params, Index, nEntities ]
                {
                    XCORE_PERF_ZONE_SCOPED_N( "ProcessGroup" )

                    const auto  epp    = static_cast<std::int32_t>(Params.m_Group.m_GroupDescriptor.m_EntriesPerPage);
                    auto        I      { static_cast<std::uint16_t>( Index % epp) };
                    auto        P      { static_cast<std::uint16_t>( Index / epp) };
                    auto        TotalRemainingForThisPage   = std::min( nEntities, (epp - I) );
                    auto        TotalRemaining              = nEntities - TotalRemainingForThisPage;

                    for(;;) 
                    {
                        const auto Pools  = [&]() constexpr noexcept
                        {
                            if constexpr( has_pointers_v )
                            {
                                return pool_tuple
                                { 
                                    reinterpret_cast<std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*>
                                    ( 
                                        Params.m_ResultEntry.m_lRemaps[ xcore::types::tuple_t2i_v< T_ARGS, function_t::args_t > ] == 0xffff ? nullptr :
                                            &Params.m_Group.m_lPages[P]->m_RawData
                                            [ 
                                                Params.m_ResultEntry.m_lRemaps[ xcore::types::tuple_t2i_v< T_ARGS, function_t::args_t > ]
                                            ]
                                    ) ... 
                                };
                            }
                            else
                            {
                                return pool_tuple
                                { 
                                    reinterpret_cast<std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*>
                                    ( &Params.m_Group.m_lPages[P]->m_RawData
                                        [ 
                                            Params.m_ResultEntry.m_lRemaps[ xcore::types::tuple_t2i_v< T_ARGS, function_t::args_t > ]
                                        ]
                                    ) ... 
                                };
                            }
                        }();

                        // if we have any component as mutable and we have people which we need to notify then lets run this loop type
                        if ( writing_v && Params.m_nDelegates )
                        {
                            auto pEntityPool  = reinterpret_cast<entity::instance*>( &Params.m_Group.m_lPages[P]->m_RawData[ Params.m_Group.m_GroupDescriptor.m_lComponentOffsets[0].m_Offset ] );

                            for( const auto End = I + TotalRemainingForThisPage; I<End; ++I )
                            {
                                if constexpr ( has_pointers_v )
                                {
                                    Params.m_Functor
                                    ( 
                                        ([&]() constexpr noexcept -> T_ARGS
                                        {
                                            if constexpr( std::is_pointer_v<T_ARGS> )
                                            {
                                                auto p = std::get<T_ARGS>(Pools);
                                                return p?&p[I]:p;
                                            }
                                            else
                                            {
                                                return std::get
                                                <
                                                    std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*
                                                >(Pools)[I];
                                            }
                                        }()) ...
                                    );
                                }
                                else
                                {
                                    Params.m_Functor
                                    ( 
                                        std::get
                                        <
                                            std::remove_reference_t<T_ARGS>*
                                        >(Pools)[I] ... 
                                    );
                                }

                                for( std::uint8_t end = Params.m_nDelegates, i=0u; i!=end; ++i )
                                    Params.m_Group.m_Events.m_UpdateComponent.m_Event.m_lDelegates[Params.m_Delegates[i]]( pEntityPool[I], Params.m_System );
                            }
                        }
                        else
                        {
                            for( const auto End = I + TotalRemainingForThisPage; I<End; ++I )
                            {
                                if constexpr ( has_pointers_v )
                                {
                                    Params.m_Functor
                                    ( 
                                        ([&]() constexpr noexcept -> T_ARGS
                                        {
                                            if constexpr( std::is_pointer_v<T_ARGS> )
                                            {
                                                auto p = std::get<T_ARGS>(Pools);
                                                return p?&p[I]:p;
                                            }
                                            else
                                            {
                                                return std::get
                                                <
                                                    std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*
                                                >(Pools)[I];
                                            }
                                        }()) ...
                                    );
                                }
                                else
                                {
                                    Params.m_Functor
                                    ( 
                                        std::get
                                        <
                                            std::remove_reference_t<T_ARGS>*
                                        >(Pools)[I] ... 
                                    );
                                }

                            }
                        }

                        // are we done?
                        if(TotalRemaining==0) break;

                        // get ready for next page
                        P++;
                        I                         = 0;
                        TotalRemainingForThisPage = std::min( TotalRemaining, epp );
                        TotalRemaining           -= TotalRemainingForThisPage;
                    }

                });

                Index        += nEntities;
                TotalEntries -= nEntities;

            } while( TotalEntries );
        }
        
        //-----------------------------------------------------------------------------------------
        template< typename T_PARAMS, typename T_ACCESS, typename...T_ARGS >
        void ProcessGroupByAccess( T_PARAMS& Params, xcore::scheduler::channel& Channel, std::tuple<T_ARGS...>* ) noexcept
        {
            using                       pool_tuple      = typename std::tuple< std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*... >;
            static constexpr auto       writing_v       = ( (std::is_same_v< T_ARGS, const T_ARGS > == false) || ... );  // check to see if we have non cost types
            static constexpr auto       has_pointers_v  = ( std::is_pointer_v< T_ARGS > || ... );

            if constexpr (writing_v) if( Params.m_Group.m_Events.m_UpdateComponent.m_Event.hasSubscribers() )
            {
                static const tools::bits Bits{[&]
                {
                    tools::bits Bits{nullptr};
                    ( ( (std::is_same_v< T_ARGS, const T_ARGS > == false) 
                        ? (Bits.AddBit( Params.m_System.m_World.m_mapComponentDescriptor.get( xcore::types::decay_full_t<T_ARGS>::type_guid_v )->m_BitNumber ),1)
                        : 0), ... );
                    return Bits;
                }()};

                for( int end = static_cast<int>(Params.m_Group.m_Events.m_UpdateComponent.m_Bits.size()), i = 0; i!= end; ++i )
                {
                    if (Params.m_Group.m_Events.m_UpdateComponent.m_Bits[i].isMatchingBits(Bits) )
                        Params.m_Delegates[Params.m_nDelegates++] = static_cast<std::uint8_t>(i);
                }
            }

            int TotalEntries    = Params.m_Group.m_Count;
            int Index           = 0;
            xassert( TotalEntries );
            do 
            {
                const auto  nEntities  = std::min( (int)Params.m_nEntriesPerJob, (int)TotalEntries );

                Channel.SubmitJob( [ &Params, Index, nEntities ]
                {
                    XCORE_PERF_ZONE_SCOPED_N( "ProcessGroupByAccess" )

                    const auto  epp    = static_cast<std::int32_t>(Params.m_Group.m_GroupDescriptor.m_EntriesPerPage);
                    auto        I      { static_cast<std::uint16_t>( Index % epp) };
                    auto        P      { static_cast<std::uint16_t>( Index / epp) };
                    auto        TotalRemainingForThisPage   = std::min( nEntities, (epp - I) );
                    auto        TotalRemaining              = nEntities - TotalRemainingForThisPage;

                    for(;;) 
                    {
                        const auto Pools  = [&]() constexpr noexcept
                        {
                            if constexpr( has_pointers_v )
                            {
                                return pool_tuple
                                { 
                                    reinterpret_cast<std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*>
                                    ( 
                                        Params.m_ResultEntry.m_lRemaps[ xcore::types::tuple_t2i_v< T_ARGS, typename T_ACCESS::tuple_t > ] == 0xffff ? nullptr :
                                            &Params.m_Group.m_lPages[P]->m_RawData
                                            [ 
                                                Params.m_ResultEntry.m_lRemaps[ xcore::types::tuple_t2i_v< T_ARGS, typename T_ACCESS::tuple_t > ] 
                                            ]
                                    ) ... 
                                };
                            }
                            else
                            {
                                return pool_tuple
                                { 
                                    reinterpret_cast<std::remove_pointer_t<std::remove_reference_t<T_ARGS>>*>
                                    ( &Params.m_Group.m_lPages[P]->m_RawData
                                        [ 
                                            Params.m_ResultEntry.m_lRemaps[ xcore::types::tuple_t2i_v< T_ARGS, typename T_ACCESS::tuple_t > ] 
                                        ]
                                    ) ... 
                                };
                            }
                        }();
                        

                        // if we have any component as mutable and we have people which we need to notify then lets run this loop type
                        if ( writing_v && Params.m_nDelegates )
                        {
                            auto pEntityPool  = reinterpret_cast<entity::instance*>( &Params.m_Group.m_lPages[P]->m_RawData[ Params.m_Group.m_GroupDescriptor.m_lComponentOffsets[0].m_Offset ] );

                            if constexpr ( has_pointers_v )
                            {
                                // execute each entry
                                for( const auto End = I + TotalRemainingForThisPage; I<End; ++I )
                                {
                                    Params.m_Functor
                                    (
                                        xcore::types::lvalue
                                        (
                                            T_ACCESS
                                            {{
                                                ([&]() constexpr noexcept -> T_ARGS
                                                {
                                                    if constexpr( std::is_pointer_v<T_ARGS> )
                                                    {
                                                        auto p = std::get<T_ARGS>(Pools);
                                                        return p?&p[I]:p;
                                                    }
                                                    else
                                                    {
                                                        return std::get
                                                        <
                                                            std::remove_reference_t<T_ARGS>*
                                                        >(Pools)[I];
                                                    }
                                                }()) ...
                                            }}
                                        )
                                    );

                                    for( std::uint8_t end = Params.m_nDelegates, i=0u; i!=end; ++i )
                                        Params.m_Group.m_Events.m_UpdateComponent.m_Event.m_lDelegates[Params.m_Delegates[i]]( pEntityPool[I], Params.m_System );
                                }
                            }
                            else
                            {
                                // execute each entry
                                for( const auto End = I + TotalRemainingForThisPage; I<End; ++I )
                                {
                                    Params.m_Functor
                                    (
                                        xcore::types::lvalue
                                        (
                                            T_ACCESS
                                            {{
                                                std::get
                                                <
                                                    std::remove_reference_t<T_ARGS>*
                                                >(Pools)[I] ... 
                                            }}
                                        )
                                    );

                                    for( std::uint8_t end = Params.m_nDelegates, i=0u; i!=end; ++i )
                                        Params.m_Group.m_Events.m_UpdateComponent.m_Event.m_lDelegates[Params.m_Delegates[i]]( pEntityPool[I], Params.m_System );
                                }
                            }
                        }
                        else
                        {
                            if constexpr ( has_pointers_v )
                            {
                                // execute each entry
                                for( const auto End = I + TotalRemainingForThisPage; I<End; ++I )
                                {
                                    Params.m_Functor
                                    (
                                        xcore::types::lvalue
                                        (
                                            T_ACCESS
                                            {{
                                                ([&]() constexpr noexcept -> T_ARGS
                                                {
                                                    if constexpr( std::is_pointer_v<T_ARGS> )
                                                    {
                                                        auto p = std::get<T_ARGS>(Pools);
                                                        return p?&p[I]:p;
                                                    }
                                                    else
                                                    {
                                                        return std::get
                                                        <
                                                            std::remove_reference_t<T_ARGS>*
                                                        >(Pools)[I];
                                                    }
                                                }()) ...
                                            }}
                                        )
                                    );
                                }
                            }
                            else
                            {
                                // execute each entry
                                for( const auto End = I + TotalRemainingForThisPage; I<End; ++I )
                                {
                                    Params.m_Functor
                                    (
                                        xcore::types::lvalue
                                        (
                                            T_ACCESS
                                            {{
                                                std::get
                                                <
                                                    std::remove_reference_t<T_ARGS>*
                                                >(Pools)[I] ... 
                                            }}
                                        )
                                    );
                                }
                            }
                        }

                        // are we done?
                        if(TotalRemaining==0) break;

                        // get ready for next page
                        P++;
                        I                         = 0;
                        TotalRemainingForThisPage = std::min( TotalRemaining, epp );
                        TotalRemaining           -= TotalRemainingForThisPage;
                    }

                });

                Index        += nEntities;
                TotalEntries -= nEntities;

            } while( TotalEntries );
        }

        //-----------------------------------------------------------------------------------------
        // SYSTEM::DETAILS::IS_BY_ACCESS
        //-----------------------------------------------------------------------------------------
        template< typename T >
        static constexpr bool is_by_access_v = xcore::types::is_specialized_v
        < 
                query::access
            ,   std::decay_t
                <
                    std::remove_reference_t
                    <
                        typename xcore::function::traits<T>::template arg_t<0>
                    >
                >
        >;

        //-----------------------------------------------------------------------------------------
        // SYSTEM::DETAILS::DO_QUERY
        //-----------------------------------------------------------------------------------------
        template
        < 
                typename    T_WORLD_INSTANCE
            ,   typename    T_CALLBACK
            ,   bool        T_IS_BY_ACCESS = is_by_access_v<T_CALLBACK>
        >
        struct do_query;

        //-----------------------------------------------------------------------------------------
        template< typename T_WORLD_INSTANCE, typename T_CALLBACK >
        struct do_query<T_WORLD_INSTANCE,T_CALLBACK,false>
        {
            constexpr xforceinline
            static void DoQuery( T_WORLD_INSTANCE& World, query::instance& Query,const query::define_data& Data ) noexcept
            {
                World.DoQuery<T_CALLBACK>( Query, Data );
            }
        };

        //-----------------------------------------------------------------------------------------
        template< typename T_WORLD_INSTANCE, typename T_CALLBACK >
        struct do_query<T_WORLD_INSTANCE,T_CALLBACK,true>
        {
            template< typename...T_ARGS >
            struct type_func;

            template< typename...T_ARGS >
            struct type_func<std::tuple<T_ARGS...>>
            {
                void operator()( T_ARGS... ) noexcept;
            };

            constexpr xforceinline
            static void DoQuery( T_WORLD_INSTANCE& World, query::instance& Query, const query::define_data& Data ) noexcept
            {
                using the_tuple = typename std::decay_t
                <
                    std::remove_reference_t
                    <
                        typename xcore::function::traits<T_CALLBACK>::template arg_t<0>
                    >
                >::tuple_t;

                World.DoQuery< type_func< the_tuple > >( Query, Data );
            }
        };

        //-----------------------------------------------------------------------------------------
        // SYSTEM::DETAILS::FOR_EACH
        //-----------------------------------------------------------------------------------------
        template< typename T_SYSTEM, std::size_t T_ENTITIES_PER_JOB, typename T_CALLBACK, bool T_IS_BY_ACCESS = is_by_access_v<T_CALLBACK> >
        struct for_each;

        //-----------------------------------------------------------------------------------------
        template< typename T_SYSTEM, std::size_t T_ENTITIES_PER_JOB, typename T_CALLBACK >
        struct for_each<T_SYSTEM, T_ENTITIES_PER_JOB,T_CALLBACK,false>
        {
            template< typename...T_ARGS >
            static const bool Process( T_SYSTEM& System, query::instance& Query, T_CALLBACK&& Functor, std::tuple<T_ARGS...>* ) noexcept
            {
                using                           function_t  = query::system_function<decltype(Functor)>;

                // If there is nothing to do then lets bail out
                if( Query.m_lResults.size() == 0 )
                    return true;

                //
                // Lock all the groups
                //
                if( false == LockQueryComponents( System, Query ) ) return false;

                //
                // Do all the groups
                //
                xcore::scheduler::channel Channel( System.getName() );
                using params_t = params<system::instance, T_CALLBACK>;
                xcore::rawarray< params_t, 32 > ParamStack;
                int                             StackPointer = 0;
                for (auto& R : Query.m_lResults )
                {
                    if( R.m_pGroup->m_Count == 0 ) continue;
                    auto& Params = *new( &ParamStack[StackPointer++] ) params_t
                    {
                            System
                        ,   Functor
                        ,   *R.m_pGroup
                        ,   R
                        ,   T_ENTITIES_PER_JOB
                    };

                    ProcessGroup<params_t, T_ARGS...>( Params, Channel );
                }
                Channel.join();

                //
                // unlock all the groups that we need to work with so that no one tries to add/remove entities
                //
                UnlockQueryComponents( System, Query );

                return true;
            }

            constexpr xforceinline
            static bool ForEach( T_SYSTEM& System, query::instance& Query, T_CALLBACK&& Functor ) noexcept
            {
                using function_t  = query::system_function<T_CALLBACK>;
                return Process( System, Query, Functor, reinterpret_cast< typename function_t::args_t* >(nullptr) );
            }
        };

        //-----------------------------------------------------------------------------------------
        template< typename T_SYSTEM, std::size_t T_ENTITIES_PER_JOB,typename T_CALLBACK >
        struct for_each<T_SYSTEM,T_ENTITIES_PER_JOB,T_CALLBACK,true>
        {
            static_assert( xcore::function::traits<T_CALLBACK>::arg_count_v == 1 );

            static bool ForEach( T_SYSTEM& System, query::instance& Query, T_CALLBACK&& Functor ) noexcept
            {
                using   access_raw_t    = typename xcore::function::traits<T_CALLBACK>::template arg_t<0>;
                using   access_t        = typename std::decay_t< std::remove_reference_t< access_raw_t >>;

                static_assert( std::is_reference_v<access_raw_t> );

                // If there is nothing to do then lets bail out
                if( Query.m_lResults.size() == 0 )
                    return true;

                //
                // Lock all the groups
                //
                if( false == details::LockQueryComponents( System, Query ) ) return false;

                //
                // Do all the groups
                //
                xcore::scheduler::channel Channel( System.getName() );
                using params_t = params<system::instance, T_CALLBACK>;
                xcore::rawarray< params_t, 32 > ParamStack;
                int                             StackPointer = 0;
                for (auto& R : Query.m_lResults )
                {
                    if( R.m_pGroup->m_Count == 0 ) continue;
                    auto& Params = *new( &ParamStack[StackPointer++] ) params_t
                    {
                            System
                        ,   Functor
                        ,   *R.m_pGroup
                        ,   R
                        ,   T_ENTITIES_PER_JOB
                    };

                    details::ProcessGroupByAccess< params_t, access_t >
                    (       Params
                        ,   Channel                        
                        ,   reinterpret_cast<typename access_t::tuple_t*>(nullptr) 
                    );
                }
                Channel.join();

                //
                // unlock all the groups that we need to work with so that no one tries to add/remove entities
                //
                details::UnlockQueryComponents( System, Query );

                return true;
            }
        };

        //-----------------------------------------------------------------------------------------

        template< typename T_CALLBACK >
        bool ForEachGroup( system::instance& System, query::instance& Query, T_CALLBACK&& Functor ) noexcept
        {
            using                                           function_t  = query::system_function<decltype(Functor)>;

            // If there is nothing to do then lets bail out
            if( Query.m_lResults.size() == 0 )
                return true;

            //
            // Lock all the groups
            //
            if( false == LockQueryComponents( System, Query ) ) return false;

            //
            // Do all the groups
            //
            xcore::scheduler::channel Channel( xconst_universal_str("ForEach") );
            for (auto& R : Query.m_lResults )
            {
                if( R.m_pGroup->m_Count == 0 ) continue;
                Functor(*R.m_pGroup);
            }
            Channel.join();

            //
            // unlock all the groups that we need to work with so that no one tries to add/remove entities
            //
            UnlockQueryComponents( System, Query );

            return true;
        }

        //-----------------------------------------------------------------------------------------

        struct cache
        {
            struct line
            {
                component::group::guid          m_Guid;
                component::group::instance*     m_pGroup;
            };
        
            xcore::lock::semaphore  m_Lock;
            std::vector<line>       m_Lines;
        };

        //-----------------------------------------------------------------------------------------
        template< typename...T_ADD_ARGS1, typename...T_SUBTRACT_ARGS2 >
        static constexpr xforceinline auto ComputeGroupGuid( std::uint64_t Value, std::tuple<T_ADD_ARGS1...>*, std::tuple<T_SUBTRACT_ARGS2...>* ) noexcept
        {
            if constexpr ( sizeof...(T_ADD_ARGS1) && sizeof...(T_SUBTRACT_ARGS2) )
            {
                return Value 
                    + (( 0u + T_ADD_ARGS1::type_guid_v.m_Value ) + ... )
                    - (( 0u + T_SUBTRACT_ARGS2::type_guid_v.m_Value ) + ... );
            }
            else if constexpr ( sizeof...(T_ADD_ARGS1) )
            {
                return Value 
                    + (( 0u + T_ADD_ARGS1::type_guid_v.m_Value ) + ... );
            
            }
            else if constexpr ( sizeof...(T_SUBTRACT_ARGS2) )
            {
                return Value 
                    - (( 0u + T_SUBTRACT_ARGS2::type_guid_v.m_Value ) + ... );
            }
            else
            {
                return Value;
            }
        }
    }


    //---------------------------------------------------------------------------------
    // SYSTEM::INSTANCE 
    //---------------------------------------------------------------------------------
    struct instance : xcore::scheduler::job<4>
    {
        struct construct
        {
            system::guid                    m_Guid;
            world::instance&                m_World;
            xcore::string::const_universal  m_Str;
            xcore::scheduler::definition    m_Def;
        };

        using                                   parent_t        = xcore::scheduler::job<4>;

        // Defaults that can be overwritten by the user
        constexpr static auto                   entities_per_job_v  = 5000;
        constexpr static auto                   name_v              = xconst_universal_str("mecs::System");
        using                                   query_t             = query::define<>; 
        using                                   events_t            = std::tuple<>;

        details::cache      m_Cache;
        system::guid        m_Guid;

        // Messages that we can handle these can be overwritten by the user
        inline void msgSyncPointDone    ( sync_point::instance& )   noexcept {}
        inline void msgSyncPointStart   ( sync_point::instance& )   noexcept {}
        inline void msgWorldStart       ( world::instance& )        noexcept {}
        inline void msgFrameStart       ( void )                    noexcept {}
        inline void msgFrameDone        ( void )                    noexcept {}
        inline void msgUpdate           ( void )                    noexcept {}

        void clearGroupCache()
        {
            // unlock and sync groups from the cache
            for( const auto& E : m_Cache.m_Lines )
            {
                std::as_const(E.m_pGroup->m_SemaphoreLock).unlock();
            }

            m_Cache.m_Lines.clear();
        }

        constexpr xforceinline
        system::guid getGuid( void ) const noexcept
        {
            return m_Guid;
        }

        template< typename T_CALLBACK > constexpr xforceinline
        void DoQuery( query::instance& Query, const query::define_data& Data ) const noexcept
        {
            details::do_query<world::instance,T_CALLBACK>::DoQuery( m_World, Query, Data );
        }

        template< typename T_CALLBACK > constexpr xforceinline
        void DoQuery( query::instance& Query ) const noexcept
        {
            query::define_data Data{};
            details::do_query<world::instance,T_CALLBACK>::DoQuery( m_World, Query, Data );
        }

        template< std::size_t T_ENTITIES_PER_JOB, typename T > constexpr xforceinline
        bool ForEach( query::instance& Query, T&& Functor ) noexcept
        {
        //     std::decay_t<std::remove_reference_t<typename xcore::function::traits<T>::template arg_t<0>>> xx = 66;

            static_assert( xcore::function::traits<T>::arg_count_v >= 1 );
            return details::for_each<system::instance,T_ENTITIES_PER_JOB,T>::ForEach( *this, Query, std::forward<T&&>(Functor) );
        }

        // Functions
        template<typename K = world::instance>
        xforceinline void deleteEntity( entity::instance& Entity )
        {
            reinterpret_cast<K&>(m_World).deleteEntity( *this, Entity );
        }

        template<typename K = world::instance>
        xforceinline auto& getTime( void ) const noexcept
        {
            return reinterpret_cast<const K&>(m_World).m_Time;
        }

        template< typename T_EVENT, typename...T_ARGS >
        xforceinline void EventNotify( T_ARGS&&...Args ) noexcept;

        template< typename T_SYSTEM, typename T_EVENT, typename...T_ARGS >
        xforceinline void GeneralEventNotify( T_ARGS&&...Args ) noexcept;

        template< typename T >
        xforceinline bool getGroup( component::group::guid gGroup, T&& CallBack ) noexcept;

        template< typename...T_COMPONENTS_AND_TAGS >
        component::group::instance& getOrCreateGroup( void ) noexcept;


        template< typename T_TUPLE_ADD_COMPONENTS_AND_TAGS, typename T_TUPLE_REMOVE_COMPONENTS_AND_TAGS, typename T_CALLBACK, typename K = world::instance >
        xforceinline void getGroupBy( entity::instance& Entity, T_CALLBACK&& CallBack ) noexcept
        {
            const auto Value = details::ComputeGroupGuid
            ( 
                    Entity.getTmpRef().m_pGroupInstance->m_Guid.m_Value
                ,   reinterpret_cast<T_TUPLE_ADD_COMPONENTS_AND_TAGS*>(nullptr)
                ,   reinterpret_cast<T_TUPLE_REMOVE_COMPONENTS_AND_TAGS*>(nullptr) 
            );

            //
            // Look in the cache first
            //
            std::as_const(m_Cache.m_Lock).lock();
            for( auto& Line : m_Cache.m_Lines )
            {
                if( Line.m_Guid.m_Value == Value )
                {
                    CallBack( *Line.m_pGroup );
                    std::as_const(m_Cache.m_Lock).unlock();
                    return;
                }
            }
            std::as_const(m_Cache.m_Lock).unlock();

            m_Cache.m_Lock.lock();
            for( auto& Line : m_Cache.m_Lines )
            {
                if( Line.m_Guid.m_Value == Value )
                {
                    CallBack( *Line.m_pGroup );
                    m_Cache.m_Lock.unlock();
                    return;
                }
            }

            // Ok we need to fill the cache
            auto& Group = reinterpret_cast<K&>(m_World).getOrCreateGroupBy
                            ( *Entity.getTmpRef().m_pGroupInstance, (T_TUPLE_ADD_COMPONENTS_AND_TAGS*)nullptr, (T_TUPLE_REMOVE_COMPONENTS_AND_TAGS*)nullptr );
            m_Cache.m_Lines.push_back( details::cache::line{ component::group::guid{Value}, &Group } );
            std::as_const(Group.m_SemaphoreLock).lock();
            m_Cache.m_Lock.unlock();

            // Handle the callback
            CallBack( Group );
        }

        template< typename K = world::instance >
        xforceinline void moveEntityToGroup( component::group::instance& Group, entity::instance& Entity, xcore::function::view<void(entity::tmp_ref)> CallBack = [](entity::tmp_ref)constexpr noexcept{}) noexcept
        {
            reinterpret_cast<K&>(m_World).moveEntityToGroup(*this, Group, Entity, CallBack );
        }

        template< typename T_CREATE_CALLBACK, typename K = world::instance>
        xforceinline void createEntity( component::group::instance& Group, T_CREATE_CALLBACK&& CallBack, entity::guid Guid = entity::guid{xcore::not_null} ) noexcept
        {
            reinterpret_cast<K&>(m_World).createEntity( *this, Group, std::forward<T_CREATE_CALLBACK>(CallBack), Guid );
        }

        template< typename...T_COMPONENTS_AND_TAGS, typename T_INIT_ENTITY_CALLBACK >
        xforceinline void createEntity( T_INIT_ENTITY_CALLBACK&& InitCallback, entity::guid gEntity = entity::guid{xcore::not_null} ) noexcept;

        template< typename...T_COMPONENTS_AND_TAGS >
        xforceinline void createEntity( entity::guid gEntity = entity::guid{xcore::not_null} ) noexcept
        {
            createEntity<T_COMPONENTS_AND_TAGS... >([]()constexpr noexcept{}, gEntity );
        }

        template< typename T_CALLBACK, typename K = world::instance >
        xforceinline bool findEntity( entity::guid gEntity, T_CALLBACK&& Callback ) const noexcept
        {
            return reinterpret_cast<K&>(m_World).m_mapEntity.find( gEntity, [&](entity::map_t::entry& Entry ) constexpr noexcept
            {
                //UpdateGroupCache( *Entry.m_Value.m_pGroupInstance );
                details::CallFunctionWithComponents( Entry.m_Value, std::forward<T_CALLBACK>(Callback) );
            });
        }

        template< typename T_CALLBACK, typename K = world::instance >
        xforceinline bool findEntityRelax( entity::guid gEntity, T_CALLBACK&& Callback ) const noexcept
        {
            entity::map_t::entry* pValue = nullptr;
            reinterpret_cast<K&>(m_World).m_mapEntity.find( gEntity, [&](entity::map_t::entry& Entry ) constexpr noexcept
            {
                //UpdateGroupCache( *Entry.m_Value.m_pGroupInstance );
                pValue = &Entry;
            });
            if(pValue) details::CallFunctionWithComponents( pValue->m_Value, std::forward<T_CALLBACK>(Callback) );
            return !!pValue;
        }

        template< typename T_CALLBACK >
        xforceinline void getEntity( entity::guid gEntity, T_CALLBACK&& CallBack ) const noexcept
        {
            bool b = findEntity( gEntity, std::forward<T_CALLBACK>(CallBack) );
            xassert( b );
        }

        template< typename T_CALLBACK, typename K = world::instance >
        xforceinline bool getEntity( entity::instance& Entity, T_CALLBACK&& Callback ) const noexcept
        {
            return reinterpret_cast<K&>(m_World).m_mapEntity.find( Entity.getGuid(), [&](entity::map_t::entry& Entry ) constexpr noexcept
            {
                //UpdateGroupCache( *Entry.m_Value.m_pGroupInstance );
                details::CallFunctionWithComponents( Entry.m_Value, std::forward<T_CALLBACK>(Callback) );
            });
        }

        template< typename T_CALLBACK, typename K = world::instance >
        xforceinline bool getEntity( const entity::instance& Entity, T_CALLBACK&& Callback ) const noexcept
        {
            return reinterpret_cast<K&>(m_World).m_mapEntity.find( Entity.getGuid(), [&](entity::map_t::entry& Entry ) constexpr noexcept
            {
                //UpdateGroupCache( *Entry.m_Value.m_pGroupInstance );
                details::CallFunctionWithComponents( Entry.m_Value, std::forward<T_CALLBACK>(Callback) );
            });
        }

        template< typename T_CALLBACK, typename K = world::instance >
        xforceinline bool getEntityRelax( const entity::instance& Entity, T_CALLBACK&& Callback ) const noexcept
        {
            //UpdateGroupCache( *Entry.m_Value.m_pGroupInstance );
            details::CallFunctionWithComponents( Entity.m_pInstance->m_Value, std::forward<T_CALLBACK>(Callback) );
            return true;
        }

        template< typename...T_COMPONENTS_AND_TAGS, typename T_GET_CALLBACK, typename T_INIT_ENTITY_CALLBACK >
        xforceinline void getOrCreatEntity( entity::guid gEntity, T_GET_CALLBACK&& GetCallback, T_INIT_ENTITY_CALLBACK&& InitCallback ) noexcept;

        template< typename...T_COMPONENTS_AND_TAGS, typename T_GET_CALLBACK, typename T_INIT_ENTITY_CALLBACK > 
        xforceinline void getOrCreatEntityRelax( entity::guid gEntity, T_GET_CALLBACK&& GetCallback, T_INIT_ENTITY_CALLBACK&& InitCallback ) noexcept;

        instance( const construct& Settings ) noexcept 
            : parent_t  { Settings.m_Def | xcore::scheduler::triggers::DONT_CLEAN_COUNT }
            , m_World   { Settings.m_World }
            , m_Guid    { Settings.m_Guid }
            { setupName(Settings.m_Str); }

        // TODO: This will be private
        world::instance&    m_World;
        query::instance     m_Query;
    };

    //---------------------------------------------------------------------------------
    // SYSTEM::DETAILS::CUSTOM_SYSTEM
    //---------------------------------------------------------------------------------
    namespace details
    {
        template< typename...T_ARGS > 
        std::tuple< xcore::types::make_unique< typename T_ARGS::event_t,  T_ARGS >... > GetEventTuple( std::tuple<T_ARGS...>* ); 

        //---------------------------------------------------------------------------------
        // SYSTEM::CUSTOM SYSTEM 
        //---------------------------------------------------------------------------------
        template< typename T_SYSTEM >
        struct custom_system final : T_SYSTEM
        {
            using user_system_t     = T_SYSTEM;
            using world_instance_t  = std::decay_t< decltype(user_system_t::m_World) >;

            constexpr static typename user_system_t::query_t query_v{};

            sync_point::events::start::delegate                         m_SyncPointStartDelegate{       &custom_system::msgSyncPointStart };
            sync_point::events::done::delegate                          m_SyncPointDoneDelegate { this, &custom_system::msgSyncPointDone  };
            typename world_instance_t::events::world_start::delegate    m_WorldStartDelegate    {       &custom_system::msgWorldStart     };
            typename world_instance_t::events::frame_start::delegate    m_FrameStartDelegate    {       &custom_system::msgFrameStart     };
            typename world_instance_t::events::frame_done::delegate     m_FrameDoneDelegate     {       &custom_system::msgFrameDone      };

            using user_system_t::user_system_t;

            using events_tuple = decltype( GetEventTuple( reinterpret_cast< typename user_system_t::events_t*>(nullptr) ) );
            events_tuple m_Events{};

            custom_system( world::instance& World ) noexcept 
                : T_SYSTEM { instance::construct
                {
                        user_system_t::guid_v
                    ,   World
                    ,   user_system_t::name_v
                    ,   {}
                }}
            {}

            void msgSyncPointDone( sync_point::instance& Syncpoint ) noexcept
            {
                // Call the user function if he overwrote it
                if constexpr ( &user_system_t::msgSyncPointDone != &system::instance::msgSyncPointDone )
                {
                    T_SYSTEM::msgSyncPointDone(Syncpoint);
                }

                // unlock and sync groups from the cache
                for( const auto& E : user_system_t::m_Cache.m_Lines )
                {
                    bool bFound = false;
                    std::as_const(E.m_pGroup->m_SemaphoreLock).unlock();

                    for( const auto& Q : user_system_t::m_Query.m_lResults )
                    {
                        if( E.m_pGroup == Q.m_pGroup )
                        {
                            bFound = true;
                            break;
                        }
                    }

                    if( bFound == false ) E.m_pGroup->MemoryBarrierSync( Syncpoint );
                }

                // Lock all the groups that we need to work with so that no one tries to add/remove entities
                for( const auto& E : user_system_t::m_Query.m_lResults )
                {
                    // TODO: Must lock each component base on r/w also must lock the group
                    E.m_pGroup->MemoryBarrierSync( Syncpoint );
                }

                user_system_t::m_Cache.m_Lines.clear();

                user_system_t::m_Query.m_lResults.clear();
            }

            virtual void qt_onRun( void ) noexcept override
            {
                XCORE_PERF_ZONE_SCOPED_N( user_system_t::name_v.m_Str )
                if constexpr ( &user_system_t::msgUpdate != &system::instance::msgUpdate )
                {
                    user_system_t::msgUpdate();
                }
                else
                {
                    user_system_t::template DoQuery<user_system_t>(user_system_t::m_Query,query_v);
                    if( false == user_system_t::ForEach<user_system_t::entities_per_job_v>(user_system_t::m_Query,*this) )
                    {
                        xassert(false);
                    }
                }
            }
        };
    }
}